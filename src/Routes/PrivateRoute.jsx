import React from "react";
import { Navigate } from "react-router-dom";

const PrivateRoute = ({ children }) => {
  if (localStorage.getItem("access_token") === null) {
    return <Navigate to="/login" replace />;
  }
  return children;
};

export default PrivateRoute;
