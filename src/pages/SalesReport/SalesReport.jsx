import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import Header from "../../components/Header/Header";
import SideBar from "../../components/Sidebar/Sidebar";
import Footer from "../../components/Footer/Footer";
import pdfIcon from "../../assets/icons/pdf.png";
import calender from "../../assets/icons/calender.png";
import restrictCalender from "../../assets/icons/restrict-filter.png";
import cancel from "../../assets/icons/cancel.png";
import { Container, Row, Col, Button, Form } from "react-bootstrap";
import "./SalesReport.css";
import LoadingModal from "../../components/PopupModal/LoadingModal";
import {
  getBarcodeSaleReport,
  getProductSaleReport,
  getInvoiceSaleReport,
  getCashierSaleReport,
  getGenericSaleReport,
  getAllCategorySaleReport,
  getAllSubCategorySaleReport,
  getHourlySaleReport,
  getInvoiceWiseHourlySaleReport,
  getSupplierProductSaleReport,
  getCategoryWiseSupplierReport,
  getSupplierSaleSummary,
  getSupplierWiseCategoryReport,
  getDailySaleSummary,
  getDailyCounterSaleSummary,
  getDailyCounterExchangeSummary,
  getSubcategoryWiseSupplierReport,
  getSupplierWiseSubcategoryReport,
  getCategoryProductSaleReport,
  getSectionWiseSaleReport,
  getSubCategoryProductSaleReport,
} from "../../redux/actions/SalesReportActions";
import {
  getSupplierList,
  getCategoryList,
  getCashierList,
  getCounterList,
  getSubcategoryList,
} from "../../redux/actions/HelperActions";
import BarcodeSaleReport from "../../components/SalesReport/BarcodeSaleReport";
import ProductSaleReport from "../../components/SalesReport/ProductSaleReport";
import GenericSaleReport from "../../components/SalesReport/GenericSaleReport";
import AllCategorySaleReport from "../../components/SalesReport/AllCategorySaleReport";
import DailyInvoiceSaleReport from "../../components/SalesReport/DailyInvoiceSaleReport";
import HourlySaleReport from "../../components/SalesReport/HourlySaleReport";
import InvoiceWiseSaleReport from "../../components/SalesReport/InvoiceWiseSaleReport";
import SupplierProductSaleReport from "../../components/SalesReport/SupplierProductSaleReport";
import CategorySupplierSaleReport from "../../components/SalesReport/CategorySupplierSaleReport";
import SupplierSalesSummary from "../../components/SalesReport/SupplierSalesSummary";
import SupplierCategorySaleReport from "../../components/SalesReport/SupplierCategorySaleReport";
import CashierSaleReport from "../../components/SalesReport/CashierSaleReport";
import DailySalesSummary from "../../components/SalesReport/DailySalesSummary";
import CounterSalesSummary from "../../components/SalesReport/CounterSaleSummary";
import CounterExchangeSummary from "../../components/SalesReport/CounterExchangeSummary";
import SubCategorySupplierSaleReport from "../../components/SalesReport/SubcategorySupplierReport";
import SupplierSubCategorySaleReport from "../../components/SalesReport/SupplierSubCategoryReport";
import CategoryProductSaleReport from "../../components/SalesReport/CategoryProductReport";
import SectionProductSaleReport from "../../components/SalesReport/SectionProductReport";
import SubCategoryProductSaleReport from "../../components/SalesReport/SubCategoryProductReport";
import SalesReportPdf from "../../services/SalesReportPdf";
import MultiTableReportPdf from "../../services/MultiTableReportPdf";
import InvoiceReportPdf from "../../services/InvoiceReportPdf";
import SupplierSaleReportPdf from "../../services/SupplierSaleReportPdf";
import SalesSummaryPdf from "../../services/SalesSummaryPdf";
import axios from "axios";

const SalesReports = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [selectReport, setSelectReport] = useState("");
  const [productType, setProductType] = useState("");
  const [selectSupplier, setSelectSupplier] = useState("");
  const [fromDate, setFromDate] = useState("");
  const [toDate, setToDate] = useState("");
  const [generateButton, setGenerateButton] = useState(false);
  const [filterDateButton, setFilterDateButton] = useState(false);
  const [exportData, setExportData] = useState(false);
  const [searchProduct, setSearchProduct] = useState("");
  const [reportType, setReportType] = useState("");
  const [invoiceReportType, setInvoiceReportType] = useState("");
  const [fromTime, setFromTime] = useState("");
  const [toTime, setToTime] = useState("");
  const [selectCategory, setSelectCategory] = useState("");
  const [selectCashier, setSelectCashier] = useState("");
  const [cashierName, setCashierName] = useState("");
  const [selectCounter, setSelectCounter] = useState("");
  const [selectLocation, setSelectLocation] = useState("");
  const [selectSubCategory, setSelectSubCategory] = useState("");
  const [resultData, setResultData] = useState([]);
  const [allProductList, setAllProductList] = useState([]);

  const { barcodeLoading, barcodeSales, barcodeError } = useSelector(
    (state) => state.barcodeWiseSale
  );
  const { productLoading, productSales, productError } = useSelector(
    (state) => state.productWiseSale
  );
  const { dailyInvoiceLoading, dailyInvoiceSales, dailyInvoiceError } =
    useSelector((state) => state.InvoiceWiseSale);
  const { cashierWiseLoading, cashierWiseSales, cashierWiseError } =
    useSelector((state) => state.cashierWiseSale);
  const { dailyLoading, dailySales, dailyError } = useSelector(
    (state) => state.dailySaleSummary
  );
  const { genericWiseLoading, genericWiseSales, genericWiseError } =
    useSelector((state) => state.genericWiseSale);
  const { allCategoryWiseLoading, allCategoryWiseSales, allCategoryWiseError } =
    useSelector((state) => state.allCategoryWiseSale);
  const {
    allSubCategoryWiseLoading,
    allSubCategoryWiseSales,
    allSubCategoryWiseError,
  } = useSelector((state) => state.allSubCategoryWiseSale);
  const { hourlySalesLoading, hourlySalesList, hourlySalesError } = useSelector(
    (state) => state.hourlySaleReport
  );
  const { invoiceSalesLoading, invoiceSalesList, invoiceSalesError } =
    useSelector((state) => state.invoiceWiseSaleReport);
  const { supplierSalesLoading, supplierSalesList, supplierSalesError } =
    useSelector((state) => state.supplierWiseSaleReport);
  const {
    categorySupplierLoading,
    categorySupplierSale,
    categorySupplierError,
  } = useSelector((state) => state.categoryWiseSupplierReport);
  const {
    allSupplierSummaryLoading,
    allSupplierSummary,
    allSupplierSummaryError,
  } = useSelector((state) => state.allSupplierSummaryReport);
  const {
    supplierCategorySaleLoading,
    supplierCategorySale,
    supplierCategorySaleError,
  } = useSelector((state) => state.supplierWiseCategoryReport);
  const { dailyCounterLoading, dailyCounterSales, dailyCounterError } =
    useSelector((state) => state.dailyCounterSale);
  const { dailyExchangeLoading, dailyCounterExchange, dailyExchangeError } =
    useSelector((state) => state.dailyCounterExchange);
  const { categorySalesLoading, categorySalesList, categorySalesError } =
    useSelector((state) => state.categoryWiseSaleReport);
  const { subcategorySalesLoading, subcategorySalesList } = useSelector(
    (state) => state.subcategoryWiseSaleReport
  );
  const { sectionSalesLoading, sectionSalesList } = useSelector(
    (state) => state.sectionWiseSaleReport
  );
  const { subcategorySupplierLoading, subcategorySupplierSale } = useSelector(
    (state) => state.subcategoryWiseSupplierReport
  );
  const { supplierSubcategorySaleLoading, supplierSubcategorySale } =
    useSelector((state) => state.supplierWiseSubcategoryReport);

  const { supplierList } = useSelector((state) => state.supplierList);
  const { subcategoryList } = useSelector((state) => state.subcategoryList);
  const { categoryList } = useSelector((state) => state.categoryList);
  const { cashierLoading, cashierList, cashierError } = useSelector(
    (state) => state.cashierList
  );
  const { counterLoading, counterList, counterError } = useSelector(
    (state) => state.counterList
  );

  const limit = 30;
  const offset = 0;

  const allReportsType = [
    "Barcode Wise Sales",
    "Product Wise Sales",
    "Daily Invoice Wise Sales Summary",
    "Cashier Wise Daily Sales",
    "Daily Sales Summary",
    "Generic Name Wise Sales",
    "Section Wise Product Sales Summary",
    "Category Wise Product Sales Summary",
    "SubCategory Wise Product Sales Summary",
    "All Category/Sub Category Wise Sales Summary",
    "Comparative Hourly Sales Summary",
    "Invoice Wise Hourly Sales Details",
    "Supplier Wise Product Sales",
    "Category Wise Supplier Sales Summary",
    "SubCategory Wise Supplier Sales Summary",
    "All Supplier Wise Sales Summary",
    "Supplier Wise Category Sales Summary",
    "Supplier Wise SubCategory Sales Summary",
    "Daily Counter Wise Sales Summary",
    "Daily Counter Wise Product Exchange",
  ];

  const BarcodeHeaders = [
    [
      {
        content: `Barcode: ${searchProduct}`,
        colSpan: 6,
        styles: { halign: "start" },
      },
      {
        content: `From Date : ${fromDate !== "" ? fromDate : "N/A"}`,
        colSpan: 3,
      },
      {
        content: `To Date : ${toDate !== "" ? toDate : "N/A"}`,
        colSpan: 3,
      },
    ],
    [
      "SL",
      "Product Information",
      "Sale Date",
      "Sold(Qty)",
      "Total MRP",
      "Disc. Amount",
      "Vat Amount",
      "Ex/Ret Amount",
      "Total Cost",
      "Net Amount",
      "Pft Amount",
      "GP%",
    ],
  ];
  const ProductHeaders = [
    [
      {
        content: `Product Name: ${searchProduct}`,
        colSpan: 7,
        styles: { halign: "start" },
      },
      {
        content: `From Date : ${fromDate !== "" ? fromDate : "N/A"}`,
        colSpan: 3,
      },
      {
        content: `To Date : ${toDate !== "" ? toDate : "N/A"}`,
        colSpan: 3,
      },
    ],
    [
      "SL",
      "Barcode",
      "Product Information",
      "Sale Date",
      "Sold(Qty)",
      "Total MRP",
      "Disc. Amount",
      "Vat Amount",
      "Ex/Ret Amount",
      "Total Cost",
      "Net Amount",
      "Pft Amount",
      "GP%",
    ],
  ];
  const CashierHeaders = [
    [
      {
        content: `Cashier Name : ${cashierName}`,
        colSpan: 3,
      },
      {
        content: `Cashier Id : ${selectCashier}`,
        colSpan: 3,
      },
      {
        content: `From Date : ${fromDate !== "" ? fromDate : "N/A"}`,
        colSpan: 2,
      },
      {
        content: `To Date : ${toDate !== "" ? toDate : "N/A"}`,
        colSpan: 2,
      },
    ],
    [
      "SL",
      "Invoice No.",
      "Total Amount",
      "Disc. Amount",
      "Net Amount",
      "Cash Amount",
      "Online Amount",
      "Card Amount",
      "Due Amount",
      "Payment Method",
    ],
  ];
  const GenericHeaders = [
    [
      {
        content: `Generic Name : ${searchProduct}`,
        colSpan: 7,
      },
      {
        content: `From Date : ${fromDate !== "" ? fromDate : "N/A"}`,
        colSpan: 3,
      },
      {
        content: `To Date : ${toDate !== "" ? toDate : "N/A"}`,
        colSpan: 3,
      },
    ],
    [
      "SL",
      "Barcode",
      "Sale Date",
      "Product Information",
      "Sold Qty",
      "Total MRP",
      "Disc. Amount",
      "Vat Amount",
      "Ex/Ret Amount",
      "Total Cost",
      "Net Amount",
      "Pft Amount",
      "GP%",
    ],
  ];

  useEffect(() => {
    if (
      dailySales.data &&
      dailySales.data.daily_data &&
      dailySales.data.daily_data.length &&
      !dailyLoading
    ) {
      let cumulativeSale = 0;
      setResultData(
        dailySales.data.daily_data.map((data, i) => {
          cumulativeSale += data.sales_amount;
          return { ...data, cumulativeSale };
        })
      );
    }
  }, [dailySales.data, dailyLoading]);

  useEffect(() => {
    if (
      !supplierList.data &&
      (selectReport === "Supplier Wise Product Sales" ||
        selectReport === "Supplier Wise Category Sales Summary" ||
        selectReport === "Supplier Wise SubCategory Sales Summary")
    ) {
      dispatch(getSupplierList(500, 0));
    }
  }, [supplierList.data, selectReport]);

  useEffect(() => {
    if (
      !categoryList.data &&
      (selectReport === "Category Wise Supplier Sales Summary" ||
        selectReport === "Category Wise Product Sales Summary")
    ) {
      dispatch(getCategoryList(500, 0));
    }
  }, [categoryList.data, selectReport]);

  useEffect(() => {
    if (
      !subcategoryList.data &&
      (selectReport === "SubCategory Wise Supplier Sales Summary" ||
        selectReport === "SubCategory Wise Product Sales Summary")
    ) {
      dispatch(getSubcategoryList(500, 0));
    }
  }, [subcategoryList.data, selectReport]);

  useEffect(() => {
    if (!cashierList.data && selectReport === "Cashier Wise Daily Sales") {
      dispatch(getCashierList());
    }
  }, [cashierList.data, selectReport]);

  useEffect(() => {
    if (
      !counterList.data &&
      (selectReport === "Daily Counter Wise Sales Summary" ||
        selectReport === "Daily Counter Wise Product Exchange")
    ) {
      dispatch(getCounterList());
    }
  }, [counterList.data, selectReport]);

  useEffect(() => {
    if (selectReport === "Daily Invoice Wise Sales Summary") {
      // Get the current date
      const currentDate = new Date();
      // Format the current date as YYYY-MM-DD
      const year = currentDate.getFullYear();
      const month = String(currentDate.getMonth() + 1).padStart(2, "0");
      const day = String(currentDate.getDate()).padStart(2, "0");

      const formattedDate = `${year}-${month}-${day}`;

      setFromDate(formattedDate);
    }
  }, [selectReport]);

  useEffect(() => {
    if (selectReport === "Barcode Wise Sales") {
      if (
        exportData &&
        allProductList.products_data &&
        allProductList.products_data.results.length ===
          barcodeSales.data.products_data.count
      ) {
        setExportData(false);
        SalesReportPdf(
          BarcodeHeaders,
          allProductList,
          selectReport,
          "Barcode_Wise_Details_Sales"
        );
      }
    } else if (selectReport === "Product Wise Sales") {
      if (
        exportData &&
        allProductList.products_data &&
        allProductList.products_data.results.length ===
          productSales.data.products_data.count
      ) {
        setExportData(false);
        SalesReportPdf(
          ProductHeaders,
          allProductList,
          selectReport,
          "Product_Wise_Details_Sales"
        );
      }
    } else if (selectReport === "Cashier Wise Daily Sales") {
      if (
        exportData &&
        allProductList.products_data &&
        allProductList.products_data.results.length ===
          cashierWiseSales.data.products_data.count
      ) {
        setExportData(false);
        SalesReportPdf(
          CashierHeaders,
          allProductList,
          selectReport,
          "Cashier_Wise_Daily_Sales"
        );
      }
    } else if (selectReport === "Generic Name Wise Sales") {
      if (
        exportData &&
        allProductList.generic_data &&
        allProductList.generic_data.results.length ===
          genericWiseSales.data.generic_data.count
      ) {
        setExportData(false);
        SalesReportPdf(
          GenericHeaders,
          allProductList,
          selectReport,
          "Generic_Name_Wise_Sales"
        );
      }
    }
  }, [exportData, allProductList, selectReport]);

  //console.log(selectReport === "Barcode Wise Sales");
  //console.log("btn",generateButton)
  // console.log(hourlySalesList.data ? hourlySalesList.data.results.grand_total.filter((data) => data.hour === "00")[0].value : "..");
  const handleGenerateReport = () => {
    if (selectReport === "Barcode Wise Sales") {
      if (searchProduct !== "") {
        setGenerateButton(true);
        if (fromDate !== "" && toDate !== "") {
          dispatch(
            getBarcodeSaleReport(
              `/?limit=${limit}&offset=${offset}&barcode=${searchProduct}&from_date=${fromDate}&to_date=${toDate}`
            )
          );
        } else {
          dispatch(
            getBarcodeSaleReport(
              `/?limit=${limit}&offset=${offset}&barcode=${searchProduct}`
            )
          );
        }
      } else {
        setGenerateButton(false);
        alert("Please Write Down Product Barcode");
      }
    } else if (selectReport === "Product Wise Sales") {
      if (searchProduct !== "") {
        setGenerateButton(true);
        if (fromDate !== "" && toDate !== "") {
          dispatch(
            getProductSaleReport(
              `/?limit=${limit}&offset=${offset}&product_name=${searchProduct}&from_date=${fromDate}&to_date=${toDate}`
            )
          );
        } else {
          dispatch(
            getProductSaleReport(
              `/?limit=${limit}&offset=${offset}&product_name=${searchProduct}`
            )
          );
        }
      } else {
        setGenerateButton(false);
        alert("Please Write Down Product Barcode");
      }
    } else if (selectReport === "Daily Invoice Wise Sales Summary") {
      if (invoiceReportType === "daily") {
        if (fromDate !== "") {
          dispatch(
            getInvoiceSaleReport(
              `/?limit=${limit}&offset=${offset}&from_date=${fromDate}&to_date=${fromDate}`
            )
          );
          setGenerateButton(true);
        } else {
          alert("Please Select Date");
        }
      } else if (invoiceReportType === "time") {
        if (fromTime !== "" && toTime !== "") {
          dispatch(
            getInvoiceSaleReport(
              `/?limit=${limit}&offset=${offset}&from_date=${fromDate}&to_date=${fromDate}&from_time=${fromTime}:00&to_time=${toTime}:00`
            )
          );
          setGenerateButton(true);
        } else {
          alert("Please Select Time Range");
        }
      } else {
        setGenerateButton(false);
        alert("Please Select Report Type");
      }
    } else if (selectReport === "Cashier Wise Daily Sales") {
      if (selectCashier !== "") {
        setGenerateButton(true);
        cashierList.data?.results.map((cashier) =>
          cashier.id == selectCashier ? setCashierName(cashier.Name) : ""
        );
        if (fromDate !== "" && toDate !== "") {
          dispatch(
            getCashierSaleReport(
              `/?limit=${limit}&offset=${offset}&cashier_id=${selectCashier}&from_date=${fromDate}&to_date=${toDate}`
            )
          );
        } else {
          dispatch(
            getCashierSaleReport(
              `/?limit=${limit}&offset=${offset}&cashier_id=${selectCashier}`
            )
          );
        }
      } else {
        setGenerateButton(false);
        alert("Please Select Cashier");
      }
    } else if (selectReport === "Daily Sales Summary") {
      setGenerateButton(true);
      if (fromDate !== "" && toDate !== "") {
        dispatch(
          getDailySaleSummary(`/?from_date=${fromDate}&to_date=${toDate}`)
        );
      } else {
        dispatch(getDailySaleSummary(`/`));
      }
    } else if (selectReport === "Generic Name Wise Sales") {
      if (searchProduct !== "") {
        setGenerateButton(true);
        if (fromDate !== "" && toDate !== "") {
          dispatch(
            getGenericSaleReport(
              `/?limit=${limit}&offset=${offset}&generic_name=${searchProduct}&from_date=${fromDate}&to_date=${toDate}`
            )
          );
        } else {
          dispatch(
            getGenericSaleReport(
              `/?limit=${limit}&offset=${offset}&generic_name=${searchProduct}`
            )
          );
        }
      } else {
        setGenerateButton(false);
        alert("Please Write Down Generic Name");
      }
    } else if (selectReport === "Section Wise Product Sales Summary") {
      if (selectLocation !== "" && fromDate !== "" && toDate !== "") {
        setGenerateButton(true);
        dispatch(
          getSectionWiseSaleReport(
            `/?product_section=${selectLocation}&from_date=${fromDate}&to_date=${toDate}`
          )
        );
      } else if (fromDate === "" || toDate === "") {
        setGenerateButton(false);
        alert("Please Select Date Range");
      } else {
        setGenerateButton(false);
        alert("Please Select Location/Section");
      }
    } else if (selectReport === "Category Wise Product Sales Summary") {
      if (selectCategory !== "" && fromDate !== "" && toDate !== "") {
        setGenerateButton(true);
        if (selectCategory === "all") {
          dispatch(
            getCategoryProductSaleReport(
              `/?from_date=${fromDate}&to_date=${toDate}`
            )
          );
        } else {
          dispatch(
            getCategoryProductSaleReport(
              `/?product_category=${selectCategory}&from_date=${fromDate}&to_date=${toDate}`
            )
          );
        }
      } else if (fromDate === "" || toDate === "") {
        setGenerateButton(false);
        alert("Please Select Date Range");
      } else {
        setGenerateButton(false);
        alert("Please Select Category");
      }
    } else if (selectReport === "SubCategory Wise Product Sales Summary") {
      if (selectSubCategory !== "" && fromDate !== "" && toDate !== "") {
        setGenerateButton(true);
        dispatch(
          getSubCategoryProductSaleReport(
            `/?product_sub_category=${selectSubCategory}&from_date=${fromDate}&to_date=${toDate}`
          )
        );
      } else if (fromDate === "" || toDate === "") {
        setGenerateButton(false);
        alert("Please Select Date Range");
      } else {
        setGenerateButton(false);
        alert("Please Select Subcategory");
      }
    } else if (
      selectReport === "All Category/Sub Category Wise Sales Summary"
    ) {
      if (reportType === "Category") {
        setGenerateButton(true);
        if (fromDate !== "" && toDate !== "") {
          dispatch(
            getAllCategorySaleReport(
              `/?from_date=${fromDate}&to_date=${toDate}`
            )
          );
        } else {
          dispatch(getAllCategorySaleReport(`/`));
        }
      } else if (reportType === "SubCategory") {
        setGenerateButton(true);
        if (fromDate !== "" && toDate !== "") {
          dispatch(
            getAllSubCategorySaleReport(
              `/?from_date=${fromDate}&to_date=${toDate}`
            )
          );
        } else {
          dispatch(getAllSubCategorySaleReport(`/`));
        }
      } else {
        setGenerateButton(false);
        alert("Please Select Report Type");
      }
    } else if (selectReport === "Comparative Hourly Sales Summary") {
      if (fromDate !== "" && toDate !== "") {
        setGenerateButton(true);
        dispatch(
          getHourlySaleReport(
            `/?limit=${limit}&offset=${offset}&date_range_after=${fromDate}&date_range_before=${toDate}`
          )
        );
      } else {
        setGenerateButton(false);
        alert("Please Select Date Range");
      }
    } else if (selectReport === "Invoice Wise Hourly Sales Details") {
      if (fromDate !== "" && toDate !== "") {
        setGenerateButton(true);
        dispatch(
          getInvoiceWiseHourlySaleReport(
            `&limit=${limit}&offset=${offset}&date_range_after=${fromDate}&date_range_before=${toDate}`
          )
        );
      } else {
        setGenerateButton(false);
        alert("Please Select Date Range");
      }
    } else if (selectReport === "Supplier Wise Product Sales") {
      if (selectSupplier !== "") {
        setGenerateButton(true);
        if (fromDate !== "" && toDate !== "") {
          dispatch(
            getSupplierProductSaleReport(
              `/?supplier_id=${selectSupplier}&from_date=${fromDate}&to_date=${toDate}`
            )
          );
        } else {
          dispatch(
            getSupplierProductSaleReport(`/?supplier_id=${selectSupplier}`)
          );
        }
      } else {
        setGenerateButton(false);
        alert("Please Select Supplier");
      }
    } else if (selectReport === "Category Wise Supplier Sales Summary") {
      if (selectCategory !== "") {
        setGenerateButton(true);
        if (selectCategory === "all") {
          if (fromDate !== "" && toDate !== "") {
            dispatch(
              getCategoryWiseSupplierReport(
                `/?from_date=${fromDate}&to_date=${toDate}`
              )
            );
          } else {
            dispatch(getCategoryWiseSupplierReport(`/`));
          }
        } else {
          if (fromDate !== "" && toDate !== "") {
            dispatch(
              getCategoryWiseSupplierReport(
                `/?category=${selectCategory}&from_date=${fromDate}&to_date=${toDate}`
              )
            );
          } else {
            dispatch(
              getCategoryWiseSupplierReport(`/?category=${selectCategory}`)
            );
          }
        }
      } else {
        setGenerateButton(false);
        alert("Please Select Category");
      }
    } else if (selectReport === "All Supplier Wise Sales Summary") {
      setGenerateButton(true);
      if (fromDate !== "" && toDate !== "") {
        dispatch(
          getSupplierSaleSummary(`/?from_date=${fromDate}&to_date=${toDate}`)
        );
      } else {
        dispatch(getSupplierSaleSummary(`/`));
      }
    } else if (selectReport === "Supplier Wise Category Sales Summary") {
      if (selectSupplier !== "") {
        setGenerateButton(true);
        if (selectSupplier === "all") {
          if (fromDate !== "" && toDate !== "") {
            dispatch(
              getSupplierWiseCategoryReport(
                `/?from_date=${fromDate}&to_date=${toDate}`
              )
            );
          } else {
            dispatch(getSupplierWiseCategoryReport(`/`));
          }
        } else {
          if (fromDate !== "" && toDate !== "") {
            dispatch(
              getSupplierWiseCategoryReport(
                `/?supplier=${selectSupplier}&from_date=${fromDate}&to_date=${toDate}`
              )
            );
          } else {
            dispatch(
              getSupplierWiseCategoryReport(`/?supplier=${selectSupplier}`)
            );
          }
        }
      } else {
        setGenerateButton(false);
        alert("Please Select Supplier");
      }
    } else if (selectReport === "SubCategory Wise Supplier Sales Summary") {
      if (selectSubCategory !== "") {
        setGenerateButton(true);
        if (selectSubCategory === "all") {
          if (fromDate !== "" && toDate !== "") {
            dispatch(
              getSubcategoryWiseSupplierReport(
                `/?from_date=${fromDate}&to_date=${toDate}`
              )
            );
          } else {
            dispatch(getSubcategoryWiseSupplierReport(`/`));
          }
        } else {
          if (fromDate !== "" && toDate !== "") {
            dispatch(
              getSubcategoryWiseSupplierReport(
                `/?sub_category=${selectSubCategory}&from_date=${fromDate}&to_date=${toDate}`
              )
            );
          } else {
            dispatch(
              getSubcategoryWiseSupplierReport(
                `/?sub_category=${selectSubCategory}`
              )
            );
          }
        }
      } else {
        setGenerateButton(false);
        alert("Please Select Supplier");
      }
    } else if (selectReport === "Supplier Wise SubCategory Sales Summary") {
      if (selectSupplier !== "") {
        setGenerateButton(true);
        if (selectSupplier === "all") {
          if (fromDate !== "" && toDate !== "") {
            dispatch(
              getSupplierWiseSubcategoryReport(
                `/?from_date=${fromDate}&to_date=${toDate}`
              )
            );
          } else {
            dispatch(getSupplierWiseSubcategoryReport(`/`));
          }
        } else {
          if (fromDate !== "" && toDate !== "") {
            dispatch(
              getSupplierWiseSubcategoryReport(
                `/?supplier=${selectSupplier}&from_date=${fromDate}&to_date=${toDate}`
              )
            );
          } else {
            dispatch(
              getSupplierWiseSubcategoryReport(`/?supplier=${selectSupplier}`)
            );
          }
        }
      } else {
        setGenerateButton(false);
        alert("Please Select Supplier");
      }
    } else if (selectReport === "Daily Counter Wise Sales Summary") {
      if (selectCounter !== "") {
        setGenerateButton(true);
        if (fromDate !== "" && toDate !== "") {
          dispatch(
            getDailyCounterSaleSummary(
              `/?counter_name=${selectCounter}&from_date=${fromDate}&to_date=${toDate}`
            )
          );
        } else {
          dispatch(
            getDailyCounterSaleSummary(`/?counter_name=${selectCounter}`)
          );
        }
      } else {
        setGenerateButton(false);
        alert("Please Select Counter No.");
      }
    } else if (selectReport === "Daily Counter Wise Product Exchange") {
      if (selectCounter !== "") {
        setGenerateButton(true);
        if (fromDate !== "" && toDate !== "") {
          dispatch(
            getDailyCounterExchangeSummary(
              `/?counter_name=${selectCounter}&from_date=${fromDate}&to_date=${toDate}`
            )
          );
        } else {
          dispatch(
            getDailyCounterExchangeSummary(`/?counter_name=${selectCounter}`)
          );
        }
      } else {
        setGenerateButton(false);
        alert("Please Select Counter No.");
      }
    }
  };

  const findMaxDateValue = () => {
    const inputDate = fromDate;

    // Create a JavaScript Date object from the input date string
    const currentDate = new Date(inputDate);

    // Add two days to the current date
    currentDate.setDate(currentDate.getDate() + 3);

    // Get the year, month, and day components of the new date
    const year = currentDate.getFullYear();
    const month = String(currentDate.getMonth() + 1).padStart(2, "0"); // Months are zero-based, so add 1
    const day = String(currentDate.getDate()).padStart(2, "0");

    // Format the new date in "YYYY-MM-DD" format
    const newDate = `${year}-${month}-${day}`;

    return newDate;
  };
  const getDataFromApi = (url) => {
    try {
      const settings = {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("access_token")}`,
        },
      };
      const api = url;

      axios
        .get(api, settings)
        .then((res) => {
          setAllProductList(res.data);
        })
        .catch((errror) => {
          console.log("error", errror);
          setExportData(false);
          alert("Something Went Wrong,Please Try Again After a minute");
        });
    } catch {
      console.log("error");
      setExportData(false);
      alert("Something Went Wrong,Please Try Again After a minute");
    }
  };

  const convertReportToPdf = () => {
    if (generateButton && selectReport !== "") {
      if (selectReport === "Barcode Wise Sales") {
        if (
          barcodeSales.data.products_data.results.length ===
          barcodeSales.data.products_data.count
        ) {
          SalesReportPdf(
            BarcodeHeaders,
            barcodeSales.data,
            selectReport,
            "Barcode_Wise_Details_Sales"
          );
        } else if (
          barcodeSales.data.products_data.results.length ===
            barcodeSales.data.products_data.count &&
          allProductList.products_data.results.length ===
            barcodeSales.data.products_data.count
        ) {
          SalesReportPdf(
            BarcodeHeaders,
            allProductList,
            selectReport,
            "Barcode_Wise_Details_Sales"
          );
        } else {
          setExportData(true);
          if (fromDate !== "" && toDate !== "") {
            getDataFromApi(
              `${
                import.meta.env.VITE_REACT_APP_BASE_URL
              }/api/report/v1/sales_report/barcode_sales_report/?limit=${
                barcodeSales.data.products_data.count
              }&offset=${offset}&barcode=${searchProduct}&from_date=${fromDate}&to_date=${toDate}`
            );
          } else {
            getDataFromApi(
              `${
                import.meta.env.VITE_REACT_APP_BASE_URL
              }/api/report/v1/sales_report/barcode_sales_report/?limit=${
                barcodeSales.data.products_data.count
              }&offset=${offset}&barcode=${searchProduct}`
            );
          }
        }
      } else if (selectReport === "Product Wise Sales") {
        if (
          productSales.data.products_data.results.length ===
          productSales.data.products_data.count
        ) {
          SalesReportPdf(
            ProductHeaders,
            productSales.data,
            selectReport,
            "Product_Wise_Details_Sales"
          );
        } else if (
          productSales.data.products_data.results.length ===
            productSales.data.products_data.count &&
          allProductList.products_data.results.length ===
            productSales.data.products_data.count
        ) {
          SalesReportPdf(
            ProductHeaders,
            allProductList,
            selectReport,
            "Product_Wise_Details_Sales"
          );
        } else {
          setExportData(true);
          if (fromDate !== "" && toDate !== "") {
            getDataFromApi(
              `${
                import.meta.env.VITE_REACT_APP_BASE_URL
              }/api/report/v1/sales_report/product_sales_report/?limit=${
                productSales.data.products_data.count
              }&offset=${offset}&product_name=${searchProduct}&from_date=${fromDate}&to_date=${toDate}`
            );
          } else {
            getDataFromApi(
              `${
                import.meta.env.VITE_REACT_APP_BASE_URL
              }/api/report/v1/sales_report/product_sales_report/?limit=${
                productSales.data.products_data.count
              }&offset=${offset}&product_name=${searchProduct}`
            );
          }
        }
      } else if (selectReport === "Daily Invoice Wise Sales Summary") {
        // Pagination Data Not Ready
        var headerRows = [
          [
            {
              content: `From Date : ${fromDate !== "" ? fromDate : "N/A"}`,
              colSpan: 9,
            },
            {
              content: `To Date : ${fromDate !== "" ? fromDate : "N/A"}`,
              colSpan: 8,
            },
          ],
          [
            "SL",
            "Invoice No.",
            "MRP",
            "Disc. Amount",
            "Vat",
            "Exchange",
            "Net Amount",
            "Redeem Point",
            "Total Cost",
            "Return Cost",
            "Cash",
            "Online",
            "Card",
            "Due",
            "Pft Amount",
            "GP%",
            "I.Time",
          ],
        ];
        SalesReportPdf(
          headerRows,
          dailyInvoiceSales.data,
          selectReport,
          "Daily_Invoice_Wise_Sales_Summary"
        );
      } else if (selectReport === "Cashier Wise Daily Sales") {
        if (
          cashierWiseSales.data.products_data.results.length ===
          cashierWiseSales.data.products_data.count
        ) {
          SalesReportPdf(
            CashierHeaders,
            cashierWiseSales.data,
            selectReport,
            "Cashier_Wise_Daily_Sales"
          );
        } else if (
          cashierWiseSales.data.products_data.results.length ===
            cashierWiseSales.data.products_data.count &&
          allProductList.products_data.results.length ===
            cashierWiseSales.data.products_data.count
        ) {
          SalesReportPdf(
            CashierHeaders,
            allProductList,
            selectReport,
            "Cashier_Wise_Daily_Sales"
          );
        } else {
          setExportData(true);
          if (fromDate !== "" && toDate !== "") {
            getDataFromApi(
              `${
                import.meta.env.VITE_REACT_APP_BASE_URL
              }/api/report/v1/sales_report/cashier_sales_report/?limit=${
                cashierWiseSales.data.products_data.count
              }&offset=${offset}&cashier_id=${selectCashier}&from_date=${fromDate}&to_date=${toDate}`
            );
          } else {
            getDataFromApi(
              `${
                import.meta.env.VITE_REACT_APP_BASE_URL
              }/api/report/v1/sales_report/cashier_sales_report/?limit=${
                cashierWiseSales.data.products_data.count
              }&offset=${offset}&cashier_id=${selectCashier}`
            );
          }
        }
      } else if (selectReport === "Daily Sales Summary") {
        var headerRows = [
          [
            {
              content: `From Date : ${fromDate !== "" ? fromDate : "N/A"}`,
              colSpan: 3,
            },
            {
              content: `To Date : ${toDate !== "" ? toDate : "N/A"}`,
              colSpan: 2,
            },
          ],
          [
            "Sale Date",
            "Sales Amount",
            "Without (Return + VAT)",
            "Return + VAT",
            "Cumulative Sales",
          ],
        ];
        SalesReportPdf(
          headerRows,
          dailySales.data,
          selectReport,
          "Daily_Sales_Summary",
          resultData
        );
      } else if (selectReport === "Generic Name Wise Sales") {
        if (
          genericWiseSales.data.generic_data.results.length ===
          genericWiseSales.data.generic_data.count
        ) {
          SalesReportPdf(
            GenericHeaders,
            genericWiseSales.data,
            selectReport,
            "Generic_Name_Wise_Sales"
          );
        } else if (
          genericWiseSales.data.generic_data.results.length ===
            genericWiseSales.data.generic_data.count &&
          allProductList.generic_data.results.length ===
            genericWiseSales.data.generic_data.count
        ) {
          SalesReportPdf(
            GenericHeaders,
            allProductList,
            selectReport,
            "Generic_Name_Wise_Sales"
          );
        } else {
          setExportData(true);
          if (fromDate !== "" && toDate !== "") {
            getDataFromApi(
              `${
                import.meta.env.VITE_REACT_APP_BASE_URL
              }/api/report/v1/sales_report/generic_sales_report/?limit=${
                genericWiseSales.data.generic_data.count
              }&offset=${offset}&generic_name=${searchProduct}&from_date=${fromDate}&to_date=${toDate}`
            );
          } else {
            getDataFromApi(
              `${
                import.meta.env.VITE_REACT_APP_BASE_URL
              }/api/report/v1/sales_report/generic_sales_report/?limit=${
                genericWiseSales.data.generic_data.count
              }&offset=${offset}&generic_name=${searchProduct}`
            );
          }
        }
      } else if (selectReport === "Section Wise Product Sales Summary") {
        var headerRows = [
          [
            {
              content: `Location Name : ${selectLocation}`,
              colSpan: 7,
            },
            {
              content: `From Date : ${fromDate !== "" ? fromDate : "N/A"}`,
              colSpan: 3,
            },
            {
              content: `To Date : ${toDate !== "" ? toDate : "N/A"}`,
              colSpan: 3,
            },
          ],
          [
            "SL",
            "Barcode",
            "Sale Date",
            "Product Information",
            "Sold Qty",
            "Total MRP",
            "Disc. Amount",
            "Vat Amount",
            "Ex/Ret Amount",
            "Total Cost",
            "Net Amount",
            "Pft Amount",
            "GP%",
          ],
        ];
        SalesReportPdf(
          headerRows,
          sectionSalesList.data,
          selectReport,
          "Section_Wise_Product_Sales_Summary"
        );
      } else if (selectReport === "Category Wise Product Sales Summary") {
        const tableData = categorySalesList.data.sub_total.map((category) => ({
          category: category,
          products_data: categorySalesList.data.category_data.filter(
            (product) => product.product_category === category.product_category
          ),
        }));
        MultiTableReportPdf(
          tableData,
          selectReport,
          "Category_Wise_Product_Sales_Summary",
          fromDate,
          toDate
        );
      } else if (selectReport === "SubCategory Wise Product Sales Summary") {
        var headerRows = [
          [
            {
              content: `SubCategory Name : ${subcategorySalesList.data.sub_total[0].product_sub_category}`,
              colSpan: 7,
            },
            {
              content: `From Date : ${fromDate !== "" ? fromDate : "N/A"}`,
              colSpan: 3,
            },
            {
              content: `To Date : ${toDate !== "" ? toDate : "N/A"}`,
              colSpan: 3,
            },
          ],
          [
            "SL",
            "Barcode",
            "Sale Date",
            "Product Information",
            "Sold Qty",
            "Total MRP",
            "Disc. Amount",
            "Vat Amount",
            "Ex/Ret Amount",
            "Total Cost",
            "Net Amount",
            "Pft Amount",
            "GP%",
          ],
        ];
        SalesReportPdf(
          headerRows,
          subcategorySalesList.data,
          selectReport,
          "SubCategory_Wise_Product_Sales_Summary"
        );
      } else if (
        selectReport === "All Category/Sub Category Wise Sales Summary"
      ) {
        var headerRows = [
          [
            {
              content: `From Date : ${fromDate !== "" ? fromDate : "N/A"}`,
              colSpan: 7,
            },
            {
              content: `To Date : ${toDate !== "" ? toDate : "N/A"}`,
              colSpan: 5,
            },
          ],
          [
            "SL",
            reportType === "Category" ? "Category Name" : "SubCategory Name",
            "Sold Qty",
            "Return Qty",
            "Total MRP",
            "Disc. Amount",
            "Vat Amount",
            "Ex/Ret Amount",
            "Total Cost",
            "Net Amount",
            "Pft Amount",
            "GP%",
          ],
        ];
        SalesReportPdf(
          headerRows,
          reportType === "Category"
            ? allCategoryWiseSales.data.category_data
            : allSubCategoryWiseSales.data.subcategory_data,
          selectReport,
          "All_Category/Sub Category_Wise_Sales_Summary",
          allCategoryWiseSales.data.sub_total
        );
      } else if (selectReport === "Comparative Hourly Sales Summary") {
        // Pagination Data Not Ready
        var headerRows = [
          [
            {
              content: `From Date : ${fromDate !== "" ? fromDate : "N/A"}`,
              colSpan: 13,
            },
            {
              content: `To Date : ${toDate !== "" ? toDate : "N/A"}`,
              colSpan: 12,
            },
          ],
          [
            "Date",
            "00",
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
          ],
        ];
        SalesReportPdf(
          headerRows,
          hourlySalesList.data.results,
          selectReport,
          "Comparative_Hourly_Sales_Summary"
        );
      } else if (selectReport === "Invoice Wise Hourly Sales Details") {
        // Pagination data is not ready
        InvoiceReportPdf(
          invoiceSalesList.data.results,
          selectReport,
          "Invoice_Wise_Hourly_Sales_Details",
          fromDate,
          toDate
        );
      } else if (selectReport === "Supplier Wise Product Sales") {
        var headerRows = [
          [
            {
              content: `Supplier Name : ${supplierSalesList.data.sub_total[0].supplier_name}`,
              colSpan: 4,
            },
            {
              content: `Supplier ID : ${selectSupplier}`,
              colSpan: 3,
            },
            {
              content: `From Date : ${fromDate !== "" ? fromDate : "N/A"}`,
              colSpan: 3,
            },
            {
              content: `To Date : ${toDate !== "" ? toDate : "N/A"}`,
              colSpan: 3,
            },
          ],
          [
            "SL",
            "Barcode",
            "Product Information",
            "Sale Date",
            "Sold Qty",
            "Total MRP",
            "Disc. Amount",
            "Vat Amount",
            "Ex/Ret Amount",
            "Total Cost",
            "Net Amount",
            "Pft Amount",
            "GP%",
          ],
        ];
        SalesReportPdf(
          headerRows,
          supplierSalesList.data,
          selectReport,
          "Supplier_Wise_Product_Sales"
        );
      } else if (selectReport === "Category Wise Supplier Sales Summary") {
        const tableData = categorySupplierSale.data.category_total.map(
          (category) => ({
            category: category,
            products_data: categorySupplierSale.data.supplier_data.filter(
              (product) => product.category_name === category.product_category
            ),
          })
        );
        SupplierSaleReportPdf(
          tableData,
          selectReport,
          "Category_Wise_Supplier_Sales_Summary",
          fromDate,
          toDate,
          "Category Name :"
        );
      } else if (selectReport === "SubCategory Wise Supplier Sales Summary") {
        const tableData = subcategorySupplierSale.data.sub_category_total.map(
          (category) => ({
            category: category,
            products_data: subcategorySupplierSale.data.supplier_data.filter(
              (product) =>
                product.sub_category_name === category.product_sub_category
            ),
          })
        );
        SupplierSaleReportPdf(
          tableData,
          selectReport,
          "SubCategory_Wise_Supplier_Sales_Summary",
          fromDate,
          toDate,
          "SubCategory Name :"
        );
      } else if (selectReport === "All Supplier Wise Sales Summary") {
        var headerRows = [
          [
            {
              content: `From Date : ${fromDate !== "" ? fromDate : "N/A"}`,
              colSpan: 6,
            },
            {
              content: `To Date : ${toDate !== "" ? toDate : "N/A"}`,
              colSpan: 5,
            },
          ],
          [
            "SL",
            "Supplier Name",
            "Sold Qty",
            "Return Qty",
            "Total MRP",
            "Discount Amount",
            "Vat Amount",
            "Ex/Ret Amount",
            "Net Amount",
            "Pft Amount",
            "GP%",
          ],
        ];
        SalesReportPdf(
          headerRows,
          allSupplierSummary.data,
          selectReport,
          "All_Supplier_Wise_Sales_Summary"
        );
      } else if (selectReport === "Supplier Wise Category Sales Summary") {
        const tableData = supplierCategorySale.data.supplier_total.map(
          (supplier) => ({
            supplier: supplier,
            products_data: supplierCategorySale.data.category_data.filter(
              (product) => product.supplier_name === supplier.supplier_name
            ),
          })
        );
        SalesSummaryPdf(
          tableData,
          selectReport,
          "Supplier_Wise_Category_Sales_Summary",
          fromDate,
          toDate,
          "Category Name"
        );
      } else if (selectReport === "Supplier Wise SubCategory Sales Summary") {
        const tableData = supplierSubcategorySale.data.supplier_total.map(
          (supplier) => ({
            supplier: supplier,
            products_data:
              supplierSubcategorySale.data.sub_category_data.filter(
                (product) => product.supplier_name === supplier.supplier_name
              ),
          })
        );
        SalesSummaryPdf(
          tableData,
          selectReport,
          "Supplier_Wise_SubCategory_Sales_Summary",
          fromDate,
          toDate,
          "SubCategory Name"
        );
      } else if (selectReport === "Daily Counter Wise Sales Summary") {
        var headerRows = [
          [
            {
              content: `Counter No : ${selectCounter}`,
              colSpan: 5,
            },
            {
              content: `From Date : ${fromDate !== "" ? fromDate : "N/A"}`,
              colSpan: 5,
            },
            {
              content: `To Date : ${toDate !== "" ? toDate : "N/A"}`,
              colSpan: 5,
            },
          ],
          [
            "Invoice No",
            "MRP",
            "Disc. Amount",
            "Vat",
            "Exchange",
            "Net Amount",
            "Total Cost",
            "Return Cost",
            "Cash",
            "Online",
            "Card",
            "Due",
            "Adj. Amount",
            "Pft Amount",
            "GP%",
          ],
        ];
        SalesReportPdf(
          headerRows,
          dailyCounterSales.data,
          selectReport,
          "Daily_Counter_Wise_Sales_Summary"
        );
      } else if (selectReport === "Daily Counter Wise Product Exchange") {
        // Need To work after modify pos body data
        alert("No Data Found");
      } else {
        alert("Report is not ready");
      }
    } else {
      alert("Please Generate Report First");
    }
  };

  return (
    <div>
      <LoadingModal
        show={exportData}
        onHide={() => {
          setExportData(false);
        }}
      />
      <SideBar></SideBar>
      <div className="mainContent">
        <Header></Header>
        <Container fluid>
          <h4 className="product-heading container-view">
            Sales Report at a Glance
          </h4>
          <p className="product-paragraph container-view">
            All Details of Sales Report
          </p>
          <div className="mb-4 report-page-card card product-card container-view">
            <div className="">
              <Row className="mb-2">
                <Col xl={10}>
                  {selectReport ===
                  "Purchase Number Wise Supplier Stock Receipt Position Details" ? (
                    <img
                      src={restrictCalender}
                      alt="restrictCalender"
                      height={45}
                      style={{ cursor: "not-allowed" }}
                    />
                  ) : (
                    <img
                      src={filterDateButton ? cancel : calender}
                      alt="calender"
                      height={45}
                      style={{ cursor: "pointer" }}
                      onClick={() => {
                        if (filterDateButton) {
                          setFromDate("");
                          setToDate("");
                        }
                        setFilterDateButton(!filterDateButton);
                      }}
                    />
                  )}
                  <select
                    name="stock-report"
                    className="select-bar"
                    onChange={(e) => {
                      setSelectReport(e.target.value);
                      setGenerateButton(false);
                      setSelectSupplier("");
                      setProductType("");
                      setFromDate("");
                      setToDate("");
                      setSearchProduct("");
                      setSelectCategory("");
                      setCashierName("");
                      setSelectCounter("");
                      setSelectLocation("");
                      setSelectSubCategory("");
                    }}
                    value={selectReport}
                  >
                    <option value="">Select Report Type</option>
                    {allReportsType.map((report, i) => (
                      <option key={i} value={report}>
                        {report}
                      </option>
                    ))}
                  </select>
                  {selectReport !== "" ? (
                    <>
                      {selectReport === "Barcode Wise Sales" ? (
                        <input
                          name="sales-report"
                          className="search-product"
                          placeholder="Search/Scan Barcode"
                          onChange={(e) => setSearchProduct(e.target.value)}
                          value={searchProduct}
                        />
                      ) : selectReport === "Product Wise Sales" ? (
                        <input
                          name="sales-report"
                          className="search-product"
                          placeholder="Search By Product Name"
                          onChange={(e) => setSearchProduct(e.target.value)}
                          value={searchProduct}
                        />
                      ) : selectReport ===
                        "Daily Invoice Wise Sales Summary" ? (
                        <>
                          <select
                            name="sales-report"
                            className="select-product-type"
                            onChange={(e) => {
                              setInvoiceReportType(e.target.value);
                              setGenerateButton(false);
                            }}
                            value={invoiceReportType}
                          >
                            <option value="">Select Report Type</option>
                            <option value="daily">Daily Report</option>
                            <option value="time">Time Range Wise Report</option>
                          </select>
                        </>
                      ) : selectReport === "Cashier Wise Daily Sales" ? (
                        <select
                          name="sales-report"
                          className="select-product-type"
                          onChange={(e) => {
                            setSelectCashier(e.target.value);
                            setGenerateButton(false);
                          }}
                          value={selectCashier}
                        >
                          <option value="">Select Cashier</option>
                          {cashierList.data?.results.map((cashier) => (
                            <option key={cashier.id} value={cashier.id}>
                              {cashier.Name}
                            </option>
                          ))}
                        </select>
                      ) : selectReport === "Generic Name Wise Sales" ? (
                        <input
                          name="sales-report"
                          className="search-product"
                          placeholder="Search By Generic Name"
                          onChange={(e) => setSearchProduct(e.target.value)}
                          value={searchProduct}
                        />
                      ) : selectReport ===
                        "Section Wise Product Sales Summary" ? (
                        <>
                          <select
                            name="sales-report"
                            className="select-product-type"
                            onChange={(e) => {
                              setSelectLocation(e.target.value);
                              setGenerateButton(false);
                            }}
                            value={selectLocation}
                          >
                            <option value="">Select Location/Section</option>
                            <option value="medicare">Medicare</option>
                            <option value="bodycare">Bodycare</option>
                            <option value="privatebox">PrivateBox</option>
                            <option value="food">Food</option>
                          </select>
                        </>
                      ) : selectReport ===
                        "All Category/Sub Category Wise Sales Summary" ? (
                        <select
                          name="sales-report"
                          className="select-product-type"
                          onChange={(e) => {
                            setReportType(e.target.value);
                            setGenerateButton(false);
                          }}
                          value={reportType}
                        >
                          <option value="">Select Category/Sub Category</option>
                          <option value="Category">Category</option>
                          <option value="SubCategory">Sub Category</option>
                        </select>
                      ) : selectReport === "Supplier Wise Product Sales" ||
                        selectReport ===
                          "Supplier Wise Category Sales Summary" ||
                        selectReport ===
                          "Supplier Wise SubCategory Sales Summary" ? (
                        <select
                          name="sales-report"
                          className="select-product-type"
                          onChange={(e) => {
                            setSelectSupplier(e.target.value);
                            setGenerateButton(false);
                          }}
                          value={selectSupplier}
                        >
                          <option value="">Select Supplier</option>
                          {selectReport !== "Supplier Wise Product Sales" ? (
                            <option value="all">All</option>
                          ) : (
                            <></>
                          )}
                          {supplierList.data?.results.map((supplier) => (
                            <option key={supplier.id} value={supplier.id}>
                              {supplier.supplier_name}
                            </option>
                          ))}
                        </select>
                      ) : selectReport ===
                          "Category Wise Supplier Sales Summary" ||
                        selectReport ===
                          "Category Wise Product Sales Summary" ? (
                        <select
                          name="sales-report"
                          className="select-product-type"
                          onChange={(e) => {
                            setSelectCategory(e.target.value);
                            setGenerateButton(false);
                          }}
                          value={selectCategory}
                        >
                          <option value="">Select Category</option>
                          <option value="all">All</option>
                          {categoryList.data?.results.map((category) => (
                            <option key={category.id} value={category.id}>
                              {category.name}
                            </option>
                          ))}
                        </select>
                      ) : selectReport ===
                          "SubCategory Wise Supplier Sales Summary" ||
                        selectReport ===
                          "SubCategory Wise Product Sales Summary" ? (
                        <select
                          name="sales-report"
                          className="select-product-type"
                          onChange={(e) => {
                            setSelectSubCategory(e.target.value);
                            setGenerateButton(false);
                          }}
                          value={selectSubCategory}
                        >
                          <option value="">Select SubCategory</option>
                          {selectReport ===
                          "SubCategory Wise Supplier Sales Summary" ? (
                            <option value="all">All</option>
                          ) : (
                            <></>
                          )}
                          {subcategoryList.data?.results.map((subcategory) => (
                            <option key={subcategory.id} value={subcategory.id}>
                              {subcategory.name}
                            </option>
                          ))}
                        </select>
                      ) : selectReport === "Daily Counter Wise Sales Summary" ||
                        selectReport ===
                          "Daily Counter Wise Product Exchange" ? (
                        <select
                          name="sales-report"
                          className="select-product-type"
                          onChange={(e) => {
                            setSelectCounter(e.target.value);
                            setGenerateButton(false);
                          }}
                          value={selectCounter}
                        >
                          <option value="">Select Counter</option>
                          {counterList.data?.counter_data.map((counter) => (
                            <option key={counter.id} value={counter.id}>
                              {counter.id}
                            </option>
                          ))}
                        </select>
                      ) : (
                        <></>
                      )}
                      <Button
                        className="generate-btn border-0"
                        type="submit"
                        onClick={handleGenerateReport}
                      >
                        Generate Report
                      </Button>
                    </>
                  ) : (
                    <></>
                  )}
                </Col>
                <Col xl={2} className="d-flex justify-content-end">
                  <img
                    //className="pe-3"
                    src={pdfIcon}
                    alt="pdf"
                    height={45}
                    onClick={convertReportToPdf}
                    style={{ cursor: "pointer" }}
                  />
                </Col>
              </Row>
              <Row>
                {filterDateButton ? (
                  <>
                    <Col lg={2} md={4} sm={6}>
                      <Form.Group
                        className="form-group"
                        controlId="formBasicEmail"
                      >
                        <Form.Label className="report-form-label">
                          From Date
                        </Form.Label>
                        <Form.Control
                          type="date"
                          placeholder="From Date"
                          value={fromDate}
                          onChange={(e) => {
                            setFromDate(e.target.value);
                            setToDate("");
                            setGenerateButton(false);
                          }}
                        />
                      </Form.Group>
                    </Col>
                    {selectReport !== "Daily Invoice Wise Sales Summary" ? (
                      <Col lg={2} md={4} sm={6}>
                        <Form.Group
                          className="form-group"
                          controlId="formBasicEmail"
                        >
                          <Form.Label className="report-form-label">
                            To Date
                          </Form.Label>
                          <Form.Control
                            type="date"
                            placeholder="To Date"
                            value={toDate}
                            onChange={(e) => setToDate(e.target.value)}
                            min={fromDate}
                            max={
                              fromDate !== "" &&
                              (selectReport ===
                                "Section Wise Product Sales Summary" ||
                                selectReport ===
                                  "Category Wise Product Sales Summary" ||
                                selectReport ===
                                  "SubCategory Wise Product Sales Summary")
                                ? findMaxDateValue()
                                : ""
                            }
                            //max={fromDate && new Date(fromDate + 2 * 24 * 60 * 60 * 1000)}
                            //style={{ height: "40px" }}
                          />
                        </Form.Group>
                      </Col>
                    ) : (
                      <></>
                    )}
                  </>
                ) : (
                  <></>
                )}
                {selectReport === "Daily Invoice Wise Sales Summary" &&
                invoiceReportType === "time" ? (
                  <>
                    <Col lg={2} md={4} sm={6}>
                      <Form.Group
                        className="form-group"
                        controlId="formBasicEmail"
                      >
                        <Form.Label className="report-form-label">
                          From Time
                        </Form.Label>
                        <Form.Control
                          type="time"
                          placeholder="From Time"
                          value={fromTime}
                          onChange={(e) => setFromTime(e.target.value)}
                        />
                      </Form.Group>
                    </Col>
                    <Col lg={2} md={4} sm={6}>
                      <Form.Group
                        className="form-group"
                        controlId="formBasicEmail"
                      >
                        <Form.Label className="report-form-label">
                          To Time
                        </Form.Label>
                        <Form.Control
                          type="time"
                          placeholder="To Time"
                          value={toTime}
                          onChange={(e) => setToTime(e.target.value)}
                        />
                      </Form.Group>
                    </Col>
                  </>
                ) : (
                  <></>
                )}
              </Row>
            </div>
            <div className="">
              <Row className="mt-3">
                <h4 className="product-heading">
                  {selectReport ===
                    "All Category/Sub Category Wise Sales Summary" &&
                  reportType === "Category"
                    ? "All Category Wise Sales Summary"
                    : selectReport ===
                        "All Category/Sub Category Wise Sales Summary" &&
                      reportType === "SubCategory"
                    ? "All Sub Category Wise Sales Summary"
                    : selectReport === "Daily Invoice Wise Sales Summary" &&
                      invoiceReportType === "time"
                    ? "Daily invoice Wise Sales Summary With Time Range"
                    : selectReport !== ""
                    ? selectReport
                    : "Select An Option to See Sales Report at a Glance"}
                </h4>
              </Row>
            </div>
            {selectReport === "Barcode Wise Sales" && generateButton ? (
              <BarcodeSaleReport
                searchProduct={searchProduct}
                barcodeSales={barcodeSales}
                barcodeLoading={barcodeLoading}
                fromDate={fromDate}
                toDate={toDate}
                limit={limit}
              />
            ) : selectReport === "Product Wise Sales" && generateButton ? (
              <ProductSaleReport
                searchProduct={searchProduct}
                productSales={productSales}
                productLoading={productLoading}
                fromDate={fromDate}
                toDate={toDate}
                limit={limit}
              />
            ) : selectReport === "Daily Invoice Wise Sales Summary" &&
              generateButton ? (
              <DailyInvoiceSaleReport
                invoiceReportType={invoiceReportType}
                dailyInvoiceSales={dailyInvoiceSales}
                dailyInvoiceLoading={dailyInvoiceLoading}
                fromDate={fromDate}
                fromTime={fromTime}
                toTime={toTime}
                limit={limit}
              />
            ) : selectReport === "Cashier Wise Daily Sales" &&
              generateButton ? (
              <CashierSaleReport
                selectCashier={selectCashier}
                cashierName={cashierName}
                cashierWiseSales={cashierWiseSales}
                cashierWiseLoading={cashierWiseLoading}
                fromDate={fromDate}
                toDate={toDate}
                limit={limit}
              />
            ) : selectReport === "Daily Sales Summary" && generateButton ? (
              <DailySalesSummary
                dailySales={dailySales}
                dailyLoading={dailyLoading}
                fromDate={fromDate}
                toDate={toDate}
                resultData={resultData}
              />
            ) : selectReport === "Generic Name Wise Sales" && generateButton ? (
              <GenericSaleReport
                searchProduct={searchProduct}
                genericWiseSales={genericWiseSales}
                genericWiseLoading={genericWiseLoading}
                fromDate={fromDate}
                toDate={toDate}
                limit={limit}
              />
            ) : selectReport === "Section Wise Product Sales Summary" &&
              generateButton ? (
              <SectionProductSaleReport
                sectionSalesList={sectionSalesList}
                sectionSalesLoading={sectionSalesLoading}
                fromDate={fromDate}
                toDate={toDate}
              />
            ) : selectReport === "Category Wise Product Sales Summary" &&
              generateButton ? (
              <CategoryProductSaleReport
                categorySalesList={categorySalesList}
                categorySalesLoading={categorySalesLoading}
                fromDate={fromDate}
                toDate={toDate}
              />
            ) : selectReport === "SubCategory Wise Product Sales Summary" &&
              generateButton ? (
              <SubCategoryProductSaleReport
                subcategorySalesList={subcategorySalesList}
                subcategorySalesLoading={subcategorySalesLoading}
                fromDate={fromDate}
                toDate={toDate}
              />
            ) : selectReport ===
                "All Category/Sub Category Wise Sales Summary" &&
              generateButton ? (
              <AllCategorySaleReport
                reportType={reportType}
                allCategoryWiseSales={allCategoryWiseSales}
                allCategoryWiseLoading={allCategoryWiseLoading}
                allSubCategoryWiseSales={allSubCategoryWiseSales}
                allSubCategoryWiseLoading={allSubCategoryWiseLoading}
                fromDate={fromDate}
                toDate={toDate}
              />
            ) : selectReport === "Comparative Hourly Sales Summary" &&
              generateButton ? (
              <HourlySaleReport
                hourlySalesList={hourlySalesList}
                hourlySalesLoading={hourlySalesLoading}
                fromDate={fromDate}
                toDate={toDate}
                limit={limit}
              />
            ) : selectReport === "Invoice Wise Hourly Sales Details" &&
              generateButton ? (
              <InvoiceWiseSaleReport
                invoiceSalesList={invoiceSalesList}
                invoiceSalesLoading={invoiceSalesLoading}
                fromDate={fromDate}
                toDate={toDate}
                limit={limit}
              />
            ) : selectReport === "Supplier Wise Product Sales" &&
              generateButton ? (
              <SupplierProductSaleReport
                supplierSalesList={supplierSalesList}
                supplierSalesLoading={supplierSalesLoading}
                selectSupplier={selectSupplier}
                fromDate={fromDate}
                toDate={toDate}
              />
            ) : selectReport === "Category Wise Supplier Sales Summary" &&
              generateButton ? (
              <CategorySupplierSaleReport
                categorySupplierSale={categorySupplierSale}
                categorySupplierLoading={categorySupplierLoading}
                selectCategory={selectCategory}
                fromDate={fromDate}
                toDate={toDate}
              />
            ) : selectReport === "All Supplier Wise Sales Summary" &&
              generateButton ? (
              <SupplierSalesSummary
                allSupplierSummary={allSupplierSummary}
                allSupplierSummaryLoading={allSupplierSummaryLoading}
                fromDate={fromDate}
                toDate={toDate}
              />
            ) : selectReport === "Supplier Wise Category Sales Summary" &&
              generateButton ? (
              <SupplierCategorySaleReport
                supplierCategorySale={supplierCategorySale}
                supplierCategorySaleLoading={supplierCategorySaleLoading}
                fromDate={fromDate}
                toDate={toDate}
              />
            ) : selectReport === "SubCategory Wise Supplier Sales Summary" &&
              generateButton ? (
              <SubCategorySupplierSaleReport
                subcategorySupplierSale={subcategorySupplierSale}
                subcategorySupplierLoading={subcategorySupplierLoading}
                setSelectSubCategory={setSelectSubCategory}
                fromDate={fromDate}
                toDate={toDate}
              />
            ) : selectReport === "Supplier Wise SubCategory Sales Summary" &&
              generateButton ? (
              <SupplierSubCategorySaleReport
                supplierSubcategorySale={supplierSubcategorySale}
                supplierSubcategorySaleLoading={supplierSubcategorySaleLoading}
                fromDate={fromDate}
                toDate={toDate}
              />
            ) : selectReport === "Daily Counter Wise Sales Summary" &&
              generateButton ? (
              <CounterSalesSummary
                dailyCounterSales={dailyCounterSales}
                dailyCounterLoading={dailyCounterLoading}
                selectCounter={selectCounter}
                fromDate={fromDate}
                toDate={toDate}
              />
            ) : selectReport === "Daily Counter Wise Product Exchange" &&
              generateButton ? (
              <CounterExchangeSummary
                dailyCounterExchange={dailyCounterExchange}
                dailyExchangeLoading={dailyExchangeLoading}
                selectCounter={selectCounter}
                fromDate={fromDate}
                toDate={toDate}
              />
            ) : (
              <></>
            )}
          </div>
        </Container>
      </div>
      <Footer></Footer>
    </div>
  );
};

export default SalesReports;
