import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import { useSelector } from "react-redux";
import BestItemAllTime from "../../components/Dashboard/BestItemAllTime/BestItemAllTime";
import OnlineService from "../../components/Dashboard/OnlineServiceOverview/OnlineService";
import ProcurementOverview from "../../components/Dashboard/ProcurementOverview/ProcurementOverview";
import ProductLocation from "../../components/Dashboard/ProductLocation/ProductLocation";
import SalesAnalysis from "../../components/Dashboard/SalesAnalysis/SalesAnalysis";
import SalesOverview from "../../components/Dashboard/SalesOverview/SalesOverview";
import Footer from "../../components/Footer/Footer";
import Header from "../../components/Header/Header";
import SideBar from "../../components/Sidebar/Sidebar";
import "./Dashboard.css";
import SalesAnalysisANDBestItemAllTime from "../../components/Dashboard/SalesAnalysisANDBestItemAllTime";

const Dashboard = () => {
  const { status } = useSelector((state) => state.DahboardOverlay);

  return (
    <>
      <div className="position-relative">
        <SideBar />
        <div className="mainContent">
          <Header />
          <Container fluid className="mb-4 ">
            <SalesOverview />
            <ProductLocation />

            {/* <div className="container-view">
              <Row>
                <Col xl={6} className="sectionMargin">
                  <SalesAnalysis></SalesAnalysis>
                </Col>
                <Col xl={6} className="sectionMargin">
                  <BestItemAllTime></BestItemAllTime>
                </Col>
              </Row>
            </div>  */}
            <SalesAnalysisANDBestItemAllTime></SalesAnalysisANDBestItemAllTime>

            <OnlineService />
            <ProcurementOverview />
          </Container>
        </div>
        <Footer></Footer>

        {status && (
          <div
            className="h-100 w-100 position-absolute"
            style={{
              zIndex: "2",
              backgroundColor: "#000000",
              top: "0px",
              opacity: "0.5",
            }}
          ></div>
        )}
      </div>
    </>
  );
};

export default Dashboard;
