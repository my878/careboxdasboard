import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useLocation, useNavigate } from "react-router-dom";
import { Row, Col, Button, Spinner } from "react-bootstrap";
import logo from "../../assets/logo/care-box-logo.png";
import loginSideImage from "../../assets/icons/landingPage.png";
import { login } from "../../redux/actions/userActions";
import "./Login.css";

const Login = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const dispatch = useDispatch();

  const { loading, userInfo, error } = useSelector((state) => state.userLogin);
  const [name, setName] = useState("");
  const [password, setPassword] = useState("");
  const [errorAlert, setErrorAlert] = useState(false);

  const onSignInSubmit = (e) => {
    e.preventDefault();
    dispatch(login(name, password));
  };

  useEffect(() => {
    if (error) {
      navigate("/login");
      localStorage.setItem("url", location.pathname);
      //window.location.reload();
      setErrorAlert(true);
      //console.log(error);
    } else if (userInfo) {
      navigate("/", { replace: true });
      //console.log(userInfo);
    } else {
      // history.push("/home");
      // history.replace(from);
      // dispatch(getUserDetails());
      setErrorAlert(false);
    }
  }, [error, userInfo]);

  return (
    <div
      style={{
        height: "100vh",
        overflow: "hidden",
        background: "rgba(255, 153, 0, 0.1)",
      }}
    >
      <Row>
        <Col className="d-none d-lg-block" xl={6} lg={6} md={0}>
          <img
            className="img-fluid"
            src={loginSideImage}
            alt="login"
            style={{ height: "100vh", width: "50vw", background: "white" }}
          />
        </Col>
        <Col
          xl={6}
          lg={6}
          md={12}
          className="login-container d-flex flex-column justify-content-center align-items-center"
        >
          {errorAlert ? (
            <div className="alert alert-danger fade show" role="alert">
              <p className="text-center">
                <i className="bi bi-exclamation-triangle-fill"></i> Request
                Failed!! Give Correct Informations !!
              </p>
            </div>
          ) : (
            <div></div>
          )}
          <p
            className="mt-2 text-center"
            style={{ fontWeight: "700", fontSize: "32px", wordSpacing: "5px" }}
          >
            Sales Management System Login
          </p>
          <form className="form_container" onSubmit={onSignInSubmit}>
            <div className="pt-3 pb-3 text-center">
              <input
                type="text"
                placeholder="User Name"
                value={name}
                onChange={(e) => setName(e.target.value)}
                required
              />
            </div>
            <div className="pb-3 text-center">
              <input
                type="password"
                placeholder="Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required
              />
            </div>
            {loading === true ? (
              <>
                <Row className="mb-2 d-flex justify-content-center align-items-center">
                  <Spinner animation="border" variant="warning" />
                </Row>
                <Button variant="secondary" disabled>
                  Login
                </Button>
              </>
            ) : (
              <button className="login-button mb-2" type="submit">
                LOGIN
              </button>
            )}
          </form>
          <div style={{ marginTop: "100px" }}>
            <p
              className="text-center"
              style={{
                letterSpacing: "0px",
                fontSize: "0.75rem",
                marginBottom: "3px",
              }}
            >
              Powered By
            </p>
            <img className="" src={logo} alt="logo" height={60} />
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default Login;
