import React from 'react';
import LoadingModal from '../../components/PopupModal/LoadingModal';
import { useState } from 'react';
import SideBar from '../../components/Sidebar/Sidebar';
import Header from '../../components/Header/Header';
import { Container, Row, Col, Button, Form } from "react-bootstrap";
import Footer from '../../components/Footer/Footer';
import pdfIcon from "../../assets/icons/pdf.png";
import calender from "../../assets/icons/calender.png";
import restrictCalender from "../../assets/icons/restrict-filter.png";
import cancel from "../../assets/icons/cancel.png";
import VoidReportDetails from '../../components/VoidReport/VoidReportDetails';
import VoidSummary from '../../components/VoidReport/VoidSummary';
import { getVoidReportDetails , getVoidSummary } from '../../redux/actions/VoidReportActions';
import { useDispatch, useSelector } from 'react-redux';
import VoidReportPdf from '../../services/VoidReportPdf';

function formatDateToYYYYMMDD(date) {
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, "0"); // Add 1 to the month because it's 0-based
    const day = String(date.getDate()).padStart(2, "0");
  
    return `${year}-${month}-${day}`;
  }

  function convertDateFormat(inputDate) {
    // Split the input date into an array using the '-' delimiter
    var dateArray = inputDate.split('-');
  
    // Rearrange the elements to form the new date format
    var convertedDate = dateArray[2] + '-' + dateArray[1] + '-' + dateArray[0];
  
    // Return the converted date
    return convertedDate;
  }

function VoidReport() {
    const dispatch = useDispatch();    
  const [exportData, setExportData] = useState(false);  
  const [filterDateButton, setFilterDateButton] = useState(false);
  const [fromDate, setFromDate] = useState("");
  const [toDate, setToDate] = useState("");
  const [selectReport, setSelectReport] = useState("");
  const [isContentGenerated, setIsContentGenerated] =  useState(false);
  const [generateContent, setGenerateContent] = useState("");
  const [showReport , setShowReport] = useState(false);

  
  let currentDate = formatDateToYYYYMMDD(new Date());


  const allReportsType = [
    "Void Report Details",
    "Void Summary",
  ];

  const VoidReportDetailsHeader = [
    [      
      {
        content: `From Date : ${fromDate !== "" ? convertDateFormat(fromDate) : "N/A"}`,
        colSpan: 6,
      },
      {
        content: `To Date : ${toDate !== "" ? convertDateFormat(toDate) : "N/A"}`,
        colSpan: 6,
      },
    ],
    [
      "Barcode",
      "Product Information",
      "Sale Date",
      "Void Date",
      "Sold(Qty)",
      "Void(Qty)",
      "Total MRP",
      "Disc. Amount",
      "Vat Amount",
      "Void Amount",
      "Void Status",
      "Void By"
    ],
  ];

  const VoidSummaryHeader = [
    [      
      {
        content: `From Date : ${fromDate !== "" ? fromDate : "N/A"}`,
        colSpan: 6,
      },
      {
        content: `To Date : ${toDate !== "" ? toDate : "N/A"}`,
        colSpan: 6,
      },
    ],
    [
        "Invoice No.",
        "MRP",
        "Disc. Amt.",
        "VAT",
        "Void Amt.",
        "Return Cost",
        "Pft Amt.",
        "GP%",
        "Void Date",
        "I. Time",
        "Ref_Invoice",
        "Void By",
    ],
  ];

//   let reportTable = null;
  
//   if(selectReport === "Void Report Details"){
//     reportTable = <VoidReportDetails fromDate={fromDate} toDate={toDate} />        
//     console.log("Void Report Details");
//   }

  
//   if(selectReport === "Void Summary"){
//     reportTable = <VoidSummary fromDate={fromDate} toDate={toDate} />        
//     console.log("Void Summary");
//   }    

//   const generateHandler = ()=>{
//     console.log("inside",reportTable);
//     if(selectReport === "Void Report Details"){
//         setShowReport(true);
//         reportTable = <VoidReportDetails fromDate={fromDate} toDate={toDate} />        
//         console.log("Void Report Details",reportTable);
//     }
    
      
//       if(selectReport === "Void Summary"){
//         setShowReport(true);
//         reportTable = <VoidSummary fromDate={fromDate} toDate={toDate} />        
//         console.log("Void Summary");
//       }
//   }
// console.log(reportTable);

const generateHandler = ()=>{
    // console.log("yoo");
    if(fromDate && !toDate){
        return alert(`Please Select "To Date"`)
    }
    
    if(!fromDate && toDate){
        return alert(`Please Select "From Date"`)
    }

    if(selectReport === "Void Report Details"){
        dispatch(getVoidReportDetails({fromDate, toDate}));
        setGenerateContent("Void Report Details");
        setIsContentGenerated(true);        
      }
    
      
      if(selectReport === "Void Summary"){
        dispatch(getVoidSummary({fromDate, toDate}));        
        setGenerateContent("Void Summary");
        setIsContentGenerated(true);       
      }
  }

  const {VoidReportDetailsLoading, VoidReportDetailsError, VoidReportDetails: VoidReportDetailsResult } = useSelector((state)=> state.VoidReportDetails);  
  const {VoidSummaryLoading, VoidSummaryError, VoidSummary: VoidSummaryResult} = useSelector((state)=> state.VoidSummary);


const pdfHandler = ()=>{
    // console.log("isContentGenerated = ", isContentGenerated);
    if(isContentGenerated){
        if(generateContent === "Void Report Details"){
            if(VoidReportDetailsLoading){
                alert("Try again after loading is complete")
            }else if(!VoidReportDetailsLoading && VoidReportDetailsResult?.data){
                VoidReportPdf("Void Report Details", VoidReportDetailsHeader, VoidReportDetailsResult.data)
            }else{
                alert("SomeThing went wrong");
            }

        }else if(generateContent === "Void Summary"){
            if(VoidSummaryLoading){
                alert("Try again after loading is complete")
            }else if(!VoidSummaryLoading && VoidSummaryResult?.data){
                VoidReportPdf("Void Summary", VoidSummaryHeader, VoidSummaryResult.data)
            }else{
                alert("SomeThing went wrong");
            }
        }
    }else{
        alert("Please Generate Report First");
    }
}

const selectHandler = (e)=>{
    setSelectReport(e.target.value);
    setGenerateContent("");
    setIsContentGenerated(false);
}


    return (
        <div>            
            <LoadingModal
                show={exportData}
                onHide={() => {
                setExportData(false);
                }}
            />
            <SideBar></SideBar>

            <div className="mainContent">
                <Header></Header>
                    <Container fluid>
                        <h4 className="product-heading container-view">
                            Void Report at a Glance
                        </h4>
                        <p className="product-paragraph container-view">
                            All Details of Void Report
                        </p>
                        
                        <div className="mb-4 report-page-card card product-card container-view">
                            <div className="">
                                <Row className="mb-2">
                                    <Col xl={10}>

                                        <img
                                            src={filterDateButton ? cancel : calender}
                                            alt="calender"
                                            height={45}
                                            style={{ cursor: "pointer" }}
                                            onClick={() => {
                                                if (filterDateButton) {
                                                setFromDate("");
                                                setToDate("");
                                                }
                                                setFilterDateButton(!filterDateButton);
                                            }}
                                        />

                                                            
                                    <select
                                        name="stock-report"
                                        className="select-bar"
                                        onChange={(e) => {
                                            selectHandler(e);
                                        }}
                                        value={selectReport}
                                    >
                                        <option value="" disabled="disabled">Select Reports Type</option>
                                        {allReportsType.map((report, i) => (
                                        <option key={i} value={report}>
                                            {report}
                                        </option>
                                        ))}
                                    </select>

                                    {selectReport !== "" ?(<>

                                    {/* content */}
                                    <Button
                                        className="generate-btn border-0"
                                        type="submit"
                                        onClick={()=>{generateHandler()}}
                                    >
                                        Generate Report
                                    </Button>

                                    </>):(<></>) }
                                        
                                    </Col>

                                    <Col xl={2} className="d-flex justify-content-end">
                                    <img
                                        //className="pe-3"
                                        src={pdfIcon}
                                        alt="pdf"
                                        height={45}
                                        onClick={()=>{pdfHandler()}}
                                        style={{ cursor: "pointer" }}
                                    />
                                    </Col>
                                </Row>

                                <Row>
                                    {filterDateButton && (selectReport !== "") && (<>
                                        <Col lg={2} md={4} sm={6}>
                                            <Form.Group
                                                className="form-group"
                                                controlId="formBasicEmail"
                                            >
                                                <Form.Label className="report-form-label">
                                                From Date
                                                </Form.Label>
                                                <Form.Control
                                                type="date"
                                                placeholder="From Date"
                                                value={fromDate}
                                                max={toDate || currentDate}
                                                onChange={(e) => {
                                                    setFromDate(e.target.value);
                                                    // setToDate("");
                                                    setGenerateContent("");
                                                    setIsContentGenerated(false);
                                                }}
                                                />
                                            </Form.Group>
                                        </Col>

                                        <Col lg={2} md={4} sm={6}>
                                            <Form.Group
                                            className="form-group"
                                            controlId="formBasicEmail"
                                            >
                                            <Form.Label className="report-form-label">
                                                To Date
                                            </Form.Label>
                                            <Form.Control
                                                type="date"
                                                placeholder="To Date"
                                                value={toDate}
                                                onChange={(e) => {
                                                    setToDate(e.target.value)
                                                    setGenerateContent("");
                                                    setIsContentGenerated(false);
                                                }}
                                                min={fromDate}
                                                max={currentDate}
    
                                                //max={fromDate && new Date(fromDate + 2 * 24 * 60 * 60 * 1000)}
                                                //style={{ height: "40px" }}
                                            />
                                            </Form.Group>
                                        </Col>
                                        
                                        </>)}

                                </Row>

                            </div>

                            <div className="">
                                <Row className="mt-3">
                                    <h4 className="product-heading">
                                    {(selectReport === "")?"Select An Option to See Sales Report at a Glance": selectReport}
                                    </h4>
                                </Row>
                            </div>

                            {generateContent === "Void Report Details" ?
                             <VoidReportDetails fromDate={fromDate} toDate={toDate} />
                              : generateContent === "Void Summary" ?  <VoidSummary fromDate={fromDate} toDate={toDate} /> : <></>}


                        </div>
                    </Container>
            </div>

            <Footer></Footer>            
        </div>
    );
}

export default VoidReport;