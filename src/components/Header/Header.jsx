import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import {
  Container,
  Navbar,
  Nav,
  NavDropdown,
} from "react-bootstrap";
import { SidebarAction } from "../../redux/actions/SidebarActions";
import { logout } from "../../redux/actions/userActions";
import userIcon from "../../assets/icons/user.png";
import "./Header.css";
import NavTimer from "./NavTimer";
import toggleIcon from "../../assets/icons/toggle.png";
import screenIcon from "../../assets/icons/screen.png";
import { getUserDetails } from "../../redux/actions/userActions";
import "../Footer/Footer.css"

const Header = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [fullScreen, setFullScreen] = useState(false);
  const { toggleClick } = useSelector((state) => state.sideBarState);
  const { loading, user, error } = useSelector((state) => state.userDetails);
  //console.log(error === Network Error);
  //console.log(user);
    useEffect(() => {
     if(!user?.id){
      dispatch(getUserDetails());
     }
    }, [dispatch,user]);
  useEffect(() => {
    if (toggleClick === true) {
      document.documentElement.style.setProperty(
        "--main-content-margin",
        "0px"
      );
      document.documentElement.style.setProperty("--footer-content", "0px");
    } else {
      document.documentElement.style.setProperty(
        "--main-content-margin",
        "300px"
      );
      document.documentElement.style.setProperty("--footer-content", "300px");
    }
  }, [toggleClick]);
  const handleChnageSidebar = () => {
    if (toggleClick === true) {
      dispatch(SidebarAction(false));
      document.documentElement.style.setProperty("--footer-content", "300px");
      document.documentElement.style.setProperty(
        "--main-content-margin",
        "300px"
      );
    } else {
      dispatch(SidebarAction(true));
      document.documentElement.style.setProperty(
        "--main-content-margin",
        "0px"
      );
      document.documentElement.style.setProperty("--footer-content", "0px");
    }
  };

  const handleScreenSize = () => {
    if (fullScreen === false) {
      setFullScreen(true);
      if (document.documentElement.requestFullscreen) {
        document.documentElement.requestFullscreen();
      } else if (document.documentElement.webkitRequestFullscreen) {
        /* Safari */
        document.documentElement.webkitRequestFullscreen();
      } else if (document.documentElement.msRequestFullscreen) {
        /* IE11 */
        document.documentElement.msRequestFullscreen();
      }
    } else {
      setFullScreen(false);
      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.webkitExitFullscreen) {
        /* Safari */
        document.webkitExitFullscreen();
      } else if (document.msExitFullscreen) {
        /* IE11 */
        document.msExitFullscreen();
      }
    }
  };

  const handleLogout = () => {
    dispatch(logout());
    navigate("/login");
    navigate(0);
  };
  //const { toggleClick } = useSelector((state) => state.sideBarState);
  //console.log(toggleClick);
  // useEffect(() => {
  //   var timer = setInterval(() => setDate(new Date()), 1000);
  //   return function cleanup() {
  //     clearInterval(timer);
  //   };
  // }, []);
  //console.log(localStorage.getItem("menuState"));
  return (
    <>
      <Navbar className="main-content-header">
        <Container fluid className="header-container-view">
          <Navbar.Brand>
            <img
              src={toggleIcon}
              alt="menuIcon"
              //height={40}
              className="brand-logo"
              onClick={handleChnageSidebar}
            />
            <img
              src={screenIcon}
              alt="screenIcon"
              //height={40}
              className="ms-2 ms-sm-4 brand-logo"
              onClick={handleScreenSize}
            />
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse
            id="basic-navbar-nav"
            className="justify-content-end"
          >
            <Nav>
              <Nav.Item
                className="p-2 p-md-3"
                style={{ borderRight: "2px solid #DCE0E4" }}
              >
                {" "}
                <NavTimer></NavTimer>
              </Nav.Item>
              <Nav.Item className="p-md-2 d-flex justify-content-center align-items-center">
                {" "}
                <img
                  src={userIcon}
                  alt="user"
                  //height={65}
                  className="user-icon ps-1 ps-sm-3"
                  //style={{ paddingLeft: "1rem" }}
                />{" "}
              </Nav.Item>
              <Nav.Item
                className="d-flex justify-content-center align-items-center"
                //style={{ paddingRight: "2.5rem" }}
              >
                <NavDropdown
                  //id="nav-dropdown-dark-example"
                  className="nav-dropdown"
                  style={{
                    fontWeight: "600",
                    //fontSize: "20px",
                    //color: "#212529",
                    //paddingLeft: "0.4rem",
                    //paddingRight: "0rem",
                  }}
                  title={user && user.Name ? user.Name : "User Name"}
                  //title={"AdminUser"}
                  //menuVariant="dark"
                >
                  <NavDropdown.Item onClick={handleLogout} className="dropdown-item">
                    Log 0ut
                  </NavDropdown.Item>
                </NavDropdown>
              </Nav.Item>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
};

export default Header;
