import React from "react";
import { Table, Spinner } from "react-bootstrap";

const CounterSalesSummary = (props) => {
  return (
    <>
      <Table className="mt-3" responsive bordered>
        <thead className="text-center report-table-header">
            <tr>
                <th colSpan={5}>Counter No: {props.selectCounter} </th>
                <th colSpan={5}>From Date: {props.fromDate ? props.fromDate : "N/A"} </th>
                <th colSpan={5}>To Date: {props.toDate ? props.toDate : "N/A"} </th>
            </tr>
            <tr>
                <th>Invoice No</th>
                <th>MRP</th>
                <th>Disc. Amount</th>
                <th>Vat</th>
                <th>Exchange</th>
                <th>Net Amount</th>
                <th>Total Cost</th>
                <th>Return Cost</th>
                <th>Cash</th>
                <th>Online</th>
                <th>Card</th>
                <th>Due</th>
                <th>Adj. Amount</th>
                <th>Pft Amount</th>
                <th>GP%</th>
            </tr>
        </thead>
        <tbody className="text-center report-table-body">
          {props.dailyCounterSales.data && props.dailyCounterSales.data.counter_data && props.dailyCounterSales.data.counter_data.length && !props.dailyCounterLoading ? (
            <>
              {props.dailyCounterSales.data.counter_data.map((prod, i) => (
                <tr key={i}>
                  <td>{prod.invoice_id ? prod.invoice_id : "N/A"}</td>
                  <td>{prod.total_mrp}</td>
                  <td>{prod.discount_amount}</td>
                  <td>{prod.vat_amount}</td>
                  <td>{prod.exchange_amount}</td>
                  <td>{prod.net_amount}</td>
                  <td>{prod.total_cost}</td>
                  <td>{prod.exchange_amount}</td>
                  <td>{prod.cash_paid}</td>
                  <td>{prod.online_paid}</td>
                  <td>{prod.card_paid}</td>
                  <td>{prod.due}</td>
                  <td>{prod.less_amount}</td>
                  <td>{prod.pft_amount}</td>
                  <td>{prod.gross_profit}</td>
                </tr>
              ))}
              <tr className="grand-total-row">
                <td className="text-start">Grand Total</td>
                <td>{props.dailyCounterSales.data.counter_total[0].total_mrp}</td>
                <td>{props.dailyCounterSales.data.counter_total[0].discount_amount}</td>
                <td>{props.dailyCounterSales.data.counter_total[0].vat_amount}</td>
                <td>{props.dailyCounterSales.data.counter_total[0].exchange_amount}</td>
                <td>{props.dailyCounterSales.data.counter_total[0].net_amount}</td>
                <td>{props.dailyCounterSales.data.counter_total[0].total_cost}</td>
                <td>{props.dailyCounterSales.data.counter_total[0].exchange_amount}</td>
                <td>{props.dailyCounterSales.data.counter_total[0].cash_paid}</td>
                <td>{props.dailyCounterSales.data.counter_total[0].online_paid}</td>
                <td>{props.dailyCounterSales.data.counter_total[0].card_paid}</td>
                <td>{props.dailyCounterSales.data.counter_total[0].due}</td>
                <td>{props.dailyCounterSales.data.counter_total[0].less_amount}</td>
                <td>{props.dailyCounterSales.data.counter_total[0].pft_amount}</td>
                <td>{props.dailyCounterSales.data.counter_total[0].gross_profit}</td>
              </tr>
            </>
          ) : props.dailyCounterLoading ? (
            <tr>
              <td colSpan={10}>
                <Spinner animation="border" variant="warning" />
              </td>
            </tr>
          ) : (
            <tr>
              <td colSpan={10}>No Data Found</td>
            </tr>
          )}
        </tbody>
      </Table>
    </>
  );
};

export default CounterSalesSummary;
