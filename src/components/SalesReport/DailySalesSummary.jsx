import React, { useEffect, useState } from "react";
import { Table, Spinner } from "react-bootstrap";

const DailySalesSummary = (props) => {
  return (
    <>
      <Table className="mt-3" responsive bordered>
        <thead className="text-center report-table-header">
          <tr>
            <th colSpan={3}>
              From Date: {props.fromDate ? props.fromDate : "N/A"}{" "}
            </th>
            <th colSpan={2}>To Date: {props.toDate ? props.toDate : "N/A"} </th>
          </tr>
          <tr>
            <th>Sale Date</th>
            <th>Sales Amount</th>
            <th>Without (Return + VAT)</th>
            <th>Return + VAT</th>
            <th>Cumulative Sales</th>
          </tr>
        </thead>
        <tbody className="text-center report-table-body">
          {props.resultData.length && !props.dailyLoading ? (
            <>
              {props.resultData.map((prod, i) => (
                <tr key={i}>
                  <td>{prod.sales_date}</td>
                  <td>{prod.sales_amount}</td>
                  <td>{prod.less_return_vat}</td>
                  <td>{prod.return_vat}</td>
                  <td>
                    {prod.cumulativeSale ? prod.cumulativeSale.toFixed(2) : 0}
                  </td>
                </tr>
              ))}
              <tr className="grand-total-row">
                <td className="text-start">Grand Total</td>
                <td>{props.dailySales.data.grand_total.total_sales_amount}</td>
                <td>
                  {props.dailySales.data.grand_total.total_less_return_vat}
                </td>
                <td>{props.dailySales.data.grand_total.total_return_vat}</td>
                <td>{props.dailySales.data.grand_total.total_sales_amount}</td>
              </tr>
            </>
          ) : props.dailyLoading ? (
            <tr>
              <td colSpan={10}>
                <Spinner animation="border" variant="warning" />
              </td>
            </tr>
          ) : (
            <tr>
              <td colSpan={10}>No Data Found</td>
            </tr>
          )}
        </tbody>
      </Table>
    </>
  );
};

export default DailySalesSummary;
