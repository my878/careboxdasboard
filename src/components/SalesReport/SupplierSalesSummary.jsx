import React from "react";
import { Table, Spinner } from "react-bootstrap";

const SupplierSalesSummary = (props) => {
  return (
    <>
      <Table className="mt-3" responsive bordered>
        <thead className="text-center report-table-header">
            <tr>
                <th colSpan={5}>From Date: {props.fromDate ? props.fromDate : "N/A"} </th>
                <th colSpan={5}>To Date: {props.toDate ? props.toDate : "N/A"} </th>
            </tr>
            <tr>
                <th>Supplier Name</th>
                <th>Sold(Qty)</th>
                <th>Return(Qty)</th>
                <th>Total MRP</th>
                <th>Discount Amount</th>
                <th>Vat Amount</th>
                <th>Ex/Ret Amount</th>
                <th>Net Amount</th>
                <th>Pft Amount</th>
                <th>GP%</th>
            </tr>
        </thead>
        <tbody className="text-center report-table-body">
          {props.allSupplierSummary.data && props.allSupplierSummary.data.supplier_data && props.allSupplierSummary.data.supplier_data.length && !props.allSupplierSummaryLoading ? (
            <>
              {props.allSupplierSummary.data.supplier_data.map((prod, i) => (
                <tr key={i}>
                  <td>{prod.supplier_name ? prod.supplier_name : "N/A"}</td>
                  <td>{prod.sold_qty}</td>
                  <td>{prod.exchange_qty}</td>
                  <td>{prod.total_mrp}</td>
                  <td>{prod.discount_amount}</td>
                  <td>{prod.vat_amount}</td>
                  <td>{prod.exchange_amount}</td>
                  <td>{prod.net_amount}</td>
                  <td>{prod.pft_amount}</td>
                  <td>{prod.gross_profit}</td>
                </tr>
              ))}
              <tr className="grand-total-row">
                <td className="text-start">Grand Total</td>
                <td>{props.allSupplierSummary.data.sub_total.total_sold_qty}</td>
                <td>{props.allSupplierSummary.data.sub_total.total_exchange_qty}</td>
                <td>{props.allSupplierSummary.data.sub_total.sub_total_mrp}</td>
                <td>{props.allSupplierSummary.data.sub_total.sub_total_discount}</td>
                <td>{props.allSupplierSummary.data.sub_total.sub_total_vat}</td>
                <td>{props.allSupplierSummary.data.sub_total.sub_total_exchange}</td>
                <td>{props.allSupplierSummary.data.sub_total.total_net_amount}</td>
                <td>{props.allSupplierSummary.data.sub_total.total_pft_amount}</td>
                <td>{props.allSupplierSummary.data.sub_total.total_gross_profit}</td>
              </tr>
            </>
          ) : props.allSupplierSummaryLoading ? (
            <tr>
              <td colSpan={10}>
                <Spinner animation="border" variant="warning" />
              </td>
            </tr>
          ) : (
            <tr>
              <td colSpan={10}>No Data Found</td>
            </tr>
          )}
        </tbody>
      </Table>
    </>
  );
};

export default SupplierSalesSummary;
