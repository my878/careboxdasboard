import React, { Fragment } from "react";
import { Table, Spinner, Row, Col } from "react-bootstrap";

const SubCategoryProductSaleReport = (props) => {
  return (
    <>
      {props.subcategorySalesList.data &&
      props.subcategorySalesList.data.sub_total &&
      props.subcategorySalesList.data.sub_total.length &&
      !props.subcategorySalesLoading ? (
        <>
          {props.subcategorySalesList.data.sub_total.map(
            (subcategory, index) => (
              <Table key={index} className="mt-3" responsive bordered>
                <thead className="text-center report-table-header">
                  <tr>
                    <th colSpan={6}>
                      Sub Category Name: {subcategory.product_sub_category}
                    </th>
                    <th colSpan={3}>
                      From Date: {props.fromDate ? props.fromDate : "N/A"}{" "}
                    </th>
                    <th colSpan={3}>
                      To Date: {props.toDate ? props.toDate : "N/A"}{" "}
                    </th>
                  </tr>
                  <tr>
                    <th>Barcode</th>
                    <th>Sale Date</th>
                    <th>Product Information</th>
                    <th>Sold(Qty)</th>
                    <th>Total MRP</th>
                    <th>Discount Amount</th>
                    <th>Vat Amount</th>
                    <th>Ex/Ret Amount</th>
                    <th>Total Cost</th>
                    <th>Net Amount</th>
                    <th>Pft Amount</th>
                    <th>GP%</th>
                  </tr>
                </thead>
                <tbody className="text-center report-table-body">
                  <>
                    {props.subcategorySalesList.data.sub_category_data.map(
                      (prod, i) =>
                        prod.product_sub_category ===
                        subcategory.product_sub_category ? (
                          <tr key={i}>
                            <td>{prod.barcode ? prod.barcode : "N/A"}</td>
                            <td>{prod.sales_date}</td>
                            <td>
                              {prod.product_name} {prod.unit}
                            </td>
                            <td>{prod.sold_qty}</td>
                            <td>{prod.total_mrp}</td>
                            <td>{prod.discount_amount}</td>
                            <td>{prod.vat_amount}</td>
                            <td>{prod.exchange_amount}</td>
                            <td>{prod.total_cost}</td>
                            <td>{prod.net_amount}</td>
                            <td>{prod.pft_amount}</td>
                            <td>{prod.gross_profit}</td>
                          </tr>
                        ) : (
                          <Fragment key={i + 1000}></Fragment>
                        )
                    )}
                    <tr className="grand-total-row">
                      <td className="text-start">Grand Total</td>
                      <td></td>
                      <td></td>
                      <td>{subcategory.sold_qty}</td>
                      <td>{subcategory.total_mrp}</td>
                      <td>{subcategory.discount_amount}</td>
                      <td>{subcategory.vat_amount}</td>
                      <td>{subcategory.exchange_amount}</td>
                      <td>{subcategory.total_cost}</td>
                      <td>{subcategory.net_amount}</td>
                      <td>{subcategory.pft_amount}</td>
                      <td>{subcategory.gross_profit}</td>
                    </tr>
                  </>
                </tbody>
              </Table>
            )
          )}
        </>
      ) : props.subcategorySalesLoading ? (
        <div className="transfer-table" style={{ border: "1px solid #dee2e6" }}>
          <Row className="p-2">
            <Col className="d-flex justify-content-center">
              <Spinner animation="border" variant="warning" />
            </Col>
          </Row>
        </div>
      ) : (
        <div className="transfer-table" style={{ border: "1px solid #dee2e6" }}>
          <Row className="p-2">
            <Col className="d-flex justify-content-center">No Data Found</Col>
          </Row>
        </div>
      )}
    </>
  );
};

export default SubCategoryProductSaleReport;
