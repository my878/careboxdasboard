import React from "react";
import { Table, Spinner } from "react-bootstrap";

const SupplierProductSaleReport = (props) => {
  return (
    <>
      <Table className="mt-3" responsive bordered>
        <thead className="text-center report-table-header">
            <tr>
                <th colSpan={3}>Supplier Name: {props.supplierSalesList.data && props.supplierSalesList.data.sub_total.length ? props.supplierSalesList.data.sub_total[0].supplier_name : "---"}</th>
                <th colSpan={3}>Supplier ID: {props.selectSupplier}</th>
                <th colSpan={3}>From Date: {props.fromDate ? props.fromDate : "N/A"} </th>
                <th colSpan={3}>To Date: {props.toDate ? props.toDate : "N/A"} </th>
            </tr>
            <tr>
                <th>Barcode</th>
                <th>Product Information</th>
                <th>Sale Date</th>
                <th>Sold(Qty)</th>
                <th>Total MRP</th>
                <th>Disc. Amount</th>
                <th>Vat Amount</th>
                <th>Ex/Ret Amount</th>
                <th>Total Cost</th>
                <th>Net Amount</th>
                <th>Pft Amount</th>
                <th>GP%</th>
            </tr>
        </thead>
        <tbody className="text-center report-table-body">
          {props.supplierSalesList.data && props.supplierSalesList.data.supplier_data && props.supplierSalesList.data.supplier_data.length && !props.supplierSalesLoading ? (
            <>
              {props.supplierSalesList.data.supplier_data.map((prod, i) => (
                <tr key={i}>
                  <td>{prod.product_barcode ? prod.product_barcode : "N/A"}</td>
                  <td className="text-start">
                    {prod.product_name} {prod.generic_name} {prod.product_unit}
                  </td>
                  <td>{prod.sales_date}</td>
                  <td>{prod.sold_qty}</td>
                  <td>{prod.total_mrp}</td>
                  <td>{prod.discount_amount}</td>
                  <td>{prod.vat_amount}</td>
                  <td>{prod.exchange_amount}</td>
                  <td>{prod.total_cost}</td>
                  <td>{prod.net_amount}</td>
                  <td>{prod.pft_amount}</td>
                  <td>{prod.gross_profit}</td>
                </tr>
              ))}
              <tr className="grand-total-row">
                <td className="text-start">Grand Total</td>
                <td></td>
                <td></td>
                <td>{props.supplierSalesList.data.sub_total[0].sold_qty}</td>
                <td>{props.supplierSalesList.data.sub_total[0].total_mrp}</td>
                <td>{props.supplierSalesList.data.sub_total[0].discount_amount}</td>
                <td>{props.supplierSalesList.data.sub_total[0].vat_amount}</td>
                <td>{props.supplierSalesList.data.sub_total[0].exchange_amount}</td>
                <td>{props.supplierSalesList.data.sub_total[0].total_cost}</td>
                <td>{props.supplierSalesList.data.sub_total[0].net_amount}</td>
                <td>{props.supplierSalesList.data.sub_total[0].pft_amount}</td>
                <td>{props.supplierSalesList.data.sub_total[0].gross_profit}</td>
              </tr>
            </>
          ) : props.supplierSalesLoading ? (
            <tr>
              <td colSpan={12}>
                <Spinner animation="border" variant="warning" />
              </td>
            </tr>
          ) : (
            <tr>
              <td colSpan={12}>No Data Found</td>
            </tr>
          )}
        </tbody>
      </Table>
    </>
  );
};

export default SupplierProductSaleReport;
