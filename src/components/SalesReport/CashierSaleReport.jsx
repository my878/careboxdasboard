import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { Table, Spinner } from "react-bootstrap";
import ReactPaginate from "react-paginate";
import { getCashierSaleReport } from "../../redux/actions/SalesReportActions";

const CashierSaleReport = (props) => {
  const dispatch = useDispatch();
  const [totalItems, setTotalItems] = useState(0);
  useEffect(() => {
    if (props.cashierWiseSales.data && props.cashierWiseSales.data.products_data) {
      setTotalItems(props.cashierWiseSales.data.products_data.count);
    }
  }, [props.cashierWiseSales]);
  const pageCount = Math.ceil(totalItems / props.limit);

  const handlePageClick = (event) => {
    const newOffset = (event.selected * props.limit) % totalItems;
    if(props.fromDate !== "" && props.toDate !== ""){
        dispatch(getCashierSaleReport(`/?limit=${props.limit}&offset=${newOffset}&cashier_id=${props.selectCashier}&from_date=${fromDate}&to_date=${toDate}`))
      }else{
        dispatch(getCashierSaleReport(`/?limit=${props.limit}&offset=${newOffset}&cashier_id=${props.selectCashier}`))
      }
  };

  return (
    <>
      <Table className="mt-3" responsive bordered>
        <thead className="text-center report-table-header">
            <tr>
                <th colSpan={3}>Cashier Name: {props.cashierName}</th>
                <th colSpan={2}>Cashier ID: {props.selectCashier}</th>
                <th colSpan={2}>From Date: {props.fromDate ? props.fromDate : "N/A"} </th>
                <th colSpan={2}>To Date: {props.toDate ? props.toDate : "N/A"} </th>
            </tr>
            <tr>
                <th>Invoice No.</th>
                <th>Total Amount</th>
                <th>Disc. Amount</th>
                <th>Net Amount</th>
                <th>Cash Amount</th>
                <th>Online Amount</th>
                <th>Card Amount</th>
                <th>Due Amount</th>
                <th>Payment Method</th>
            </tr>
        </thead>
        <tbody className="text-center report-table-body">
          {props.cashierWiseSales.data && props.cashierWiseSales.data.products_data && props.cashierWiseSales.data.products_data.results.length && !props.cashierWiseLoading ? (
            <>
              {props.cashierWiseSales.data.products_data.results.map((prod, i) => (
                <tr key={i}>
                  <td>
                    {prod.invoice_number ? prod.invoice_number : prod.id} 
                  </td>
                  <td>{prod.total_amount}</td>
                  <td>{prod.discount_amount}</td>
                  <td>{prod.net_amount}</td>
                  <td>{prod.cash_paid}</td>
                  <td>{prod.online_paid}</td>
                  <td>{prod.card_paid}</td>
                  <td>{prod.due}</td>
                  <td>{prod.payment_method}</td>
                </tr>
              ))}
              <tr className="grand-total-row">
                <td className="">Grand Total</td>
                <td>{props.cashierWiseSales.data.grand_total.grand_total}</td>
                <td>{props.cashierWiseSales.data.grand_total.grand_discount}</td>
                <td>{props.cashierWiseSales.data.grand_total.grand_net}</td>
                <td>{props.cashierWiseSales.data.grand_total.grand_cash}</td>
                <td>{props.cashierWiseSales.data.grand_total.grand_online}</td>
                <td>{props.cashierWiseSales.data.grand_total.grand_card}</td>
                <td>{props.cashierWiseSales.data.grand_total.grand_due}</td>
                <td></td>
              </tr>
            </>
          ) : props.cashierWiseLoading ? (
            <tr>
              <td colSpan={9}>
                <Spinner animation="border" variant="warning" />
              </td>
            </tr>
          ) : (
            <tr>
              <td colSpan={9}>No Data Found</td>
            </tr>
          )}
        </tbody>
      </Table>
      {pageCount > 1 && !props.cashierWiseLoading ? (
        <ReactPaginate
          breakLabel="..."
          nextLabel=""
          onPageChange={handlePageClick}
          pageRangeDisplayed={3}
          marginPagesDisplayed={1}
          pageCount={pageCount}
          previousLabel=""
          renderOnZeroPageCount={null}
          containerClassName="pagination"
          pageLinkClassName="page-num"
          previousClassName="hide"
          nextClassName="hide"
          activeLinkClassName="active"
        />
      ) : (
        <></>
      )}
    </>
  );
};

export default CashierSaleReport;
