import React from "react";
import { Table, Spinner } from "react-bootstrap";

const CounterExchangeSummary = (props) => {
  return (
    <>
      <Table className="mt-3" responsive bordered>
        <thead className="text-center report-table-header">
            <tr>
                <th colSpan={4}>Counter No: {props.selectCounter} </th>
                <th colSpan={3}>From Date: {props.fromDate ? props.fromDate : "N/A"} </th>
                <th colSpan={3}>To Date: {props.toDate ? props.toDate : "N/A"} </th>
            </tr>
            <tr>
                <th>Invoice No</th>
                <th>Barcode</th>
                <th>Product Information</th>
                <th>Sale Date</th>
                <th>Qty</th>
                <th>Total MRP</th>
                <th>Disc. Amount Per Product</th>
                <th>Vat Amount</th>
                <th>Total Return Amount</th>
            </tr>
        </thead>
        <tbody className="text-center report-table-body">
          {props.dailyCounterExchange.data && props.dailyCounterExchange.data.counter_data && props.dailyCounterExchange.data.counter_data.length && !props.dailyExchangeLoading ? (
            <>
              {props.dailyCounterExchange.data.counter_data.map((prod, i) => (
                <tr key={i}>
                  <td>{prod.invoice_id ? prod.invoice_id : "N/A"}</td>
                  <td>{prod.total_mrp}</td>
                  <td>{prod.discount_amount}</td>
                  <td>{prod.vat_amount}</td>
                  <td>{prod.exchange_amount}</td>
                  <td>{prod.net_amount}</td>
                  <td>{prod.total_cost}</td>
                  <td>{prod.pft_amount}</td>
                  <td>{prod.gross_profit}</td>
                </tr>
              ))}
              <tr className="grand-total-row">
                <td className="text-start">Grand Total</td>
                <td>{props.dailyCounterExchange.data.counter_total.total_mrp}</td>
                <td>{props.dailyCounterExchange.data.counter_total.discount_amount}</td>
                <td>{props.dailyCounterExchange.data.counter_total.vat_amount}</td>
                <td>{props.dailyCounterExchange.data.counter_total.exchange_amount}</td>
                <td>{props.dailyCounterExchange.data.counter_total.net_amount}</td>
                <td>{props.dailyCounterExchange.data.counter_total.total_cost}</td>
                <td>{props.dailyCounterExchange.data.counter_total.pft_amount}</td>
                <td>{props.dailyCounterExchange.data.counter_total.gross_profit}</td>
              </tr>
            </>
          ) : props.dailyExchangeLoading ? (
            <tr>
              <td colSpan={10}>
                <Spinner animation="border" variant="warning" />
              </td>
            </tr>
          ) : (
            <tr>
              <td colSpan={10}>No Data Found</td>
            </tr>
          )}
        </tbody>
      </Table>
    </>
  );
};

export default CounterExchangeSummary;
