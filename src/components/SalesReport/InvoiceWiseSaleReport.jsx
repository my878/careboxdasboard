import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { Table, Spinner, Row, Col } from "react-bootstrap";
import ReactPaginate from "react-paginate";
import { getInvoiceWiseHourlySaleReport } from "../../redux/actions/SalesReportActions";

const InvoiceWiseSaleReport = (props) => {
  const dispatch = useDispatch();
  const [totalItems, setTotalItems] = useState(0);
  useEffect(() => {
    if (props.invoiceSalesList.data) {
      setTotalItems(props.invoiceSalesList.data.count);
    }
  }, [props.invoiceSalesList]);
  const pageCount = Math.ceil(totalItems / props.limit);

  const handlePageClick = (event) => {
    const newOffset = (event.selected * props.limit) % totalItems;
    if (props.fromDate !== "" && props.toDate !== "") {
      dispatch(
        getInvoiceWiseHourlySaleReport(
          `&limit=${props.limit}&offset=${newOffset}&date_range_after=${props.fromDate}&date_range_before=${props.toDate}`
        )
      );
    } else {
      alert("Please Select Date Range");
    }
  };
  return (
    <>
      <div className="mt-2 transfer-table">
        <Row
          className="mx-0 py-2 table-header"
          style={{ border: "1px solid #dee2e6" }}
        >
          <Col lg={6}>
            <b>From Date: </b> {props.fromDate ? props.fromDate : "N/A"}
          </Col>
          <Col lg={6}>
            <b>To Date: </b> {props.toDate ? props.toDate : "N/A"}
          </Col>
        </Row>
      </div>
      {props.invoiceSalesList &&
      props.invoiceSalesList.data &&
      props.invoiceSalesList.data.results &&
      props.invoiceSalesList.data.results.length &&
      !props.invoiceSalesLoading ? (
        props.invoiceSalesList.data.results.map((data, index) => (
          <Table key={index} className="" responsive bordered>
            <thead className="text-center report-table-header">
              <tr>
                <th>Invoice No.</th>
                <th>MRP</th>
                <th>Disc. Amount</th>
                <th>Vat</th>
                <th>Exchange</th>
                <th>Net Amount</th>
                <th>Cash</th>
                <th>Online</th>
                <th>Card</th>
                <th>Due</th>
                <th>Total Cost</th>
                <th>Return Cost</th>
                <th>Pft Amount</th>
                <th>GP%</th>
                <th>I.Time</th>
              </tr>
            </thead>
            <tbody className="text-center report-table-body">
              <>
                {data.invoices.map((prod, i) => (
                  <tr key={i}>
                    <td>{prod.invoice_id ? prod.invoice_id : prod.id}</td>
                    <td>{prod.mrp}</td>
                    <td>{prod.discount_amount}</td>
                    <td>{prod.vat_amount}</td>
                    <td>{prod.exchange_amount}</td>
                    <td>{prod.net_amount}</td>
                    <td>{prod.cash}</td>
                    <td>{prod.online}</td>
                    <td>{prod.card}</td>
                    <td>{prod.due}</td>
                    <td>{prod.total_cost}</td>
                    <td>{prod.return_cost}</td>
                    <td>{prod.profit_amount}</td>
                    <td>{prod.gross_profit_percent}</td>
                    <td>{prod.invoice_time.slice(0, 8)}</td>
                  </tr>
                ))}
                <tr className="grand-total-row">
                  <td className="text-start">({data.hour}) Grand Total</td>
                  <td>{data.grand.grand_mrp}</td>
                  <td>{data.grand.grand_discount_amount}</td>
                  <td>{data.grand.grand_vat_amount}</td>
                  <td>{data.grand.grand_exchange_amount}</td>
                  <td>{data.grand.grand_net_amount}</td>
                  <td>{data.grand.grand_cash}</td>
                  <td>{data.grand.grand_online}</td>
                  <td>{data.grand.grand_card}</td>
                  <td>{data.grand.grand_due}</td>
                  <td>{data.grand.grand_total_cost}</td>
                  <td>{data.grand.grand_return_cost}</td>
                  <td>{data.grand.grand_profit_amount}</td>
                  <td>{data.grand.grand_gross_profit_percent}</td>
                  <td></td>
                </tr>
              </>
            </tbody>
          </Table>
        ))
      ) : props.invoiceSalesLoading ? (
        <div className="transfer-table" style={{ border: "1px solid #dee2e6" }}>
          <Row className="p-2">
            <Col className="d-flex justify-content-center">
              <Spinner animation="border" variant="warning" />
            </Col>
          </Row>
        </div>
      ) : (
        <div className="transfer-table" style={{ border: "1px solid #dee2e6" }}>
          <Row className="p-2">
            <Col className="d-flex justify-content-center">No Data Found</Col>
          </Row>
        </div>
      )}
      {pageCount > 1 && !props.invoiceSalesLoading ? (
        <ReactPaginate
          breakLabel="..."
          nextLabel=""
          onPageChange={handlePageClick}
          pageRangeDisplayed={3}
          marginPagesDisplayed={1}
          pageCount={pageCount}
          previousLabel=""
          renderOnZeroPageCount={null}
          containerClassName="pagination"
          pageLinkClassName="page-num"
          previousClassName="hide"
          nextClassName="hide"
          activeLinkClassName="active"
        />
      ) : (
        <></>
      )}
    </>
  );
};

export default InvoiceWiseSaleReport;
