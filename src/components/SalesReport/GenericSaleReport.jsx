import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { Table, Spinner } from "react-bootstrap";
import ReactPaginate from "react-paginate";
import { getGenericSaleReport } from "../../redux/actions/SalesReportActions";

const GenericSaleReport = (props) => {
  const dispatch = useDispatch();
  const [totalItems, setTotalItems] = useState(0);
  useEffect(() => {
    if (props.genericWiseSales.data && props.genericWiseSales.data.generic_data) {
      setTotalItems(props.genericWiseSales.data.generic_data.count);
    }
  }, [props.genericWiseSales]);
  const pageCount = Math.ceil(totalItems / props.limit);

  const handlePageClick = (event) => {
    const newOffset = (event.selected * props.limit) % totalItems;
    if(props.fromDate !== "" && props.toDate !== ""){
        dispatch(getGenericSaleReport(`/?limit=${props.limit}&offset=${newOffset}&generic_name=${props.searchProduct}&from_date=${fromDate}&to_date=${toDate}`))
      }else{
        dispatch(getGenericSaleReport(`/?limit=${props.limit}&offset=${newOffset}&generic_name=${props.searchProduct}`))
    }
  };
  return (
    <>
      <Table className="mt-3" responsive bordered>
        <thead className="text-center report-table-header">
            <tr>
                <th colSpan={6}>Generic Name: {props.searchProduct}</th>
                <th colSpan={3}>From Date: {props.fromDate ? props.fromDate : "N/A"} </th>
                <th colSpan={3}>To Date: {props.toDate ? props.toDate : "N/A"} </th>
            </tr>
            <tr>
                <th>Barcode</th>
                <th>Sale Date</th>
                <th>Product Information</th>
                <th>Sold Qty</th>
                <th>Total MRP</th>
                <th>Disc. Amount</th>
                <th>Vat Amount</th>
                <th>Ex/Ret Amount</th>
                <th>Total Cost</th>
                <th>Net Amount</th>
                <th>Pft Amount</th>
                <th>GP%</th>
            </tr>
        </thead>
        <tbody className="text-center report-table-body">
          {props.genericWiseSales && props.genericWiseSales.data && props.genericWiseSales.data.generic_data && props.genericWiseSales.data.generic_data.results.length && !props.genericWiseLoading ? (
            <>
              {props.genericWiseSales.data.generic_data.results.map((prod, i) => (
                <tr key={i}>
                  <td>{prod.barcode ? prod.barcode : "N/A"}</td>
                  <td>{prod.sale_date}</td>
                  <td className="text-start">
                    {prod.product_name} {prod.generic_name} {prod.product_unit}
                  </td>
                  <td>{prod.sold_quantity}</td>
                  <td>{prod.mrp_amount}</td>
                  <td>{prod.discount_amount}</td>
                  <td>{prod.vat_amount}</td>
                  <td>{prod.exchange_amount}</td>
                  <td>{prod.cost_amount}</td>
                  <td>{prod.net_amount}</td>
                  <td>{prod.profit_amount}</td>
                  <td>{prod.gp_amount}</td>
                </tr>
              ))}
              <tr className="grand-total-row">
                <td className="text-start">Grand Total</td>
                <td></td>
                <td></td>
                <td>{props.genericWiseSales.data.grand_total.total_sold}</td>
                <td>{props.genericWiseSales.data.grand_total.total_mrp}</td>
                <td>{props.genericWiseSales.data.grand_total.total_discount}</td>
                <td>{props.genericWiseSales.data.grand_total.total_vat}</td>
                <td>{props.genericWiseSales.data.grand_total.total_exchange}</td>
                <td>{props.genericWiseSales.data.grand_total.total_cost}</td>
                <td>{props.genericWiseSales.data.grand_total.total_amount}</td>
                <td>{props.genericWiseSales.data.grand_total.total_profit}</td>
                <td>{props.genericWiseSales.data.grand_total.total_gp}</td>
              </tr>
            </>
          ) : props.genericWiseLoading ? (
            <tr>
              <td colSpan={12}>
                <Spinner animation="border" variant="warning" />
              </td>
            </tr>
          ) : (
            <tr>
              <td colSpan={12}>No Data Found</td>
            </tr>
          )}
        </tbody>
      </Table>
      {pageCount > 1 && !props.genericWiseLoading ? (
        <ReactPaginate
          breakLabel="..."
          nextLabel=""
          onPageChange={handlePageClick}
          pageRangeDisplayed={3}
          marginPagesDisplayed={1}
          pageCount={pageCount}
          previousLabel=""
          renderOnZeroPageCount={null}
          containerClassName="pagination"
          pageLinkClassName="page-num"
          previousClassName="hide"
          nextClassName="hide"
          activeLinkClassName="active"
        />
      ) : (
        <></>
      )}
    </>
  );
};

export default GenericSaleReport;
