import React from "react";
import { Table, Spinner } from "react-bootstrap";

const AllCategorySaleReport = (props) => {
  return (
    <>
      <Table className="mt-3" responsive bordered>
        <thead className="text-center report-table-header">
          <tr>
            <th colSpan={6}>
              From Date: {props.fromDate ? props.fromDate : "N/A"}{" "}
            </th>
            <th colSpan={6}>To Date: {props.toDate ? props.toDate : "N/A"} </th>
          </tr>
          <tr>
            <th>
              {props.reportType === "Category"
                ? "Category Name"
                : "SubCategory Name"}
            </th>
            <th>Sold Qty</th>
            <th>Return Qty</th>
            <th>Total MRP</th>
            <th>Disc. Amount</th>
            <th>Vat Amount</th>
            <th>Ex/Ret Amount</th>
            <th>Total Cost</th>
            <th>Net Amount</th>
            <th>Pft Amount</th>
            <th>GP%</th>
          </tr>
        </thead>
        <tbody className="text-center report-table-body">
          {props.reportType === "Category" &&
          props.allCategoryWiseSales &&
          props.allCategoryWiseSales.data &&
          props.allCategoryWiseSales.data.category_data &&
          props.allCategoryWiseSales.data.category_data.length &&
          !props.allCategoryWiseLoading ? (
            <>
              {props.allCategoryWiseSales.data.category_data.map((prod, i) => (
                <tr key={i}>
                  <td>{prod.product_category}</td>
                  <td>{prod.sold_qty}</td>
                  <td>{prod.exchange_qty}</td>
                  <td>{prod.total_mrp}</td>
                  <td>{prod.discount_amount}</td>
                  <td>{prod.vat_amount}</td>
                  <td>{prod.exchange_amount}</td>
                  <td>{prod.total_cost}</td>
                  <td>{prod.net_amount}</td>
                  <td>{prod.pft_amount}</td>
                  <td>{prod.gross_profit}</td>
                </tr>
              ))}
              <tr className="grand-total-row">
                <td className="text-start">Grand Total</td>
                <td>
                  {props.allCategoryWiseSales.data.sub_total.total_sold_qty}
                </td>
                <td>
                  {props.allCategoryWiseSales.data.sub_total.total_exchange_qty}
                </td>
                <td>
                  {props.allCategoryWiseSales.data.sub_total.sub_total_mrp}
                </td>
                <td>
                  {props.allCategoryWiseSales.data.sub_total.sub_total_discount}
                </td>
                <td>
                  {props.allCategoryWiseSales.data.sub_total.sub_total_vat}
                </td>
                <td>
                  {props.allCategoryWiseSales.data.sub_total.sub_total_exchange}
                </td>
                <td>
                  {props.allCategoryWiseSales.data.sub_total.sub_total_cost}
                </td>
                <td>
                  {props.allCategoryWiseSales.data.sub_total.total_net_amount}
                </td>
                <td>
                  {props.allCategoryWiseSales.data.sub_total.total_pft_amount}
                </td>
                <td>
                  {props.allCategoryWiseSales.data.sub_total.total_gross_profit}
                </td>
              </tr>
            </>
          ) : props.reportType === "SubCategory" &&
            props.allSubCategoryWiseSales &&
            props.allSubCategoryWiseSales.data &&
            props.allSubCategoryWiseSales.data.subcategory_data &&
            props.allSubCategoryWiseSales.data.subcategory_data.length &&
            !props.allCategoryWiseLoading ? (
            <>
              {props.allSubCategoryWiseSales.data.subcategory_data.map(
                (prod, i) => (
                  <tr key={i}>
                    <td>{prod.sub_category}</td>
                    <td>{prod.sold_qty}</td>
                    <td>{prod.exchange_qty}</td>
                    <td>{prod.total_mrp}</td>
                    <td>{prod.discount_amount}</td>
                    <td>{prod.vat_amount}</td>
                    <td>{prod.exchange_amount}</td>
                    <td>{prod.total_cost}</td>
                    <td>{prod.net_amount}</td>
                    <td>{prod.pft_amount}</td>
                    <td>{prod.gross_profit}</td>
                  </tr>
                )
              )}
              <tr className="grand-total-row">
                <td className="text-start">Grand Total</td>
                <td>
                  {props.allSubCategoryWiseSales.data.sub_total.total_sold_qty}
                </td>
                <td>
                  {
                    props.allSubCategoryWiseSales.data.sub_total
                      .total_exchange_qty
                  }
                </td>
                <td>
                  {props.allSubCategoryWiseSales.data.sub_total.sub_total_mrp}
                </td>
                <td>
                  {
                    props.allSubCategoryWiseSales.data.sub_total
                      .sub_total_discount
                  }
                </td>
                <td>
                  {props.allSubCategoryWiseSales.data.sub_total.sub_total_vat}
                </td>
                <td>
                  {
                    props.allSubCategoryWiseSales.data.sub_total
                      .sub_total_exchange
                  }
                </td>
                <td>
                  {props.allSubCategoryWiseSales.data.sub_total.sub_total_cost}
                </td>
                <td>
                  {
                    props.allSubCategoryWiseSales.data.sub_total
                      .total_net_amount
                  }
                </td>
                <td>
                  {
                    props.allSubCategoryWiseSales.data.sub_total
                      .total_pft_amount
                  }
                </td>
                <td>
                  {
                    props.allSubCategoryWiseSales.data.sub_total
                      .total_gross_profit
                  }
                </td>
              </tr>
            </>
          ) : props.allCategoryWiseLoading ||
            props.allSubCategoryWiseLoading ? (
            <tr>
              <td colSpan={12}>
                <Spinner animation="border" variant="warning" />
              </td>
            </tr>
          ) : (
            <tr>
              <td colSpan={12}>No Data Found</td>
            </tr>
          )}
        </tbody>
      </Table>
    </>
  );
};

export default AllCategorySaleReport;
