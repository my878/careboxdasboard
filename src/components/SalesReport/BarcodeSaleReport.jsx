import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { Table, Spinner } from "react-bootstrap";
import ReactPaginate from "react-paginate";
import { getBarcodeSaleReport } from "../../redux/actions/SalesReportActions";

const BarcodeSaleReport = (props) => {
  const dispatch = useDispatch();
  const [totalItems, setTotalItems] = useState(0);
  useEffect(() => {
    if (props.barcodeSales.data && props.barcodeSales.data.products_data) {
      setTotalItems(props.barcodeSales.data.products_data.count);
    }
  }, [props.barcodeSales]);
  const pageCount = Math.ceil(totalItems / props.limit);

  const handlePageClick = (event) => {
    const newOffset = (event.selected * props.limit) % totalItems;
    if(props.fromDate !== "" && props.toDate !== ""){
        dispatch(getBarcodeSaleReport(`/?limit=${props.limit}&offset=${newOffset}&barcode=${props.searchProduct}&from_date=${fromDate}&to_date=${toDate}`))
      }else{
        dispatch(getBarcodeSaleReport(`/?limit=${props.limit}&offset=${newOffset}&barcode=${props.searchProduct}`))
      }
  };

  return (
    <>
      <Table className="mt-3" responsive bordered>
        <thead className="text-center report-table-header">
            <tr>
                <th colSpan={5}>Barcode: {props.searchProduct}</th>
                <th colSpan={3}>From Date: {props.fromDate ? props.fromDate : "N/A"} </th>
                <th colSpan={3}>To Date: {props.toDate ? props.toDate : "N/A"} </th>
            </tr>
            <tr>
                <th>Product Information</th>
                <th>Sale Date</th>
                <th>Sold(Qty)</th>
                <th>Total MRP</th>
                <th>Disc. Amount</th>
                <th>Vat Amount</th>
                <th>Ex/Ret Amount</th>
                <th>Total Cost</th>
                <th>Net Amount</th>
                <th>Pft Amount</th>
                <th>GP%</th>
            </tr>
        </thead>
        <tbody className="text-center report-table-body">
          {props.barcodeSales.data && props.barcodeSales.data.products_data && props.barcodeSales.data.products_data.results.length && !props.barcodeLoading ? (
            <>
              {props.barcodeSales.data.products_data.results.map((prod, i) => (
                <tr key={i}>
                  <td className="text-start">
                    {prod.product_name} {prod.generic_name} {prod.product_unit}
                  </td>
                  <td>{prod.sales_date?.split("T")[0]}</td>
                  <td>{prod.sold_quantity}</td>
                  <td>{prod.total_mrp}</td>
                  <td>{prod.discount_amount}</td>
                  <td>{prod.vat_amount}</td>
                  <td>{prod.return_amount}</td>
                  <td>{prod.total_cost}</td>
                  <td>{prod.net_amount}</td>
                  <td>{prod.profit_amouunt}</td>
                  <td>{prod.gross_profit}</td>
                </tr>
              ))}
              <tr className="grand-total-row">
                <td className="text-start">Grand Total</td>
                <td></td>
                <td>{props.barcodeSales.data.grand_total.total_sold_quantity}</td>
                <td>{props.barcodeSales.data.grand_total.grand_total_mrp}</td>
                <td>{props.barcodeSales.data.grand_total.total_discount_amount}</td>
                <td>{props.barcodeSales.data.grand_total.total_vat_amount}</td>
                <td>{props.barcodeSales.data.grand_total.total_return_amount}</td>
                <td>{props.barcodeSales.data.grand_total.grand_total_cost}</td>
                <td>{props.barcodeSales.data.grand_total.total_net_amount}</td>
                <td>{props.barcodeSales.data.grand_total.total_profit_amount}</td>
                <td>{props.barcodeSales.data.grand_total.total_gross_profit}</td>
              </tr>
            </>
          ) : props.barcodeLoading ? (
            <tr>
              <td colSpan={11}>
                <Spinner animation="border" variant="warning" />
              </td>
            </tr>
          ) : (
            <tr>
              <td colSpan={11}>No Data Found</td>
            </tr>
          )}
        </tbody>
      </Table>
      {pageCount > 1 && !props.barcodeLoading ? (
        <ReactPaginate
          breakLabel="..."
          nextLabel=""
          onPageChange={handlePageClick}
          pageRangeDisplayed={3}
          marginPagesDisplayed={1}
          pageCount={pageCount}
          previousLabel=""
          renderOnZeroPageCount={null}
          containerClassName="pagination"
          pageLinkClassName="page-num"
          previousClassName="hide"
          nextClassName="hide"
          activeLinkClassName="active"
        />
      ) : (
        <></>
      )}
    </>
  );
};

export default BarcodeSaleReport;
