import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { Table, Spinner } from "react-bootstrap";
import ReactPaginate from "react-paginate";
import { getInvoiceSaleReport } from "../../redux/actions/SalesReportActions";

const DailyInvoiceSaleReport = (props) => {
  const dispatch = useDispatch();
  const [totalItems, setTotalItems] = useState(0);
  useEffect(() => {
    if (props.dailyInvoiceSales.data && props.dailyInvoiceSales.data.products_data) {
      setTotalItems(props.dailyInvoiceSales.data.products_data.count);
    }
  }, [props.dailyInvoiceSales]);
  const pageCount = Math.ceil(totalItems / props.limit);

  const handlePageClick = (event) => {
    const newOffset = (event.selected * props.limit) % totalItems;
    if(props.invoiceReportType === "time"){
      dispatch(getInvoiceSaleReport(`/?limit=${props.limit}&offset=${newOffset}&from_date=${props.fromDate}&to_date=${props.fromDate}&from_time=${props.fromTime}:00&to_time=${props.toTime}:00`));
    }else{
      dispatch(getInvoiceSaleReport(`/?limit=${props.limit}&offset=${newOffset}&from_date=${props.fromDate}&to_date=${props.fromDate}`))
    }
  };
  return (
    <>
      <Table className="mt-3" responsive bordered>
        <thead className="text-center report-table-header">
            <tr>
              <th colSpan={8}>From Date: {props.fromDate ? props.fromDate : "N/A"} </th>
              <th colSpan={8}>To Date: {props.fromDate ? props.fromDate : "N/A"} </th>
            </tr>
            <tr>
                <th>Invoice No.</th>
                <th>MRP</th>
                <th>Disc. Amount</th>
                <th>Vat</th>
                <th>Exchange</th>
                <th>Net Amount</th>
                <th>Redeem Point</th>
                <th>Total Cost</th>
                <th>Return Cost</th>
                <th>Cash</th>
                <th>Online</th>
                <th>Card</th>
                <th>Due</th>
                <th>Pft Amount</th>
                <th>GP%</th>
                <th>I.Time</th>
            </tr>
        </thead>
        <tbody className="text-center report-table-body">
          {props.dailyInvoiceSales && props.dailyInvoiceSales.data && props.dailyInvoiceSales.data.products_data && props.dailyInvoiceSales.data.products_data.results.length && !props.dailyInvoiceLoading ? (
            <>
              {props.dailyInvoiceSales.data.products_data.results.map((prod, i) => (
                <tr key={i}>
                  <td>{prod.invoice_number ? prod.invoice_number : "N/A"}</td>
                  <td>{prod.total_mrp}</td>
                  <td>
                    {prod.discount_amount}
                  </td>
                  <td>{prod.vat_amount}</td>
                  <td>{prod.exchange_amount}</td>
                  <td>{prod.net_amount}</td>
                  <td>{prod.redeem_point ? prod.redeem_point : 0}</td>
                  <td>{prod.order_cost}</td>
                  <td>{prod.exchange_cost}</td>
                  <td>{prod.cash_paid}</td>
                  <td>{prod.online_paid}</td>
                  <td>{prod.card_paid}</td>
                  <td>{prod.due}</td>
                  <td>{prod.profit_amount}</td>
                  <td>{prod.gp_amount}</td>
                  <td>{prod.created_time.slice(0, 8)}</td>
                </tr>
              ))}
              <tr className="grand-total-row">
                <td className="text-start">Grand Total</td>
                <td>{props.dailyInvoiceSales.data.sub_total.sub_total_mrp}</td>
                <td>{props.dailyInvoiceSales.data.sub_total.total_discount}</td>
                <td>{props.dailyInvoiceSales.data.sub_total.total_vat}</td>
                <td>{props.dailyInvoiceSales.data.sub_total.total_exchange}</td>
                <td>{props.dailyInvoiceSales.data.sub_total.total_net_amount}</td>
                <td>{props.dailyInvoiceSales.data.sub_total.total_point}</td>
                <td>{props.dailyInvoiceSales.data.sub_total.sub_total_cost}</td>
                <td>{props.dailyInvoiceSales.data.sub_total.total_exchange_cost}</td>
                <td>{props.dailyInvoiceSales.data.sub_total.total_cash}</td>
                <td>{props.dailyInvoiceSales.data.sub_total.total_online}</td>
                <td>{props.dailyInvoiceSales.data.sub_total.total_card}</td>
                <td>{props.dailyInvoiceSales.data.sub_total.total_due}</td>
                <td>{props.dailyInvoiceSales.data.sub_total.total_profit}</td>
                <td>{props.dailyInvoiceSales.data.sub_total.total_gp}</td>
                <td></td>
              </tr>
            </>
          ) : props.dailyInvoiceLoading ? (
            <tr>
              <td colSpan={16}>
                <Spinner animation="border" variant="warning" />
              </td>
            </tr>
          ) : (
            <tr>
              <td colSpan={16}>No Data Found</td>
            </tr>
          )}
        </tbody>
      </Table>
      {pageCount > 1 && !props.dailyInvoiceLoading ? (
        <ReactPaginate
          breakLabel="..."
          nextLabel=""
          onPageChange={handlePageClick}
          pageRangeDisplayed={3}
          marginPagesDisplayed={1}
          pageCount={pageCount}
          previousLabel=""
          renderOnZeroPageCount={null}
          containerClassName="pagination"
          pageLinkClassName="page-num"
          previousClassName="hide"
          nextClassName="hide"
          activeLinkClassName="active"
        />
      ) : (
        <></>
      )}
    </>
  );
};

export default DailyInvoiceSaleReport;
