import React, { Fragment } from "react";
import { Table, Spinner, Row, Col } from "react-bootstrap";

const SupplierSubCategorySaleReport = (props) => {
  return (
    <>
      {props.supplierSubcategorySale.data &&
      props.supplierSubcategorySale.data.supplier_total &&
      props.supplierSubcategorySale.data.supplier_total.length &&
      !props.supplierSubcategorySaleLoading ? (
        <>
          {props.supplierSubcategorySale.data.supplier_total.map(
            (supplier, index) => (
              <Table key={index} className="mt-3" responsive bordered>
                <thead className="text-center report-table-header">
                  <tr>
                    <th colSpan={4}>
                      Supplier Name:{" "}
                      {supplier.supplier_name ? supplier.supplier_name : "N/A"}
                    </th>
                    <th colSpan={3}>
                      From Date: {props.fromDate ? props.fromDate : "N/A"}{" "}
                    </th>
                    <th colSpan={3}>
                      To Date: {props.toDate ? props.toDate : "N/A"}{" "}
                    </th>
                  </tr>
                  <tr>
                    <th>SubCategory Name</th>
                    <th>Sold(Qty)</th>
                    <th>Total MRP</th>
                    <th>Discount Amount</th>
                    <th>Vat Amount</th>
                    <th>Ex/Ret Amount</th>
                    <th>Total Cost</th>
                    <th>Net Amount</th>
                    <th>Pft Amount</th>
                    <th>GP%</th>
                  </tr>
                </thead>
                <tbody className="text-center report-table-body">
                  <>
                    {props.supplierSubcategorySale.data.sub_category_data.map(
                      (prod, i) =>
                        prod.supplier_name === supplier.supplier_name ? (
                          <tr key={i}>
                            <td>
                              {prod.sub_category_name
                                ? prod.sub_category_name
                                : "N/A"}
                            </td>
                            <td>{prod.sold_qty}</td>
                            <td>{prod.total_mrp}</td>
                            <td>{prod.discount_amount}</td>
                            <td>{prod.vat_amount}</td>
                            <td>{prod.exchange_amount}</td>
                            <td>{prod.total_cost}</td>
                            <td>{prod.net_amount}</td>
                            <td>{prod.pft_amount}</td>
                            <td>{prod.gross_profit}</td>
                          </tr>
                        ) : (
                          <Fragment key={i + 1000}></Fragment>
                        )
                    )}
                    <tr className="grand-total-row">
                      <td className="text-start">Grand Total</td>
                      <td>{supplier.sold_qty}</td>
                      <td>{supplier.total_mrp}</td>
                      <td>{supplier.discount_amount}</td>
                      <td>{supplier.vat_amount}</td>
                      <td>{supplier.exchange_amount}</td>
                      <td>{supplier.total_cost}</td>
                      <td>{supplier.net_amount}</td>
                      <td>{supplier.pft_amount}</td>
                      <td>{supplier.gross_profit}</td>
                    </tr>
                  </>
                </tbody>
              </Table>
            )
          )}
        </>
      ) : props.supplierSubcategorySaleLoading ? (
        <div className="transfer-table" style={{ border: "1px solid #dee2e6" }}>
          <Row className="p-2">
            <Col className="d-flex justify-content-center">
              <Spinner animation="border" variant="warning" />
            </Col>
          </Row>
        </div>
      ) : (
        <div className="transfer-table" style={{ border: "1px solid #dee2e6" }}>
          <Row className="p-2">
            <Col className="d-flex justify-content-center">No Data Found</Col>
          </Row>
        </div>
      )}
    </>
  );
};

export default SupplierSubCategorySaleReport;
