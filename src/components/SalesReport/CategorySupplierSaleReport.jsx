import React, { Fragment } from "react";
import { Table, Spinner,Row,Col } from "react-bootstrap";

const CategorySupplierSaleReport = (props) => {
  return (
    <>
      {props.categorySupplierSale.data && props.categorySupplierSale.data.category_total && props.categorySupplierSale.data.category_total.length && !props.categorySupplierLoading ?
        <>
            {props.categorySupplierSale.data.category_total.map((category,index) => (
                <Table key={index} className="mt-3" responsive bordered>
                    <thead className="text-center report-table-header">
                        <tr>
                            <th colSpan={4}>Category Name: {category.product_category}</th>
                            <th colSpan={3}>From Date: {props.fromDate ? props.fromDate : "N/A"} </th>
                            <th colSpan={3}>To Date: {props.toDate ? props.toDate : "N/A"} </th>
                        </tr>
                        <tr>
                            <th>Supplier Name</th>
                            <th>Sold(Qty)</th>
                            <th>Total MRP</th>
                            <th>Discount Amount</th>
                            <th>Vat Amount</th>
                            <th>Ex/Ret Amount</th>
                            <th>Total Cost</th>
                            <th>Net Amount</th>
                            <th>Pft Amount</th>
                            <th>GP%</th>
                        </tr>
                    </thead>
                    <tbody className="text-center report-table-body">
                        <>
                            {props.categorySupplierSale.data.supplier_data.map((prod,i) => (
                                prod.category_name === category.product_category ?
                                <tr key={i}>
                                    <td>{prod.supplier_name ? prod.supplier_name : "N/A"}</td>
                                    <td>{prod.sold_qty}</td>
                                    <td>{prod.total_mrp}</td>
                                    <td>{prod.discount_amount}</td>
                                    <td>{prod.vat_amount}</td>
                                    <td>{prod.exchange_amount}</td>
                                    <td>{prod.total_cost}</td>
                                    <td>{prod.net_amount}</td>
                                    <td>{prod.pft_amount}</td>
                                    <td>{prod.gross_profit}</td>
                                </tr> :
                                <Fragment key={i+1000}></Fragment>
                            ))}
                            <tr className="grand-total-row">
                                <td className="text-start">Grand Total</td>
                                <td>{category.sold_qty}</td>
                                <td>{category.total_mrp}</td>
                                <td>{category.discount_amount}</td>
                                <td>{category.vat_amount}</td>
                                <td>{category.exchange_amount}</td>
                                <td>{category.total_cost}</td>
                                <td>{category.net_amount}</td>
                                <td>{category.pft_amount}</td>
                                <td>{category.gross_profit}</td>
                            </tr>
                        </>
                    </tbody>
                </Table> 
            ))}
        </> 
        : props.categorySupplierLoading ? (
            <div className="transfer-table" style={{ border: "1px solid #dee2e6" }}>
                <Row className="p-2">
                    <Col className="d-flex justify-content-center">
                        <Spinner animation="border" variant="warning" />
                    </Col>
                </Row>
            </div>
        ) : (
            <div className="transfer-table" style={{ border: "1px solid #dee2e6" }}>
                <Row className="p-2">
                    <Col className="d-flex justify-content-center">No Data Found</Col>
                </Row>
            </div>
        )
      }
    </>
  );
};

export default CategorySupplierSaleReport;
