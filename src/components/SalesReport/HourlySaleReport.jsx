import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { Table, Spinner } from "react-bootstrap";
import ReactPaginate from "react-paginate";
import { getHourlySaleReport } from "../../redux/actions/SalesReportActions";

const HourlySaleReport = (props) => {
  const dispatch = useDispatch();
  const [totalItems, setTotalItems] = useState(0);
  useEffect(() => {
    if (props.hourlySalesList.data) {
      setTotalItems(props.hourlySalesList.data.count);
    }
  }, [props.hourlySalesList]);
  const pageCount = Math.ceil(totalItems / props.limit);

  const handlePageClick = (event) => {
    const newOffset = (event.selected * props.limit) % totalItems;
    if(props.fromDate !== "" && props.toDate !== ""){
        dispatch(getHourlySaleReport(`/?limit=${props.limit}&offset=${newOffset}&date_range_after=${fromDate}&date_range_before=${toDate}`))
      }else{
        alert("Please Select Date Range")
    }
  };
  return (
    <>
      <Table className="mt-3" responsive bordered>
        <thead className="text-center report-table-header">
            <tr>
              <th colSpan={13}>From Date: {props.fromDate ? props.fromDate : "N/A"} </th>
              <th colSpan={12}>To Date: {props.toDate ? props.toDate : "N/A"} </th>
            </tr>
            <tr>
                <th>Date</th>
                <th>00</th>
                <th>01</th>
                <th>02</th>
                <th>03</th>
                <th>04</th>
                <th>05</th>
                <th>06</th>
                <th>07</th>
                <th>08</th>
                <th>09</th>
                <th>10</th>
                <th>11</th>
                <th>12</th>
                <th>13</th>
                <th>14</th>
                <th>15</th>
                <th>16</th>
                <th>17</th>
                <th>18</th>
                <th>19</th>
                <th>20</th>
                <th>21</th>
                <th>22</th>
                <th>23</th>
            </tr>
        </thead>
        <tbody className="text-center report-table-body">
          {props.hourlySalesList && props.hourlySalesList.data && props.hourlySalesList.data.results && props.hourlySalesList.data.results.sales?.length && !props.hourlySalesLoading ? (
            <>
              {props.hourlySalesList.data.results.sales.map((prod, i) => (
                <tr key={i}>
                  <td>{prod.date}</td>
                  <td>{prod.hourly_sales["00"]}</td>
                  <td>{prod.hourly_sales["01"]}</td>
                  <td>{prod.hourly_sales["02"]}</td>
                  <td>{prod.hourly_sales["03"]}</td>
                  <td>{prod.hourly_sales["04"]}</td>
                  <td>{prod.hourly_sales["05"]}</td>
                  <td>{prod.hourly_sales["06"]}</td>
                  <td>{prod.hourly_sales["07"]}</td>
                  <td>{prod.hourly_sales["08"]}</td>
                  <td>{prod.hourly_sales["09"]}</td>
                  <td>{prod.hourly_sales["10"]}</td>
                  <td>{prod.hourly_sales["11"]}</td>
                  <td>{prod.hourly_sales["12"]}</td>
                  <td>{prod.hourly_sales["13"]}</td>
                  <td>{prod.hourly_sales["14"]}</td>
                  <td>{prod.hourly_sales["15"]}</td>
                  <td>{prod.hourly_sales["16"]}</td>
                  <td>{prod.hourly_sales["17"]}</td>
                  <td>{prod.hourly_sales["18"]}</td>
                  <td>{prod.hourly_sales["19"]}</td>
                  <td>{prod.hourly_sales["20"]}</td>
                  <td>{prod.hourly_sales["21"]}</td>
                  <td>{prod.hourly_sales["22"]}</td>
                  <td>{prod.hourly_sales["23"]}</td>
                </tr>
              ))}
              <tr className="grand-total-row">
                <td className="text-start">Grand Total</td>
                <td>{props.hourlySalesList.data.results.grand_total.filter((data) => data.hour === "00")[0].value}</td>
                <td>{props.hourlySalesList.data.results.grand_total.filter((data) => data.hour === "01")[0].value}</td>
                <td>{props.hourlySalesList.data.results.grand_total.filter((data) => data.hour === "02")[0].value}</td>
                <td>{props.hourlySalesList.data.results.grand_total.filter((data) => data.hour === "03")[0].value}</td>
                <td>{props.hourlySalesList.data.results.grand_total.filter((data) => data.hour === "04")[0].value}</td>
                <td>{props.hourlySalesList.data.results.grand_total.filter((data) => data.hour === "05")[0].value}</td>
                <td>{props.hourlySalesList.data.results.grand_total.filter((data) => data.hour === "06")[0].value}</td>
                <td>{props.hourlySalesList.data.results.grand_total.filter((data) => data.hour === "07")[0].value}</td>
                <td>{props.hourlySalesList.data.results.grand_total.filter((data) => data.hour === "08")[0].value}</td>
                <td>{props.hourlySalesList.data.results.grand_total.filter((data) => data.hour === "09")[0].value}</td>
                <td>{props.hourlySalesList.data.results.grand_total.filter((data) => data.hour === "10")[0].value}</td>
                <td>{props.hourlySalesList.data.results.grand_total.filter((data) => data.hour === "11")[0].value}</td>
                <td>{props.hourlySalesList.data.results.grand_total.filter((data) => data.hour === "12")[0].value}</td>
                <td>{props.hourlySalesList.data.results.grand_total.filter((data) => data.hour === "13")[0].value}</td>
                <td>{props.hourlySalesList.data.results.grand_total.filter((data) => data.hour === "14")[0].value}</td>
                <td>{props.hourlySalesList.data.results.grand_total.filter((data) => data.hour === "15")[0].value}</td>
                <td>{props.hourlySalesList.data.results.grand_total.filter((data) => data.hour === "16")[0].value}</td>
                <td>{props.hourlySalesList.data.results.grand_total.filter((data) => data.hour === "17")[0].value}</td>
                <td>{props.hourlySalesList.data.results.grand_total.filter((data) => data.hour === "18")[0].value}</td>
                <td>{props.hourlySalesList.data.results.grand_total.filter((data) => data.hour === "19")[0].value}</td>
                <td>{props.hourlySalesList.data.results.grand_total.filter((data) => data.hour === "20")[0].value}</td>
                <td>{props.hourlySalesList.data.results.grand_total.filter((data) => data.hour === "21")[0].value}</td>
                <td>{props.hourlySalesList.data.results.grand_total.filter((data) => data.hour === "22")[0].value}</td>
                <td>{props.hourlySalesList.data.results.grand_total.filter((data) => data.hour === "23")[0].value}</td>
              </tr>
            </>
          ) : props.hourlySalesLoading ? (
            <tr>
              <td colSpan={25}>
                <Spinner animation="border" variant="warning" />
              </td>
            </tr>
          ) : (
            <tr>
              <td colSpan={25}>No Data Found</td>
            </tr>
          )}
        </tbody>
      </Table>
      {pageCount > 1 && !props.hourlySalesLoading ? (
        <ReactPaginate
          breakLabel="..."
          nextLabel=""
          onPageChange={handlePageClick}
          pageRangeDisplayed={3}
          marginPagesDisplayed={1}
          pageCount={pageCount}
          previousLabel=""
          renderOnZeroPageCount={null}
          containerClassName="pagination"
          pageLinkClassName="page-num"
          previousClassName="hide"
          nextClassName="hide"
          activeLinkClassName="active"
        />
      ) : (
        <></>
      )}
    </>
  );
};

export default HourlySaleReport;
