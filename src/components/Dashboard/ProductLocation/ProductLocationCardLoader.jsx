import React from "react";
import { Col } from "react-bootstrap";
import Skeleton from 'react-loading-skeleton';

const ProductLocationCardLoader = (props) => {
    
  
    return(
        <Col>
            <div className="mb-2 mb-lg-0 location-card" style={{height: "138px"}}>                
                <Skeleton  height={100}/>
            </div>
        </Col>
    )
}

export default ProductLocationCardLoader;