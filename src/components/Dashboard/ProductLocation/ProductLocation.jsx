import React, { useEffect, useState } from "react";
import { Row } from "react-bootstrap";
import Spinner from "react-bootstrap/Spinner";
import { useDispatch, useSelector } from "react-redux";
import reload from "../../../assets/icons/Dashboard/Calender/reload.svg";
import bodycare from "../../../assets/icons/bodycare.png";
import dateSelect from "../../../assets/icons/dash_calender.png";
import food from "../../../assets/icons/food.png";
import medicare from "../../../assets/icons/medicare.png";
import others from "../../../assets/icons/others.png";
import privatebox from "../../../assets/icons/private.png";
import {
  changeDahboardOverlayStatus,
  getProductLocationOverview,
} from "../../../redux/actions/DashboardActions";
import Calender from "../Calendar/Calendar";
import ProductLocationCardComponent from "./ProductLocationCard";

function formatDateToYYYYMMDD(date) {
  const year = date.getFullYear();
  const month = String(date.getMonth() + 1).padStart(2, "0"); // Add 1 to the month because it's 0-based
  const day = String(date.getDate()).padStart(2, "0");

  return `${year}-${month}-${day}`;
}

const ProductLocation = () => {
  const [showCalendar, setCalendar] = useState(false);
  const [fromDate, setFromDate] = useState("");
  const [toDate, setToDate] = useState("");

  const dispatch = useDispatch();

  const calendarHandler = () => {
    // console.log("inside calendarHandler");
    dispatch(changeDahboardOverlayStatus(!showCalendar));
    setCalendar(!showCalendar);
  };

  const filterHandler = () => {
    // console.log("inside calendarHandler");
    if (!fromDate) {
      return alert("You did no select any From Date");
    }

    if (!toDate) {
      return alert("You did no select any To Date");
    }

    // console.log("fromDate = ", fromDate);
    // console.log("toDate = ", toDate);

    dispatch(getProductLocationOverview({ fromDate, toDate }));
    setFromDate("");
    setToDate("");
    setCalendar(false);
    dispatch(changeDahboardOverlayStatus(false));
  };

  const reloadHandler = () => {
    dispatch(getProductLocationOverview(formatDateToYYYYMMDD(new Date())));
    setFromDate("");
    setToDate("");
  };

  useEffect(() => {
    // console.log("inside");
    dispatch(getProductLocationOverview(formatDateToYYYYMMDD(new Date())));
  }, []);

  const { ProductLocationOverviewLoading, ProductLocationOverview } =
    useSelector((state) => state.ProductLocationOverview);

  //  What to Render
  let content;
  if (ProductLocationOverviewLoading) {
    content = (
      <div
        className="d-flex justify-content-center align-items-center"
        style={{ height: "150px" }}
      >
        <Spinner animation="grow" variant="warning" />
      </div>
    );
  }

  // if(ProductLocationOverviewLoading){
  //   content =(<>
  //     <ProductLocationCardLoader/>
  //   <ProductLocationCardLoader/>
  //   <ProductLocationCardLoader/>
  //   <ProductLocationCardLoader/>
  //   <ProductLocationCardLoader/>
  //   </>)
  // }

  if (!ProductLocationOverviewLoading && ProductLocationOverview) {
    content = (
      <>
        <ProductLocationCardComponent
          image={medicare}
          title={`Medicare`}
          query={`medicare`}
          color={`#0CB8DD`}
        />
        <ProductLocationCardComponent
          image={bodycare}
          title={`Bodycare`}
          query={`bodycare`}
          color={`#1B2850`}
        />
        <ProductLocationCardComponent
          image={food}
          title={`Food`}
          query={`food`}
          color={`#FF9900`}
        />
        <ProductLocationCardComponent
          image={privatebox}
          title={`Private`}
          query={`privatebox`}
          color={`#33C481`}
        />
        <ProductLocationCardComponent
          image={others}
          title={`Others`}
          query={null}
          color={`#E4897B`}
        />
      </>
    );
  }

  return (
    <>
      <div className={`container-view position-relative sectionMargin`}>
        <div className="d-flex justify-content-between align-items-center gap-2 mb-2">
          <div className="dashboard-title" style={{ margin: "0px" }}>
            Product Location Overview
            <span
              className="reloadButtonContainer"
              onClick={() => reloadHandler()}
              style={{ marginLeft: "6px" }}
            >
              <img src={reload} alt="" className="reloadButton" />
            </span>
          </div>

          <div
            className="select-date d-flex align-items-center gap-1"
            style={{ cursor: "pointer" }}
            onClick={() => calendarHandler()}
          >
            <img
              className="pe-1 select-date-calendar"
              src={dateSelect}
              alt="dateSelect"
            />
            <nobr>Select Date</nobr>
          </div>
        </div>

        {showCalendar && (
          <div
            className="position-absolute zIndex"
            style={{ top: "15px", right: "0px" }}
          >
            <Calender
              fromDate={fromDate}
              setFromDate={setFromDate}
              toDate={toDate}
              setToDate={setToDate}
              filterHandler={filterHandler}
              calendarHandler={calendarHandler}
            />
          </div>
        )}
      </div>

      <Row className="dashboard-container-view sectionContentMargin">
        {content}
      </Row>
    </>
  );
};

export default ProductLocation;
