import { faArrowUp } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useState } from "react";
import { Col } from "react-bootstrap";
import { useSelector } from "react-redux";
import CountUp from "react-countup";

const ProductLocationCardComponent = (props) => {
  const {
    ProductLocationOverviewLoading,
    ProductLocationOverviewError,
    ProductLocationOverview,
  } = useSelector((state) => state.ProductLocationOverview);
  const [TotalStock, setTotalStock] = useState(0);
  const [ExpiredStock, setExpiredStock] = useState(0);
  const [TotalSale, setTotalSale] = useState(0);
  const [growth, setGrowth] = useState(0);

  useEffect(() => {
    if (ProductLocationOverview.data) {
      const { stock_data, sales_data, expired_stock } =
        ProductLocationOverview.data;
      // console.log("stock_data = ", stock_data);
      // console.log("sales_data = ", sales_data);
      // console.log("expired_stock = ", expired_stock);

      stock_data.find((stockItem) => {
        if (stockItem.location_name === props.query) {
          // console.log(`${stockItem.location_name}  = ${props.query}`);
          // console.log("stockItem.total_stock = ", stockItem.total_stock);
          setTotalStock(stockItem.total_stock);
        }
      });

      expired_stock.find((stockItem) => {
        if (stockItem.location_name === props.query) {
          // console.log(`${stockItem.location_name}  = ${props.query}`);
          // console.log("stockItem.expired_today = ", stockItem.expired_today);
          setExpiredStock(stockItem.expired_today);
        }
      });

      sales_data.find((stockItem) => {
        if (stockItem.location_name === props.query) {
          // console.log(`${stockItem.location_name}  = ${props.query}`);
          // console.log("stockItem.sales_today = ", stockItem.sales_today);
          setTotalSale(stockItem.sales_today);
          if (stockItem.growth) {
            setGrowth(stockItem.growth);
          }
        }
      });
    }
  }, [ProductLocationOverview]);

  return (
    <Col>
      <div className="my-2 mb-lg-0 location-card">
        <div className="d-flex justify-content-between align-items-center">
          <p className="location-card-title">
            <img src={props.image} alt={props.image} />
            {props.title}
          </p>
          {/* <p
            style={{
              color: "white",
              borderRadius: "5px",
              padding: "3px",
              background: `${props.color}`,
              fontSize: "12px",
              fontWeight: "500",
            }}
          >
            <FontAwesomeIcon
              icon={faArrowUp}
              size="sm"
              style={{
                paddingRight: "0.2rem",
              }}
            />
            <CountUp
              start={0}
              end={growth}
              duration={1}
              separator=","
              style={{ fontSize: "12px" }}
            />
            %
          </p> */}
        </div>
        <div
          className="d-flex justify-content-between"
          style={{ fontSize: "12px", color: "#637381", fontWeight: "500" }}
        >
          <p>Total Stock</p>
          <p>Expired Stock</p>
          <p>Total Sale</p>
        </div>
        <div
          className="d-flex justify-content-between"
          style={{
            fontSize: "12px",
            color: "#1B2850",
            fontWeight: "600",
            lineHeight: "0px",
          }}
        >
          <p>
            <CountUp
              start={0}
              end={TotalStock}
              duration={1}
              separator=","
              style={{ fontSize: "12px" }}
            />
          </p>
          <p>
            <CountUp
              start={0}
              end={ExpiredStock}
              duration={1}
              separator=","
              style={{ fontSize: "12px" }}
            />
          </p>
          <p>
            <CountUp
              start={0}
              end={TotalSale}
              duration={1}
              separator=","
              style={{ fontSize: "12px" }}
            />
          </p>
        </div>
      </div>
    </Col>
  );
};

export default ProductLocationCardComponent;
