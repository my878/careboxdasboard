import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import pdfIcon from "../../../assets/icons/pdf.png";
import { getSalesAnalysis } from "../../../redux/actions/DashboardActions";
import "./SalesAnalysis.css";

import Dropdown from "react-bootstrap/Dropdown";
import DropdownButton from "react-bootstrap/DropdownButton";
import ChartLine from "./ChartLine";

function SalesAnalysis() {
  const [salesAnalysisDropdownValue, SalesAnalysisDropdownValue] =
    useState("daily");
  const dispatch = useDispatch();

  // useEffect(()=>{
  //     dispatch(getSalesAnalysis("daily"));

  // }, [])

  useEffect(() => {
    dispatch(getSalesAnalysis(salesAnalysisDropdownValue));
  }, [salesAnalysisDropdownValue]);

  return (
    <div className="salesAnalysisCard">
      <div className="d-flex justify-content-between align-items-center flex-wrap gap-1">
        <div className="dashboard-title">Sales Analysis</div>
        <div className="d-flex justify-content-between align-items-center gap-2">
          <div>
            <DropdownButton
              id="dropdown-basic-button"
              title={
                salesAnalysisDropdownValue === "daily"
                  ? "Daily Analysis"
                  : salesAnalysisDropdownValue === "yearly"
                  ? "Yearly Analysis"
                  : "Monthly Analysis"
              }
              className="salesAnalysisDropdown"
            >
              <Dropdown.Item
                onClick={() => SalesAnalysisDropdownValue("daily")}
                disabled={salesAnalysisDropdownValue === "daily" ? true : false}
              >
                Daily Analysis
              </Dropdown.Item>

              <Dropdown.Item
                onClick={() => SalesAnalysisDropdownValue("monthly")}
                disabled={
                  salesAnalysisDropdownValue === "monthly" ? true : false
                }
              >
                Monthly Analysis
              </Dropdown.Item>
              <Dropdown.Item
                onClick={() => SalesAnalysisDropdownValue("yearly")}
                disabled={
                  salesAnalysisDropdownValue === "yearly" ? true : false
                }
              >
                Yearly Analysis
              </Dropdown.Item>
            </DropdownButton>
          </div>

          <div>
            <img src={pdfIcon} alt="pdf" height={35} />
          </div>
        </div>
      </div>

      <div
        className="d-flex justify-content-center analysis-card"
        style={{ marginTop: "20px" }}
      >
        <ChartLine></ChartLine>
      </div>
    </div>
  );
}

export default SalesAnalysis;
