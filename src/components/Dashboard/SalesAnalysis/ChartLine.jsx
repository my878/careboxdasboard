/* eslint-disable no-unused-vars */
import {
  CategoryScale,
  Chart as ChartJS,
  Legend,
  LineElement,
  LinearScale,
  PointElement,
  Title,
  Tooltip,
} from 'chart.js';
import React from 'react';
import { Line } from 'react-chartjs-2';
import { useSelector } from 'react-redux';

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);


function ChartLine() {
  const {data:responseData}  = useSelector((state)=>state.SalesAnalysis.SalesAnalysis || {});
  // console.log("responseData = ", responseData);
  let content = null;

  if(responseData){
    const keys = Object.keys(responseData);

    let labels = [];
    
    for (const key of keys) {
      labels.push(key);
    }
    
    //   console.log('labels = ', labels);
    
    let datasetsArray = [];
    
    for (const key in responseData) {
      const value = responseData[key];
      datasetsArray.push(value);
    }
  
    const options = {
      responsive: true,
      plugins: {
        legend: {
          position: 'bottom',
        },
        title: {
          display: false,
          text: 'Chart.js Line Chart',
        },
      },
    };
  
  
    const data = {
      labels,
      datasets: [
        {
          label: 'Sales Amount',
          data: datasetsArray,
          borderColor: '#FF9900',
          backgroundColor: '#FF9900',
        },
      ],
    };

    content = <Line options={options} data={data} />
  

  }


  return content;
}

export default ChartLine;
