/* eslint-disable react/prop-types */
import React, { useEffect, useState } from "react";
import { Col } from "react-bootstrap";
import CountUp from "react-countup";

const SalesOverviewCard = ({ image, title, color, data, showDayKeyword }) => {
  const [online, setOnline] = useState(0);
  const [outlet, setOutlet] = useState(0);
  const [perday, setPerday] = useState(0);

  useEffect(() => {
    if (data && data?.online !== null && data?.online !== undefined) {
      if (data?.online !== null) {
        setOnline(data.online);
      }

      if (data?.outlet !== null) {
        setOutlet(data.outlet);
      }

      if (data?.per_day !== null) {
        setPerday(data.per_day);
      }
    }
  }, [data]);

  return (
    <Col sm={6} xl={3}>
      <div
        className="mb-2 mb-xl-0 sales-overview-card"
        style={{ background: `${color}` }}
      >
        <img
          src={image}
          alt="totalSalesIcon"
          className="dashboard-card-image "
        />
        <p className="card-title">{title}</p>
        <p className="card-value">
          {title !== "Total Orders" && "৳ "}
          <CountUp start={0} end={perday} duration={1} separator="," />{" "}
          {showDayKeyword && (
            <span style={{ fontSize: "14px", fontWeight: "400" }}>/Day</span>
          )}
        </p>
        <div className="d-flex justify-content-between gap-2 sales-card-footer">
          <p className="text-white">
            Online: {title !== "Total Orders" && "৳ "}
            <CountUp
              start={0}
              end={online}
              duration={1}
              separator=","
              className="sales-card-footer"
            />
            {/* {online} */}
          </p>
          <p className="text-white">
            Outlet: {title !== "Total Orders" && "৳ "}
            <CountUp
              start={0}
              end={outlet}
              duration={1}
              separator=","
              className="sales-card-footer"
            />
            {/* {outlet} */}
          </p>
        </div>
      </div>
    </Col>
  );
};

export default SalesOverviewCard;
