import React, { useEffect, useState } from "react";
import { Row } from "react-bootstrap";
import Spinner from "react-bootstrap/Spinner";
import { useDispatch, useSelector } from "react-redux";
import reload from "../../../assets/icons/Dashboard/Calender/reload.svg";
import dateSelect from "../../../assets/icons/dash_calender.png";
import totalOrderIcon from "../../../assets/icons/orders.png";
import grossProfitIcon from "../../../assets/icons/profit.png";
import totalSalesIcon from "../../../assets/icons/sales.png";
import totalProductCost from "../../../assets/icons/totalProductCost.png";
import {
  changeDahboardOverlayStatus,
  getSalesOverview,
} from "../../../redux/actions/DashboardActions";
import Calender from "../Calendar/Calendar";
import SalesOverviewCard from "./SalesOverviewCard";

function formatDateToYYYYMMDD(date) {
  const year = date.getFullYear();
  const month = String(date.getMonth() + 1).padStart(2, "0"); // Add 1 to the month because it's 0-based
  const day = String(date.getDate()).padStart(2, "0");

  return `${year}-${month}-${day}`;
}

const SalesOverview = () => {
  const [showCalendar, setCalendar] = useState(false);
  const [fromDate, setFromDate] = useState("");
  const [toDate, setToDate] = useState("");
  const [showDayKeyword, setShowDayKeyword] = useState(true);
  const { salesOverviewLoading, salesOverviewError, salesOverview } =
    useSelector((state) => state.salesOverview);
  const dispatch = useDispatch();

  const calendarHandler = () => {
    // console.log("inside calendarHandler");
    dispatch(changeDahboardOverlayStatus(!showCalendar));
    setCalendar(!showCalendar);
  };

  const filterHandler = () => {
    // console.log("inside calendarHandler");
    if (!fromDate) {
      return alert("You did no select any From Date");
    }

    if (!toDate) {
      return alert("You did no select any To Date");
    }

    // console.log("fromDate = ", fromDate);
    // console.log("toDate = ", toDate);

    dispatch(getSalesOverview({ fromDate, toDate }));
    setFromDate("");
    setToDate("");
    setCalendar(false);
    setShowDayKeyword(false);
    dispatch(changeDahboardOverlayStatus(false));
  };

  const reloadHandler = () => {
    dispatch(getSalesOverview(formatDateToYYYYMMDD(new Date())));
    setFromDate("");
    setToDate("");
    setShowDayKeyword(true);
  };

  useEffect(() => {
    dispatch(getSalesOverview(formatDateToYYYYMMDD(new Date())));
  }, []);

  // What to Render
  let content;

  if (salesOverviewLoading) {
    content = (
      <div
        className="d-flex justify-content-center align-items-center"
        style={{ height: "150px" }}
      >
        <Spinner animation="grow" variant="warning" />
      </div>
    );
  }

  // if(salesOverviewLoading){
  //     content = (<>
  //     <SalesOverviewCardLoader image={totalSalesIcon} color="#1B2850" title ="Total Sales"/>
  //     <SalesOverviewCardLoader image={totalOrderIcon} color="#33C481" title ="Total Orders"/>
  //     <SalesOverviewCardLoader image={totalProductCost} color="#E4897B" title ="Total Product Cost"/>
  //     <SalesOverviewCardLoader image={grossProfitIcon} color="#01C0EA" title ="Gross Profit"/>
  //     </>)
  // }

  if (!salesOverviewLoading && salesOverview) {
    const { total_sales, total_orders, gross_profit, total_cost } =
      salesOverview?.data || {};
    //   console.log("total_sales = ",total_sales);
    //   console.log("total_orders = ",total_orders);
    //   console.log("gross_profit = ",gross_profit);

    content = (
      <>
        <SalesOverviewCard
          image={totalSalesIcon}
          color="#1B2850"
          title="Total Sales"
          data={total_sales}
          showDayKeyword={showDayKeyword}
        />
        <SalesOverviewCard
          image={totalOrderIcon}
          color="#33C481"
          title="Total Orders"
          data={total_orders}
          showDayKeyword={showDayKeyword}
        />
        {/* <SalesOverviewCard image={totalRevenueIcon} color="#E4897B" title ="Total Revenue"/> */}
        <SalesOverviewCard
          image={totalProductCost}
          color="#E4897B"
          title="Total Product Cost"
          data={total_cost}
          showDayKeyword={showDayKeyword}
        />
        <SalesOverviewCard
          image={grossProfitIcon}
          color="#01C0EA"
          title="Gross Profit"
          data={gross_profit}
          showDayKeyword={showDayKeyword}
        />
      </>
    );
  }

  return (
    <>
      <div className={`container-view position-relative sectionMargin`}>
        <div className="d-flex justify-content-between align-items-center gap-2 ">
          {/* <div className="d-flex align-items-center gap-2 flex-wrap">
            <p
              className="dashboard-title d-flex flex-col align-items-center"
              style={{ margin: "0px" }}
            >
              Sales Overview
            </p>
            <div
              className="reloadButtonContainer"
              onClick={() => reloadHandler()}
            >
              <img src={reload} alt="" className="reloadButton" />
            </div>
          </div> */}

          <div className="dashboard-title">
            Sales Overview
            <span
              className="reloadButtonContainer"
              onClick={() => reloadHandler()}
              style={{ marginLeft: "6px" }}
            >
              <img src={reload} alt="" className="reloadButton" />
            </span>
          </div>

          <div
            className="select-date d-flex align-items-center gap-1"
            style={{ cursor: "pointer" }}
            onClick={() => calendarHandler()}
          >
            <img
              className="pe-1 select-date-calendar"
              src={dateSelect}
              alt="dateSelect"
            />
            <nobr>Select Date</nobr>
          </div>
        </div>

        {showCalendar && (
          <div
            className="position-absolute zIndex"
            style={{ top: "15px", right: "0px" }}
          >
            <Calender
              fromDate={fromDate}
              setFromDate={setFromDate}
              toDate={toDate}
              setToDate={setToDate}
              filterHandler={filterHandler}
              calendarHandler={calendarHandler}
            />
          </div>
        )}
      </div>

      <Row className="dashboard-container-view sectionContentMargin">
        {content}

        {/* <SalesOverviewCard image={totalSalesIcon} color="#1B2850" title ="Total Sales"/>
            <SalesOverviewCard image={totalOrderIcon} color="#33C481" title ="Total Orders"/>
            <SalesOverviewCard image={totalRevenueIcon} color="#E4897B" title ="Total Revenue"/>
            <SalesOverviewCard image={grossProfitIcon} color="#01C0EA" title ="Gross Profit"/> */}
      </Row>
    </>
  );
};

export default SalesOverview;
