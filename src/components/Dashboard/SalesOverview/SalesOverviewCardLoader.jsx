import React from 'react';
import { Col } from "react-bootstrap";
import totalSalesIcon from "../../assets/icons/sales.png";
import Skeleton from 'react-loading-skeleton'

const SalesOverviewCardLoader = ({image, title, color}) =>{
  console.log("SalesOverviewCardLoader ");
    return (
        <Col xl={3}>
        <div
          className="mb-2 mb-xl-0 sales-overview-card"
          style={{ background: `${color}` }}>
            <img  src={image}  alt="totalSalesIcon" className="dashboard-card-image "/>
            <p className="card-title">{title}</p>
            <p className="card-value d-flex">
              ৳{<Skeleton width={30} style={{margin: "0 3px"}}/>}
              <span style={{fontSize:"14px",fontWeight:"400"}}>/Day</span>
            </p>
            <div className="d-flex justify-content-between gap-2">
              <p className="text-white d-flex">Online: ৳{<Skeleton  width={30}  style={{margin: "0 3px"}} />}</p>
              <p className="text-white d-flex">Outlet: ৳{<Skeleton  width={30}  style={{margin: "0 3px"}} />}</p>
            </div>
        </div>
      </Col>
    );
}

export default SalesOverviewCardLoader;
