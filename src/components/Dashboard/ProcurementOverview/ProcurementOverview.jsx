import React, { useEffect, useState } from "react";
import { Col, Row } from "react-bootstrap";
import Spinner from "react-bootstrap/Spinner";
import { useDispatch, useSelector } from "react-redux";
import reload from "../../../assets/icons/Dashboard/Calender/reload.svg";
import poIcon from "../../../assets/icons/PO.png";
import dateSelect from "../../../assets/icons/dash_calender.png";
import pendingReqIcon from "../../../assets/icons/pendingReq.png";
import purchaseIcon from "../../../assets/icons/purchase.png";
import ProcurementCard from "./ProcurementCard";
import {
  changeDahboardOverlayStatus,
  getProcurementOverview,
} from "../../..//redux/actions/DashboardActions";
import SingleCalender from "../Calendar/SingleCalendar";
import Calender from "../Calendar/Calendar";

function formatDateToYYYYMMDD(date) {
  const year = date.getFullYear();
  const month = String(date.getMonth() + 1).padStart(2, "0"); // Add 1 to the month because it's 0-based
  const day = String(date.getDate()).padStart(2, "0");

  return `${year}-${month}-${day}`;
}

const ProcurementOverview = () => {
  const [showCalendar, setCalendar] = useState(false);
  const [fromDate, setFromDate] = useState("");
  const [PoCount, setPoCount] = useState(0);
  const [pendingRequest, setPendingRequest] = useState(0);
  const [purchase, setPurchase] = useState(0);
  const [toDate, setToDate] = useState("");

  const dispatch = useDispatch();

  const calendarHandler = () => {
    // console.log("inside calendarHandler");
    setCalendar(!showCalendar);
    dispatch(changeDahboardOverlayStatus(!showCalendar));
    setFromDate("");
    setToDate("");
  };

  const filterHandler = () => {
    // console.log("inside calendarHandler");

    if (fromDate && toDate) {
      dispatch(
        getProcurementOverview(`/?from_date=${fromDate}&to_date=${toDate}`)
      );
      setCalendar(false);
      dispatch(changeDahboardOverlayStatus(false));
      //setFromDate("");
      //setToDate("");
    } else {
      return alert("You need to select Date Range");
    }
    // console.log("fromDate = ", fromDate);
    // console.log("toDate = ", toDate);
  };

  const reloadHandler = () => {
    console.log(formatDateToYYYYMMDD(new Date()));
    dispatch(getProcurementOverview("/"));
    setFromDate("");
    setToDate("");
  };

  useEffect(() => {
    // console.log("inside");
    dispatch(getProcurementOverview("/"));
  }, []);

  const {
    ProcurementOverviewLoading,
    ProcurementOverviewError,
    ProcurementOverview,
  } = useSelector((state) => state.ProcurementOverview);

  useEffect(() => {
    if (!ProcurementOverviewLoading && ProcurementOverview.data) {
      // console.log("useEffect, ProcurementOverview.data = ", ProcurementOverview.data);
      if (ProcurementOverview.data.procurement_overview) {
        // console.log("useEffect, ProcurementOverview.data.procurement_overview = ", ProcurementOverview.data.procurement_overview);

        if (ProcurementOverview.data.procurement_overview.po_count !== null) {
          // console.log("ProcurementOverview.data.procurement_overview.po_count = ", ProcurementOverview.data.procurement_overview.po_count);
          setPoCount(ProcurementOverview.data.procurement_overview.po_count);
        }
        if (
          ProcurementOverview.data.procurement_overview.purchase_request !==
          null
        ) {
          setPendingRequest(
            ProcurementOverview.data.procurement_overview.purchase_request
          );
        }
        if (ProcurementOverview.data.procurement_overview.purchase !== null) {
          setPurchase(ProcurementOverview.data.procurement_overview.purchase);
        }
      }
    }
  }, [ProcurementOverview, ProcurementOverviewLoading]);

  //  What to Render
  let content;
  if (ProcurementOverviewLoading) {
    content = (
      <div
        className="d-flex justify-content-center align-items-center"
        style={{ height: "150px" }}
      >
        <Spinner animation="grow" variant="warning" />
      </div>
    );
  }

  if (!ProcurementOverviewLoading && ProcurementOverview.data) {
    // console.log("content = !ProcurementOverviewLoading && ProcurementOverview.data");
    // console.log("PoCount" , PoCount);
    // console.log("pendingRequest = ", pendingRequest);
    // console.log("purchase = ", purchase);

    content = (
      <Col xl={12}>
        <div className="online-service-card">
          <div className="p-4">
            <Row>
              <ProcurementCard
                image={poIcon}
                title={PoCount}
                text={`Number Of PO's`}
              />
              <ProcurementCard
                image={pendingReqIcon}
                title={pendingRequest}
                text={`Number of Pending Request`}
              />
              <ProcurementCard
                image={purchaseIcon}
                title={purchase}
                text={`Number of Purchase`}
              />
            </Row>
          </div>
        </div>
      </Col>
    );
  }
  // console.log(fromDate);
  return (
    <>
      <div className={`container-view position-relative sectionMargin`}>
        <div className="d-flex justify-content-between align-items-center gap-2 mb-2">
          {/* <div className="d-flex align-items-center gap-2 flex-wrap">
            <p
              className="dashboard-title d-flex flex-col align-items-center"
              style={{ margin: "0px" }}
            >
              Procurement Overview
            </p>
            <div
              className="reloadButtonContainer"
              onClick={() => reloadHandler()}
            >
              <img src={reload} alt="" className="reloadButton" />
            </div>
          </div> */}

          <div className="dashboard-title" style={{ margin: "0px" }}>
            Procurement Overview
            <span
              className="reloadButtonContainer"
              onClick={() => reloadHandler()}
              style={{ marginLeft: "6px" }}
            >
              <img src={reload} alt="" className="reloadButton" />
            </span>
          </div>

          <div
            className="select-date d-flex align-items-center gap-1"
            style={{ cursor: "pointer" }}
            onClick={() => calendarHandler()}
          >
            <img
              className="pe-1 select-date-calendar"
              src={dateSelect}
              alt="dateSelect"
            />
            {/* <nobr>Select Date</nobr> */}
            {fromDate && toDate ? (
              <span style={{ fontSize: "14px" }}>
                Selected Date: {fromDate} to {toDate}
              </span>
            ) : (
              <nobr>Select Date</nobr>
            )}
          </div>
        </div>
        {showCalendar && (
          <div
            className="position-absolute zIndex"
            style={{ right: "0px", bottom: "0px" }}
          >
            <Calender
              fromDate={fromDate}
              setFromDate={setFromDate}
              toDate={toDate}
              setToDate={setToDate}
              filterHandler={filterHandler}
              calendarHandler={calendarHandler}
            />
          </div>
        )}
      </div>

      <Row className="dashboard-container-view sectionContentMargin">
        {content}
      </Row>
    </>
  );
};

export default ProcurementOverview;
