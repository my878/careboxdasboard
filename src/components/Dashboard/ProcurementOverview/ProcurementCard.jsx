import React from "react";
import { Col, Row } from "react-bootstrap";
import CountUp from "react-countup";
import "./ProcurementCard.css";

const ProcurementCard = (props) => {
  return (
    <Col
      xl={4}
      className={`px-0  px-sm-5 py-2 py-xl-0 d-flex justify-content-start align-items-center ${
        props.text !== "Number of Purchase" ? "procurementCardBorderRight" : ""
      }`}

      // style={{
      //   borderRight: `${
      //     props.text !== "Number of Purchase" ? "1px solid #DCE0E4" : ""
      //   }`,
      // }}
    >
      <img
        //className="dashboard-card-image"
        src={props.image}
        alt={props.image}
        height={110}
        width={110}
      />
      <div className="ms-3">
        <span
          className=""
          style={{ fontWeight: 600, fontSize: "18px", color: "#1B2850" }}
        >
          <CountUp
            start={0}
            end={parseInt(props.title)}
            duration={1}
            separator=","
            style={{ fontSize: "18px" }}
          />
        </span>
        <p
          className="card-text"
          style={{ fontWeight: 400, fontSize: "14px", color: "#637381" }}
        >
          {" "}
          {props.text}
        </p>
      </div>
    </Col>
  );
};

export default ProcurementCard;
