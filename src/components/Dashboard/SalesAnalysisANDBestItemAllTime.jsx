import React, { useEffect, useState } from "react";
import SalesAnalysis from "./SalesAnalysis/SalesAnalysis";
import BestItemAllTime from "./BestItemAllTime/BestItemAllTime";
import { Col, Container, Row } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import Spinner from "react-bootstrap/Spinner";
import {
  getBestItemAllTime,
  getSalesAnalysis,
} from "../../redux/actions/DashboardActions";
import pdfIcon from "../../assets/icons/pdf.png";
import ChartLine from "./SalesAnalysis/ChartLine";
import Dropdown from "react-bootstrap/Dropdown";
import DropdownButton from "react-bootstrap/DropdownButton";
import "../Dashboard/SalesAnalysis/SalesAnalysis.css";
import Slider from "react-slick";
import BestItemAllTimeCard from "./BestItemAllTime/BestItemAllTimeCard";
import NoDataFound from "../../assets/icons/Dashboard/SalesAnalysis/No data-rafiki.svg";

function SalesAnalysisANDBestItemAllTime() {
  const [salesAnalysisDropdownValue, setSalesAnalysisDropdownValue] =
    useState("daily");
  const [bestItemAllTimeDropdownValue, setBestItemAllTimeDropdownValue] =
    useState("daily");

  const dispatch = useDispatch();

  const { BestItemAllTimeLoading, BestItemAllTime } = useSelector(
    (state) => state.BestItemAllTime
  );

  const { SalesAnalysisLoading } = useSelector((state) => state.SalesAnalysis);

  useEffect(() => {
    dispatch(getSalesAnalysis(salesAnalysisDropdownValue));
  }, [salesAnalysisDropdownValue]);

  useEffect(() => {
    dispatch(getBestItemAllTime(bestItemAllTimeDropdownValue));
  }, [bestItemAllTimeDropdownValue]);
  const slidesToShowCheck = (n, totalElementent) => {
    // console.log(totalElementent);
    if (totalElementent) {
      // return n;
      if (totalElementent > n) {
        // console.log(n);
        return n;
      }
      return totalElementent;
    }
  };

  const settings = {
    dots: false,
    infinite: true,
    slidesToShow: slidesToShowCheck(
      3,
      BestItemAllTime?.data?.best_item.results.length
    ),
    slidesToScroll: 3,
    autoplay: true,
    speed: 10000,
    autoplaySpeed: 0,
    cssEase: "linear",
    responsive: [
      {
        breakpoint: 1600, // 600px আগ পর্যন্ত 2টা দেখাবে
        settings: {
          slidesToShow: slidesToShowCheck(
            2,
            BestItemAllTime?.data?.best_item.results.length
          ),
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 1200, // 600px আগ পর্যন্ত 2টা দেখাবে
        settings: {
          slidesToShow: slidesToShowCheck(
            4,
            BestItemAllTime?.data?.best_item.results.length
          ),
          slidesToScroll: 4,
        },
      },

      {
        breakpoint: 1000, // 600px আগ পর্যন্ত 2টা দেখাবে
        settings: {
          slidesToShow: slidesToShowCheck(
            3,
            BestItemAllTime?.data?.best_item.results.length
          ),
          slidesToScroll: 3,
        },
      },

      {
        breakpoint: 800, // 600px আগ পর্যন্ত 2টা দেখাবে
        settings: {
          slidesToShow: slidesToShowCheck(
            2,
            BestItemAllTime?.data?.best_item.results.length
          ),
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 600, // 600px আগ পর্যন্ত 2টা দেখাবে
        settings: {
          slidesToShow: slidesToShowCheck(
            2,
            BestItemAllTime?.data?.best_item.results.length
          ),
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 450, // 299px আগ পর্যন্ত একটা দেখাবে
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  //   What to Render
  let BestItemAllTimeContent;

  if (BestItemAllTimeLoading) {
    BestItemAllTimeContent = <div>Loading</div>;
  }

  if (
    !BestItemAllTimeLoading &&
    BestItemAllTime.data &&
    BestItemAllTime.data.best_item.results.length > 0
  ) {
    BestItemAllTimeContent = (
      <div className="sectionContentMargin best-items-card">
        {BestItemAllTime.data.best_item.results.length === 1 ? (
          BestItemAllTime.data.best_item.results.map((CardDetails, index) => (
            <div key={index} className="py-xxl-4 px-2 h-100">
              <BestItemAllTimeCard
                CardDetails={CardDetails}
                cardWidth={`item-card-width`}
              />
            </div>
          ))
        ) : (
          <Slider {...settings}>
            {BestItemAllTime.data.best_item.results.map(
              (CardDetails, index) => (
                <div key={index} className="py-xxl-4 px-2 h-100">
                  <BestItemAllTimeCard
                    CardDetails={CardDetails}
                    cardWidth={`w-auto`}
                  />
                </div>
              )
            )}
          </Slider>
        )}
      </div>
    );
  }
  //console.log(BestItemAllTime?.data?.best_item.results.length);
  if (
    !BestItemAllTimeLoading &&
    BestItemAllTime.data &&
    BestItemAllTime.data.best_item.results.length === 0
  ) {
    BestItemAllTimeContent = (
      <div className="sectionContentMargin analysis-card d-flex justify-content-center align-items-center">
        <img src={NoDataFound} className="img-fluid w-50" />
      </div>
    );
  }

  let content;

  if (SalesAnalysisLoading && BestItemAllTimeLoading) {
    content = (
      <div
        className="d-flex justify-content-center align-items-center"
        style={{ height: "150px" }}
      >
        <Spinner animation="grow" variant="warning" />
      </div>
    );
  }

  if (!SalesAnalysisLoading || !BestItemAllTimeLoading) {
    content = (
      <Row>
        <Col xl={6} className="sectionMargin">
          {!SalesAnalysisLoading ? (
            <div className="salesAnalysisCard">
              <div className="d-flex justify-content-between align-items-center flex-wrap gap-1">
                <div className="dashboard-title">Sales Analysis</div>
                <div className="d-flex justify-content-between align-items-center gap-2">
                  <div>
                    <DropdownButton
                      id="dropdown-basic-button"
                      title={
                        salesAnalysisDropdownValue === "daily"
                          ? "Daily Analysis"
                          : salesAnalysisDropdownValue === "yearly"
                          ? "Yearly Analysis"
                          : "Monthly Analysis"
                      }
                      className="salesAnalysisDropdown"
                    >
                      <Dropdown.Item
                        onClick={() => setSalesAnalysisDropdownValue("daily")}
                        disabled={
                          salesAnalysisDropdownValue === "daily" ? true : false
                        }
                      >
                        Daily Analysis
                      </Dropdown.Item>

                      <Dropdown.Item
                        onClick={() => setSalesAnalysisDropdownValue("monthly")}
                        disabled={
                          salesAnalysisDropdownValue === "monthly"
                            ? true
                            : false
                        }
                      >
                        Monthly Analysis
                      </Dropdown.Item>
                      <Dropdown.Item
                        onClick={() => setSalesAnalysisDropdownValue("yearly")}
                        disabled={
                          salesAnalysisDropdownValue === "yearly" ? true : false
                        }
                      >
                        Yearly Analysis
                      </Dropdown.Item>
                    </DropdownButton>
                  </div>

                  <div className="d-none">
                    <img src={pdfIcon} alt="pdf" height={35} />
                  </div>
                </div>
              </div>

              <div
                className="d-flex justify-content-center analysis-card"
                style={{ marginTop: "20px" }}
              >
                <ChartLine></ChartLine>
              </div>
            </div>
          ) : (
            <div
              className="d-flex justify-content-center align-items-center"
              style={{ height: "150px" }}
            >
              <Spinner animation="grow" variant="warning" />
            </div>
          )}
        </Col>
        <Col xl={6} className="sectionMargin">
          {!BestItemAllTimeLoading ? (
            <div className="salesAnalysisCard">
              <div className="d-flex justify-content-between align-items-center flex-wrap gap-1">
                <div className="dashboard-title">Best Items All Time</div>
                <div className="d-flex justify-content-between align-items-center gap-2">
                  <div>
                    <DropdownButton
                      id="dropdown-basic-button"
                      title={
                        bestItemAllTimeDropdownValue === "daily"
                          ? "Daily"
                          : bestItemAllTimeDropdownValue === "yearly"
                          ? "Yearly"
                          : "Monthly"
                      }
                      className="salesAnalysisDropdown"
                    >
                      <Dropdown.Item
                        onClick={() => setBestItemAllTimeDropdownValue("daily")}
                        disabled={
                          bestItemAllTimeDropdownValue === "daily"
                            ? true
                            : false
                        }
                      >
                        Daily
                      </Dropdown.Item>

                      <Dropdown.Item
                        onClick={() =>
                          setBestItemAllTimeDropdownValue("monthly")
                        }
                        disabled={
                          bestItemAllTimeDropdownValue === "monthly"
                            ? true
                            : false
                        }
                      >
                        Monthly
                      </Dropdown.Item>
                      <Dropdown.Item
                        onClick={() =>
                          setBestItemAllTimeDropdownValue("yearly")
                        }
                        disabled={
                          bestItemAllTimeDropdownValue === "yearly"
                            ? true
                            : false
                        }
                      >
                        Yearly
                      </Dropdown.Item>
                    </DropdownButton>
                  </div>
                </div>
              </div>

              {BestItemAllTimeContent}
            </div>
          ) : (
            <div
              className="d-flex justify-content-center align-items-center"
              style={{ height: "150px" }}
            >
              <Spinner animation="grow" variant="warning" />
            </div>
          )}
        </Col>
      </Row>
    );
  }

  return <div className="container-view">{content}</div>;
}

export default SalesAnalysisANDBestItemAllTime;
