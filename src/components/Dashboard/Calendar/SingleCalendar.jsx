/* eslint-disable react/prop-types */
import calendar from '../../../assets/icons/Dashboard/Calender/calender.svg';
// import calenderInput from '../../../assets/icons/Dashboard/Calender/calenderInput.svg';
import './Calendar.css';

function SingleCalender({fromDate, setFromDate, filterHandler, calendarHandler }) {

  return (
    <div className="calendarContainer bg-white">
      <div className="d-flex flex-column gap-4 align-items-center">
        <div className="d-flex align-items-center flex-wrap gap-2">
          <div className="calendarLogoContainer">
            <img src={calendar} alt="" className="calendarLogo" />
          </div>
          <div className="calendarText">Date Filter</div>
        </div>

        <div className="d-flex gap-3 flex-wrap justify-content-center">
          <div className="position-relative">
          Date: {''}
            <input
              type="date"
              placeholder="From Date"
              className="calendarInput"
              value={fromDate}
              onChange={(e) => setFromDate(e.target.value)}
            />

            {/* <div className="position-absolute d-flex align-items-center h-100 calendarInputLogo">
              <img src={calenderInput} alt="" />
            </div> */}
          </div>


        </div>

        <div className="d-flex gap-3 flex-wrap">
          <button type="button" className="applyButton" onClick={()=>{filterHandler()}}>
            Apply
          </button>
          <button type="button" className="cancelButton" onClick={()=>{calendarHandler()}}>
            Cancel
          </button>
        </div>
      </div>
    </div>
  );
}

export default SingleCalender;
