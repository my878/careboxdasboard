/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import React, { useEffect } from "react";
import { useState } from "react";
import { Col } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChartLine } from "@fortawesome/free-solid-svg-icons";

const OnlineServiceCard = (props) => {
  const [TotalBooked, setTotalBooked] = useState(0);
  const [TotalAmount, setTotalAmount] = useState(0);

  useEffect(() => {
    //
    if (props.data) {
      // console.log("props.data = ", props.data);
      if (props.data.total_booked) {
        setTotalBooked(props.data.total_booked);
      } else {
        setTotalBooked(0);
      }

      if (props.data.total_amount) {
        setTotalAmount(props.data.total_amount);
      } else {
        setTotalAmount(0);
      }
    }
  }, [props]);

  return (
    <Col xl={4}>
      <div className="mb-2 mb-xl-0 online-service-card">
        <div className="d-flex ">
          <div
            className="px-4 d-flex align-items-center"
            style={{ background: `${props.color}` }}
          >
            <img
              className="img-fluid online-service-card-img"
              src={props.image}
              alt={props.image}
            />
          </div>
          <div className="ms-3 my-3 w-100">
            <div>
              <p
                className="online-service-card-text"
                style={{ color: `${props.color}` }}
              >
                {props.title}
              </p>
            </div>

            <div className="row online-service-card-count">
              <Col>
                <span>Total Booked</span>
                <p>{TotalBooked}</p>
              </Col>
              <Col>
                <span>Total Amount</span>
                <p>{TotalAmount}</p>
              </Col>
            </div>
          </div>
        </div>
      </div>
    </Col>
  );
};

export default OnlineServiceCard;
