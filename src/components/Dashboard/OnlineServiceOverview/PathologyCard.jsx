/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import React, { useEffect } from "react";
import { useState } from "react";
import { Col } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChartLine } from "@fortawesome/free-solid-svg-icons";
import PathologyGrothUp from "../../../assets/icons/Dashboard/Pathology/PathologyGrothUp.svg";
import "./OnlineService.css";
import CountUp from "react-countup";

const PathologyCard = (props) => {
  const [TotalBooked, setTotalBooked] = useState(0);
  const [TotalAmount, setTotalAmount] = useState(0);
  const [groth, setGroth] = useState(0);

  const { showDayKeyword } = props;

  useEffect(() => {
    //
    if (props.data) {
      // console.log("props.data = ", props.data);
      if (props.data.total_booked) {
        setTotalBooked(props.data.total_booked);
      } else {
        setTotalBooked(0);
      }

      if (props.data.total_amount) {
        setTotalAmount(props.data.total_amount);
      } else {
        setTotalAmount(0);
      }

      if (props.data.growth) {
        setGroth(props.data.growth);
      } else {
        setGroth(0);
      }
    }
  }, [props]);

  return (
    <Col xl={4}>
      <div className="mb-2 mb-xl-0 online-service-card">
        <div className="d-flex ">
          <div
            className="px-4 d-flex align-items-center"
            style={{ background: `${props.color}` }}
          >
            <img
              className="img-fluid online-service-card-img"
              src={props.image}
              alt={props.image}
            />
          </div>

          <div className="pathologyCardContentContainer w-100">
            <div className="d-flex justify-content-between align-items-center w-100">
              <div
                className="pathologyCardTitle"
                style={{ color: `${props.color}` }}
              >
                {props.title}
              </div>

              {/* <div className="d-flex align-items-center">
                <div>
                  <img
                    src={PathologyGrothUp}
                    alt=""
                    className="pathologyCardPathologyGroth"
                  />
                </div>

                <div
                  style={{ color: `${props.color}` }}
                  className="pathologyCardPathologyGrothText"
                >
                  (
                  <CountUp
                    start={0}
                    end={groth}
                    duration={1}
                    separator=","
                    className="pathologyCardPathologyGrothText"
                  />
                  )
                </div>
              </div> */}
            </div>

            <div className="pathologyCardBottomContentMargin d-flex justify-content-between">
              <div>
                <div className="totalBooked">Total Booked</div>
                <div className="totalBookedValue">
                  <CountUp
                    start={0}
                    end={TotalBooked}
                    duration={1}
                    separator=","
                    className="totalBookedValue"
                  />
                </div>
              </div>

              <div>
                <div className="totalBooked">Total Amount</div>
                <div className="totalBookedValue">
                  ৳
                  <CountUp
                    start={0}
                    end={TotalAmount}
                    duration={1}
                    separator=","
                    className="totalBookedValue"
                  />
                  {showDayKeyword && <span className="perDay">/Day</span>}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Col>
  );
};

export default PathologyCard;
