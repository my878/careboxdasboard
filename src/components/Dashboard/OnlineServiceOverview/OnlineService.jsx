import React, { useEffect, useState } from "react";
import { Row } from "react-bootstrap";
import Spinner from "react-bootstrap/Spinner";
import { useDispatch, useSelector } from "react-redux";
import reload from "../../../assets/icons/Dashboard/Calender/reload.svg";
import dateSelect from "../../../assets/icons/dash_calender.png";
import onlineDoctor from "../../../assets/icons/online-doctor.png";
import pathologyImg from "../../../assets/icons/pathology.png";
import physicalDoctor from "../../../assets/icons/physical-doctor.png";
import OnlineServiceCard from "./OnlineServiceCard";
import {
  changeDahboardOverlayStatus,
  getOnlineServiceOverview,
} from "../../../redux/actions/DashboardActions";
import Calender from "../Calendar/Calendar";
import PathologyCard from "./PathologyCard";
import PhysicalDoctorCard from "./PhysicalDoctorCard";
import OnlineDoctorCard from "./OnlineDoctorCard";

function formatDateToYYYYMMDD(date) {
  const year = date.getFullYear();
  const month = String(date.getMonth() + 1).padStart(2, "0"); // Add 1 to the month because it's 0-based
  const day = String(date.getDate()).padStart(2, "0");

  return `${year}-${month}-${day}`;
}

const OnlineService = (props) => {
  const [showCalendar, setCalendar] = useState(false);
  const [fromDate, setFromDate] = useState("");
  const [toDate, setToDate] = useState("");
  const [showDayKeyword, setShowDayKeyword] = useState(true);

  const {
    OnlineServiceOverviewLoading,
    OnlineServiceOverviewError,
    OnlineServiceOverview,
  } = useSelector((state) => state.OnlineServiceOverview);

  // What to Render
  let content;

  if (OnlineServiceOverviewLoading) {
    content = (
      <div
        className="d-flex justify-content-center align-items-center"
        style={{ height: "150px" }}
      >
        <Spinner animation="grow" variant="warning" />
      </div>
    );
  }

  if (!OnlineServiceOverviewLoading && OnlineServiceOverview.data) {
    const { pathology, physical_doctor, online_doctor } =
      OnlineServiceOverview.data || {};
    // console.log("pathology = ", pathology);
    // console.log("physical_doctor = ",physical_doctor);
    // console.log("online_doctor = ",online_doctor);

    content = (
      <>
        <PathologyCard
          image={pathologyImg}
          title={`Pathology`}
          color={`#0CB8DD`}
          data={pathology}
          showDayKeyword={showDayKeyword}
        />
        <PhysicalDoctorCard
          image={physicalDoctor}
          title={`Physical Doctor`}
          color={`#33C481`}
          data={physical_doctor}
          showDayKeyword={showDayKeyword}
        />
        <OnlineDoctorCard
          image={onlineDoctor}
          title={`Online Doctor`}
          color={`#00C2AD`}
          data={online_doctor}
          showDayKeyword={showDayKeyword}
        />

        {/* <OnlineServiceCard
          image={onlineDoctor}
          title={`Online Doctor`}
          color={`#00C2AD`}
          data={online_doctor}
        /> */}
      </>
    );
  }

  const dispatch = useDispatch();

  const calendarHandler = () => {
    // console.log("inside calendarHandler");
    dispatch(changeDahboardOverlayStatus(!showCalendar));
    setCalendar(!showCalendar);
  };

  const filterHandler = () => {
    // console.log("inside calendarHandler");
    if (!fromDate) {
      return alert("You did no select any From Date");
    }

    if (!toDate) {
      return alert("You did no select any To Date");
    }

    // console.log("fromDate = ", fromDate);
    // console.log("toDate = ", toDate);

    dispatch(getOnlineServiceOverview({ fromDate, toDate }));
    setCalendar(false);
    setShowDayKeyword(false);
    dispatch(changeDahboardOverlayStatus(false));
    setFromDate("");
    setToDate("");
  };

  const reloadHandler = () => {
    dispatch(getOnlineServiceOverview(formatDateToYYYYMMDD(new Date())));
    setFromDate("");
    setToDate("");
    setShowDayKeyword(true);
  };

  useEffect(() => {
    // console.log("inside");
    dispatch(getOnlineServiceOverview(formatDateToYYYYMMDD(new Date())));
  }, []);

  return (
    <>
      <div className={`container-view position-relative sectionMargin`}>
        <div className="d-flex justify-content-between align-items-center gap-2 mb-2">
          {/* <div className="d-flex align-items-center gap-2 flex-wrap">
            <p
              className="dashboard-title d-flex flex-col align-items-center"
              style={{ margin: "0px" }}
            >
              Online Service Overview
            </p>
            <div
              className="reloadButtonContainer"
              onClick={() => reloadHandler()}
            >
              <img src={reload} alt="" className="reloadButton" />
            </div>
          </div> */}

          <div className="dashboard-title" style={{ margin: "0px" }}>
            Online Service Overview
            <span
              className="reloadButtonContainer"
              onClick={() => reloadHandler()}
              style={{ marginLeft: "6px" }}
            >
              <img src={reload} alt="" className="reloadButton" />
            </span>
          </div>

          <div
            className="select-date d-flex align-items-center gap-1"
            style={{ cursor: "pointer" }}
            onClick={() => calendarHandler()}
          >
            <img
              className="pe-1 select-date-calendar"
              src={dateSelect}
              alt="dateSelect"
            />
            <nobr>Select Date</nobr>
          </div>
        </div>

        {showCalendar && (
          <div
            className="position-absolute zIndex"
            style={{ top: "15px", right: "0px" }}
          >
            <Calender
              fromDate={fromDate}
              setFromDate={setFromDate}
              toDate={toDate}
              setToDate={setToDate}
              filterHandler={filterHandler}
              calendarHandler={calendarHandler}
            />
          </div>
        )}
      </div>

      <Row className="dashboard-container-view sectionContentMargin">
        {content}
      </Row>
    </>
  );
};

export default OnlineService;
