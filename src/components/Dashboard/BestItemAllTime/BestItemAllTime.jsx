import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getBestItemAllTime } from "../../../redux/actions/DashboardActions";
import "./BestItemAllTime.css";
import BestItemAllTimeCard from "./BestItemAllTimeCard";
import "../SalesAnalysis/SalesAnalysis.css";
import Dropdown from "react-bootstrap/Dropdown";
import DropdownButton from "react-bootstrap/DropdownButton";
import Slider from "react-slick";

function BestItemAllTime(props) {
  const [bestItemAllTimeDropdownValue, setBestItemAllTimeDropdownValue] =
    useState("daily");
  const dispatch = useDispatch();

  const { BestItemAllTimeLoading, BestItemAllTimeError, BestItemAllTime } =
    useSelector((state) => state.BestItemAllTime);

  useEffect(() => {
    dispatch(getBestItemAllTime(bestItemAllTimeDropdownValue));
  }, [bestItemAllTimeDropdownValue]);

  const settings = {
    dots: false,
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    autoplay: true,
    speed: 8000,
    autoplaySpeed: 0,
    cssEase: "linear",
    responsive: [
      {
        breakpoint: 1200, // 600px আগ পর্যন্ত 2টা দেখাবে
        settings: {
          slidesToShow: 5,
          slidesToScroll: 5,
        },
      },

      {
        breakpoint: 1000, // 600px আগ পর্যন্ত 2টা দেখাবে
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4,
        },
      },

      {
        breakpoint: 800, // 600px আগ পর্যন্ত 2টা দেখাবে
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        },
      },
      {
        breakpoint: 600, // 600px আগ পর্যন্ত 2টা দেখাবে
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 400, // 299px আগ পর্যন্ত একটা দেখাবে
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  //   What to Render
  let content;

  if (BestItemAllTimeLoading) {
    content = <div>Loading</div>;
  }

  if (
    !BestItemAllTimeLoading &&
    BestItemAllTime.data &&
    BestItemAllTime.data.best_item.results.length > 0
  ) {
    content = (
      <div className="sectionContentMargin">
        <Slider {...settings}>
          {BestItemAllTime.data.best_item.results.map((CardDetails, index) => (
            <div key={index}>
              <BestItemAllTimeCard CardDetails={CardDetails} />
            </div>
          ))}
        </Slider>
      </div>
    );
  }

  if (
    !BestItemAllTimeLoading &&
    BestItemAllTime.data &&
    BestItemAllTime.data.best_item.results.length === 0
  ) {
    content = (
      <div
        className="sectionContentMargin d-flex justify-content-center align-items-center"
        style={{ minHeight: "100px" }}
      >
        No Data Found
      </div>
    );
  }

  return (
    <div className="salesAnalysisCard">
      <div className="d-flex justify-content-between align-items-center flex-wrap gap-1">
        <div className="dashboard-title">Best Item All Time</div>
        <div className="d-flex justify-content-between align-items-center gap-2">
          <div>
            <DropdownButton
              id="dropdown-basic-button"
              title={
                bestItemAllTimeDropdownValue === "daily"
                  ? "Daily"
                  : bestItemAllTimeDropdownValue === "yearly"
                  ? "Yearly"
                  : "Monthly"
              }
              className="salesAnalysisDropdown"
            >
              <Dropdown.Item
                onClick={() => setBestItemAllTimeDropdownValue("daily")}
                disabled={
                  bestItemAllTimeDropdownValue === "daily" ? true : false
                }
              >
                Daily
              </Dropdown.Item>

              <Dropdown.Item
                onClick={() => setBestItemAllTimeDropdownValue("monthly")}
                disabled={
                  bestItemAllTimeDropdownValue === "monthly" ? true : false
                }
              >
                Monthly
              </Dropdown.Item>
              <Dropdown.Item
                onClick={() => setBestItemAllTimeDropdownValue("yearly")}
                disabled={
                  bestItemAllTimeDropdownValue === "yearly" ? true : false
                }
              >
                Yearly
              </Dropdown.Item>
            </DropdownButton>
          </div>
        </div>
      </div>

      {content}
    </div>
  );
}

export default BestItemAllTime;
