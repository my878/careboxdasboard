import React from "react";
import Chocolate from "../../../assets/icons/Dashboard/BestItemAllTime/Chocolate.png";
import "./BestItemAllTimeCard.css";

function BestItemAllTimeCard({ CardDetails, cardWidth }) {
  const { product_name, unit, sold_quantity, total_earned } = CardDetails;
  // console.log("CardDetails = ", CardDetails);
  return (
    <div
      className={`h-100 card bestItemAllTimeCardContainer ${cardWidth}`}
      style={{ borderRadius: "5px" }}
    >
      <div className="d-flex justify-content-center bestItemAllTimeCardImgContainer">
        <img
          src={Chocolate}
          alt=""
          className="bestItemAllTimeCardImg img-fluid"
        />
      </div>
      <div className="card-body text-start">
        <div className="bestItemAllTimeCardTitle">
          {product_name?.slice(0, 14) || "product_name"}
        </div>

        <div className="bestItemAllTimeCardweight">{unit || "unit"}</div>
      </div>
      <div
        className="card-footer text-start"
        style={{ backgroundColor: "#fafbfe" }}
      >
        <div className="d-flex justify-content-between flex-wrap gap-1">
          <div>
            <div className="bestItemAllTimeCardFooterTitle">Total Sale</div>
            <div className="bestItemAllTimeCardFooterValue">
              {sold_quantity || 0}
            </div>
          </div>

          <div>
            <div className="bestItemAllTimeCardFooterTitle">Total Earned</div>
            <div className="bestItemAllTimeCardFooterValue">
              ৳ {total_earned || 0}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default BestItemAllTimeCard;
