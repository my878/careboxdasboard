import React from "react";
import { useEffect } from "react";
import { Table, Spinner, Row, Col } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";

function convertDateFormat(inputDate) {
  // Split the input date into an array using the '-' delimiter
  var dateArray = inputDate.split("-");

  // Rearrange the elements to form the new date format
  var convertedDate = dateArray[2] + "-" + dateArray[1] + "-" + dateArray[0];

  // Return the converted date
  return convertedDate;
}

function VoidReportDetails({ fromDate, toDate }) {
  const dispatch = useDispatch();

  // useEffect(()=>{
  //   dispatch(getVoidReportDetails({fromDate, toDate}))
  // },[])

  const {
    VoidReportDetailsLoading,
    VoidReportDetailsError,
    VoidReportDetails,
  } = useSelector((state) => state.VoidReportDetails);

  let tableContent = null;
  let tableContent2 = null;

  if (VoidReportDetailsLoading) {
    tableContent = (
      <tr>
        <td colSpan={12}>
          <Spinner animation="border" variant="warning" />
        </td>
      </tr>
    );
  }

  if (VoidReportDetails?.data) {
    // console.log("VoidReportDetails.data ",VoidReportDetails.data);
    if (VoidReportDetails.data.void_data.length) {
      tableContent = VoidReportDetails.data.void_data.map((product, index) => (
        <tr key={index} className="text-start">
          <td>{product.barcode || "N/A"}</td>
          <td>{`${product.product_name} ${product.style_size} ${product.sub_category}`}</td>
          <td>{product.sale_date}</td>
          <td>{product.void_date}</td>
          <td>{product.sold_quantity}</td>
          <td>{product.quantity}</td>
          {/* Void(qty) = quantity */}

          <td>{product.mrp}</td>
          <td>{product.discount_amount}</td>
          <td>{product.vat_amount}</td>
          <td>{product.net_amount}</td>
          {/* Void Amount = net_amount */}

          <td>{product.void_type}</td>
          <td>{product.voided_by}</td>
        </tr>
      ));

      if (
        VoidReportDetails.data.void_data.length &&
        VoidReportDetails.data.sub_total
      ) {
        tableContent2 = (
          <tr className="grand-total-row text-start">
            <td>Grand Total:</td>
            <td></td>
            <td></td>
            <td></td>
            <td>{VoidReportDetails.data.sub_total.total_sold_quantity}</td>
            <td>{VoidReportDetails.data.sub_total.total_quantity}</td>
            <td>{VoidReportDetails.data.sub_total.total_mrp}</td>
            <td>{VoidReportDetails.data.sub_total.total_discount_amount}</td>
            <td>{VoidReportDetails.data.sub_total.total_vat_amount}</td>
            <td>{VoidReportDetails.data.sub_total.total_net_amount}</td>
            {/* Void Amount = net_amount */}

            <td></td>
            <td></td>
          </tr>
        );
      }
    } else {
      tableContent = (
        <tr>
          <td colSpan={12}>No Data Found</td>
        </tr>
      );
    }
  }

  return (
    <>
      <Row
        className="mx-0 mt-3 py-2 table-header"
        style={{
          border: "1px solid #dee2e6",
          fontSize: "14px",
        }}
      >
        <Col>
          <b> From Date:</b> {fromDate ? convertDateFormat(fromDate) : "N/A"}
        </Col>
        <Col>
          <b>To Date:</b> {toDate ? convertDateFormat(toDate) : "N/A"}
        </Col>
      </Row>
      <Table className="" responsive bordered>
        <thead className="report-table-header">
          <tr className="">
            <th>Barcode</th>
            <th>Product Information</th>
            <th>Sale Date</th>
            <th>Void Date</th>
            <th>Sold(Qty)</th>
            <th>Void(Qty)</th>
            <th>Total MRP</th>
            <th>Disc. Amount</th>
            <th>VAT Amount</th>
            <th>Void Amount</th>
            <th>Void Status</th>
            <th>Void By</th>
          </tr>
        </thead>
        <tbody className="text-center report-table-body">
          {tableContent}
          {tableContent2}
        </tbody>
      </Table>
    </>
  );
}

export default VoidReportDetails;
