import React from "react";
import { Table, Spinner, Row, Col } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";

function convertDateFormat(inputDate) {
  // Split the input date into an array using the '-' delimiter
  var dateArray = inputDate.split("-");

  // Rearrange the elements to form the new date format
  var convertedDate = dateArray[2] + "-" + dateArray[1] + "-" + dateArray[0];

  // Return the converted date
  return convertedDate;
}

function VoidSummary({ fromDate, toDate }) {
  const dispatch = useDispatch();

  // useEffect(()=>{
  //   dispatch(getVoidSummary({fromDate, toDate}))
  // },[])

  const { VoidSummaryLoading, VoidSummaryError, VoidSummary } = useSelector(
    (state) => state.VoidSummary
  );

  let tableContent = null;
  let tableContent2 = null;

  if (VoidSummaryLoading) {
    tableContent = (
      <tr>
        <td colSpan={12}>
          <Spinner animation="border" variant="warning" />
        </td>
      </tr>
    );
  }

  if (VoidSummary?.data) {
    // console.log("VoidSummary = " , VoidSummary.data);
    if (VoidSummary.data.void_data.length) {
      tableContent = VoidSummary.data.void_data.map((product, index) => (
        <tr key={index} className="text-start">
          <td>{product.invoice_no}</td>
          <td>{product.mrp}</td>
          <td>{product.discount_amount}</td>
          <td>{product.vat_amount}</td>
          <td>{product.net_amount}</td>
          {/* Void Amount = net_amount */}

          <td>{product.void_cost}</td>
          {/* return cost = void cost */}

          <td>{product.pft_amount}</td>
          <td>{product.gross_profit}</td>
          <td>{product.void_date}</td>
          <td>{product.void_time.slice(0, 8)}</td>
          <td>{product.ref_invoice}</td>
          <td>{product.voided_by}</td>
        </tr>
      ));

      if (VoidSummary.data.void_data.length && VoidSummary.data.sub_total) {
        tableContent2 = (
          <tr className="grand-total-row text-start">
            <td>Grand Total:</td>
            <td>{VoidSummary.data.sub_total.total_mrp}</td>
            <td>{VoidSummary.data.sub_total.total_discount_amount}</td>
            <td>{VoidSummary.data.sub_total.total_vat_amount}</td>
            <td>{VoidSummary.data.sub_total.total_net_amount}</td>
            <td>{VoidSummary.data.sub_total.total_void_cost}</td>
            <td>{VoidSummary.data.sub_total.total_pft_amount}</td>
            <td>{VoidSummary.data.sub_total.total_gp}</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
        );
      }
    } else {
      tableContent = (
        <tr>
          <td colSpan={12}>No Data Found</td>
        </tr>
      );
    }
  }

  return (
    <>
      <Row
        className="mx-0 mt-3 py-2 table-header"
        style={{
          border: "1px solid #dee2e6",
          fontSize: "14px",
        }}
      >
        <Col>
          <b> From Date:</b> {fromDate ? convertDateFormat(fromDate) : "N/A"}
        </Col>
        <Col>
          <b>To Date:</b> {toDate ? convertDateFormat(toDate) : "N/A"}
        </Col>
      </Row>
      <Table className="" responsive bordered>
        <thead className="text-start report-table-header">
          <tr>
            <th>Invoice No.</th>
            <th>MRP</th>
            <th>Disc. Amt.</th>
            <th>VAT</th>
            <th>Void Amt.</th>
            <th>Return Cost</th>
            <th>Pft Amt.</th>
            <th>GP%</th>
            <th>Void Date</th>
            <th>I. Time</th>
            <th>Ref_Invoice</th>
            <th>Void By</th>
          </tr>
        </thead>
        <tbody className="text-center report-table-body">
          {tableContent}
          {tableContent2}
        </tbody>
      </Table>
    </>
  );
}

export default VoidSummary;
