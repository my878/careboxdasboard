import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Accordion } from "react-bootstrap";
import { Link, useLocation, useNavigate } from "react-router-dom";
import logo from "../../assets/logo/care-box-logo.png";
import dashboardLogo from "../../assets/icons/Dashboard.png";
import saleLogo from "../../assets/icons/sale-report.png";
import voidLogo from "../../assets/icons/void-report.png";
import refundLogo from "../../assets/icons/refund.png";
import "./Sidebar.css";
import { SidebarAction } from "../../redux/actions/SidebarActions";

const SideBar = () => {
  const dispatch = useDispatch();
  const location = useLocation();
  const navigate = useNavigate();
  const [activeState, setActiveState] = useState();
  // const [toggleClick, setToggleClick] = useState(false);
  const { toggleClick } = useSelector((state) => state.sideBarState);
  //console.log(toggleClick);

  // useEffect(() => {
  //   if (productId !== null && location.pathname === `/product/${productId}`) {
  //     console.log("second");
  //     setActiveState("second");
  //   }
  // }, [productId]);
  //console.log(activeState);

  // useEffect(() => {
  //   if (localStorage.getItem("menuState") === "true") {
  //     setSideMenuState(true);
  //   } else {
  //     setSideMenuState(false);
  //   }
  // }, [localStorage.getItem("menuState"), props]);
  const detectLocation = (pathname) => {
    if (location.pathname === pathname) {
      return "active";
    } else {
      return "";
    }
  };
  const handleChnageSidebar = () => {
    if (toggleClick === true) {
      dispatch(SidebarAction(false));
    } else {
      dispatch(SidebarAction(true));
    }
  };
  // console.log(props);
  return (
    <div className={toggleClick ? "sidebar d-none" : "sidebar"}>
      <div className="d-flex justify-content-end m-2 d-md-none">
        <button
          type="button"
          className="btn-close"
          onClick={handleChnageSidebar}
        ></button>
      </div>
      <div className="mt-5 mb-5 d-flex justify-content-center">
        <img
          src={logo}
          alt="logo"
          style={{ maxHeight: "5.3vh", cursor: "pointer" }}
          onClick={() => navigate("/")}
        />
      </div>
      <Accordion className="me-2 ms-2">
        <Accordion.Item className="mb-1 manual-body" eventKey="first">
          <Link to="/" className={detectLocation("/")}>
            <img
              src={dashboardLogo}
              alt="dashboard"
              height={24}
              style={{ paddingRight: "1rem" }}
            />{" "}
            Dashboard
          </Link>
        </Accordion.Item>
        <Accordion.Item className="manual-body" eventKey="second">
          <Link
            to="/sales-reports"
            className={detectLocation("/sales-reports")}
          >
            <img
              src={saleLogo}
              alt="saleLogo"
              height={24}
              style={{ paddingRight: "1rem" }}
            />{" "}
            Sales Report
          </Link>
        </Accordion.Item>
        
        <Accordion.Item className="manual-body" eventKey="second">
          <Link
            to="/void-reports"
            className={detectLocation("/void-reports")}
          >
            <img
              src={voidLogo}
              alt="voidLogo"
              height={24}
              style={{ paddingRight: "1rem" }}
            />{" "}
            Void Report
          </Link>
        </Accordion.Item>
        {/* <Accordion.Item className="manual-body" eventKey="second">
          <Link to="/refund" className={detectLocation("/refund")}>
            <img
              src={refundLogo}
              alt="refundLogo"
              height={24}
              style={{ paddingRight: "1rem" }}
            />{" "}
            Refund
          </Link>
        </Accordion.Item> */}
      </Accordion>
    </div>
  );
};

export default SideBar;
