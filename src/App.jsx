import React from "react";
import { SkeletonTheme } from "react-loading-skeleton";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import PrivateRoute from "./Routes/PrivateRoute";
import Dashboard from "./pages/Dashboard/Dashboard";
import Login from "./pages/Login/Login";
import NotFoundPage from "./pages/NotFoundPage/NotFoundPage";
import SalesReports from "./pages/SalesReport/SalesReport";
import VoidReport from "./pages/VoidReport/VoidReport";

const App = () => {
  return (
    <>
      <SkeletonTheme
        baseColor="#d1d5db"
        highlightColor="#686B70"
        borderRadius="3px"
        duration={0.5}
      >
        <BrowserRouter>
          <Routes>
            <Route
              path="/"
              element={
                <PrivateRoute>
                  <Dashboard></Dashboard>
                </PrivateRoute>
              }
            />
            <Route path="/login" element={<Login></Login>} />
            <Route
              path="/sales-reports"
              element={
                <PrivateRoute>
                  <SalesReports></SalesReports>
                </PrivateRoute>
              }
            />
            <Route path="*" element={<NotFoundPage></NotFoundPage>} />
            <Route path="/void-reports" element={<VoidReport />} />
          </Routes>
        </BrowserRouter>
      </SkeletonTheme>
    </>
  );
};

export default App;
