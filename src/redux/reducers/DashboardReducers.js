import {
  ADD_DAHBOARD_OVERLAY,
  BEST_ITEM_ALL_TIME_FAILURE,
  BEST_ITEM_ALL_TIME_REQUEST,
  BEST_ITEM_ALL_TIME_SUCCESS,
  CHANGE_DAHBOARD_OVERLAY_STATUS,
  ONLINE_SERVICE_OVERVIEW_FAILURE,
  ONLINE_SERVICE_OVERVIEW_REQUEST,
  ONLINE_SERVICE_OVERVIEW_SUCCESS,
  PROCUREMENT_OVERVIEW_FAILURE,
  PROCUREMENT_OVERVIEW_REQUEST,
  PROCUREMENT_OVERVIEW_SUCCESS,
  PRODUCT_LOCATION_OVERVIEW_FAILURE,
  PRODUCT_LOCATION_OVERVIEW_REQUEST,
  PRODUCT_LOCATION_OVERVIEW_SUCCESS,
  REMOVE_DAHBOARD_OVERLAY,
  SALES_ANALYSIS_FAILURE,
  SALES_ANALYSIS_REQUEST,
  SALES_ANALYSIS_SUCCESS,
  SALES_OVERVIEW_FAILURE,
  SALES_OVERVIEW_REQUEST,
  SALES_OVERVIEW_SUCCESS
} from "../type.js";


// Sales_Overview

const SalesOverviewInitialState = {
    salesOverviewLoading: false,
    salesOverviewError: "",
    salesOverview: {}
  };
  
  export const SalesOverviewReducer = (state = SalesOverviewInitialState, action) =>{
    switch (action.type) {
      case SALES_OVERVIEW_REQUEST:
        return {
          ...state,
          salesOverviewLoading: true,
          salesOverviewError: '',
          salesOverview: {}
        };
  
      case SALES_OVERVIEW_SUCCESS:
        return {
          ...state,
          salesOverviewLoading: false,
          salesOverviewError: '',
          salesOverview: action.payload
        };
  
      case SALES_OVERVIEW_FAILURE:
        return {
          ...state,
          salesOverviewLoading: false,
          salesOverviewError: action.payload,
          salesOverview: {}
        };
  
      default:
        return state;
    }
  };

  // Product_Location_Overview
  const ProductLocationOverviewInitialState = {
    ProductLocationOverviewLoading: false,
    ProductLocationOverviewError: "",
    ProductLocationOverview: {}
  };

  export const ProductLocationOverviewReducer = (state = ProductLocationOverviewInitialState, action) =>{
    switch (action.type) {
      case PRODUCT_LOCATION_OVERVIEW_REQUEST:
        return {
          ...state,
          ProductLocationOverviewLoading: true,
          ProductLocationOverviewError: '',
          ProductLocationOverview: {}
        };
  
      case PRODUCT_LOCATION_OVERVIEW_SUCCESS:
        return {
          ...state,
          ProductLocationOverviewLoading: false,
          ProductLocationOverviewError: '',
          ProductLocationOverview: action.payload
        };
  
      case PRODUCT_LOCATION_OVERVIEW_FAILURE:
        return {
          ...state,
          ProductLocationOverviewLoading: false,
          ProductLocationOverviewError: action.payload,
          ProductLocationOverview: {}
        };
  
      default:
        return state;
    }
  };


  // Online_Service_Overview
  const OnlineServiceOverviewInitialState = {
    OnlineServiceOverviewLoading: false,
    OnlineServiceOverviewError: "",
    OnlineServiceOverview: {}
  };

  export const OnlineServiceOverviewReducer = (state = OnlineServiceOverviewInitialState, action) =>{
    switch (action.type) {
      case ONLINE_SERVICE_OVERVIEW_REQUEST:
        return {
          ...state,
          OnlineServiceOverviewLoading: true,
          OnlineServiceOverviewError: '',
          OnlineServiceOverview: {}
        };
  
      case ONLINE_SERVICE_OVERVIEW_SUCCESS:
        return {
          ...state,
          OnlineServiceOverviewLoading: false,
          OnlineServiceOverviewError: '',
          OnlineServiceOverview: action.payload
        };
  
      case ONLINE_SERVICE_OVERVIEW_FAILURE:
        return {
          ...state,
          OnlineServiceOverviewLoading: false,
          OnlineServiceOverviewError: action.payload,
          OnlineServiceOverview: {}
        };
  
      default:
        return state;
    }
  };

  // Procurement_Overview
  const ProcurementOverviewInitialState = {
    ProcurementOverviewLoading: false,
    ProcurementOverviewError: "",
    ProcurementOverview: {}
  };

  export const ProcurementOverviewReducer = (state = ProcurementOverviewInitialState, action) =>{
    switch (action.type) {
      case PROCUREMENT_OVERVIEW_REQUEST:
        return {
          ...state,
          ProcurementOverviewLoading: true,
          ProcurementOverviewError: '',
          ProcurementOverview: {}
        };
  
      case PROCUREMENT_OVERVIEW_SUCCESS:
        return {
          ...state,
          ProcurementOverviewLoading: false,
          ProcurementOverviewError: '',
          ProcurementOverview: action.payload
        };
  
      case PROCUREMENT_OVERVIEW_FAILURE:
        return {
          ...state,
          ProcurementOverviewLoading: false,
          ProcurementOverviewError: action.payload,
          ProcurementOverview: {}
        };
  
      default:
        return state;
    }
  };

  // Dahboard_Overlay
  const DahboardOverlayInitialState = {
    status : false,
  }

  export const DahboardOverlayReducer = (state = DahboardOverlayInitialState, action) => {
    switch (action.type) {
      case ADD_DAHBOARD_OVERLAY:
        return {
          ...state,
          status : true,
        };

        
    case REMOVE_DAHBOARD_OVERLAY:        
    return {
      ...state,
      status : false,
    };

    case CHANGE_DAHBOARD_OVERLAY_STATUS:
      // console.log(action.payload);      
      return {
        ...state,
        status : action.payload,
      };

      
      default:
        return state;
    }
    
  }

  // Sales_Analysis

  const SalesAnalysisInitialState = {
    SalesAnalysisLoading: false,
    SalesAnalysisError: "",
    SalesAnalysis: {}
  };

  export const SalesAnalysisReducer = (state = SalesAnalysisInitialState, action) =>{
    switch (action.type) {
      case SALES_ANALYSIS_REQUEST:
        return {
          ...state,
          SalesAnalysisLoading: true,
          SalesAnalysisError: '',
          SalesAnalysis: {}
        };
  
      case SALES_ANALYSIS_SUCCESS:
        return {
          ...state,
          SalesAnalysisLoading: false,
          SalesAnalysisError: '',
          SalesAnalysis: action.payload
        };
  
      case SALES_ANALYSIS_FAILURE:
        return {
          ...state,
          SalesAnalysisLoading: false,
          SalesAnalysisError: action.payload,
          SalesAnalysis: {}
        };
  
      default:
        return state;
    }
  };


  // BestItemAllTime

  const BestItemAllTimeInitialState = {
    BestItemAllTimeLoading: false,
    BestItemAllTimeError: "",
    BestItemAllTime: {}
  };

  export const BestItemAllTimeReducer = (state = BestItemAllTimeInitialState, action) =>{
    switch (action.type) {
      case BEST_ITEM_ALL_TIME_REQUEST:
        return {
          ...state,
          BestItemAllTimeLoading: true,
          BestItemAllTimeError: '',
          BestItemAllTime: {}
        };
  
      case BEST_ITEM_ALL_TIME_SUCCESS:
        return {
          ...state,
          BestItemAllTimeLoading: false,
          BestItemAllTimeError: '',
          BestItemAllTime: action.payload
        };
  
      case BEST_ITEM_ALL_TIME_FAILURE:
        return {
          ...state,
          BestItemAllTimeLoading: false,
          BestItemAllTimeError: action.payload,
          BestItemAllTime: {}
        };
  
      default:
        return state;
    }
  };
