import {
    VOID_REPORT_DETAILS_REQUEST,
    VOID_REPORT_DETAILS_SUCCESS,
    VOID_REPORT_DETAILS_FAILURE,
    VOID_SUMMARY_REQUEST,
    VOID_SUMMARY_SUCCESS,
    VOID_SUMMARY_FAILURE
  } from "../type";


 // VoidReportDetails

 const VoidReportDetailsInitialState = {
    VoidReportDetailsLoading: false,
    VoidReportDetailsError: "",
    VoidReportDetails: {}
  };

  export const VoidReportDetailsReducer = (state = VoidReportDetailsInitialState, action) =>{
    switch (action.type) {
      case VOID_REPORT_DETAILS_REQUEST:
        return {
          ...state,
          VoidReportDetailsLoading: true,
          VoidReportDetailsError: '',
          VoidReportDetails: {}
        };
  
      case VOID_REPORT_DETAILS_SUCCESS:
        return {
          ...state,
          VoidReportDetailsLoading: false,
          VoidReportDetailsError: '',
          VoidReportDetails: action.payload
        };
  
      case VOID_REPORT_DETAILS_FAILURE:
        return {
          ...state,
          VoidReportDetailsLoading: false,
          VoidReportDetailsError: action.payload,
          VoidReportDetails: {}
        };
  
      default:
        return state;
    }
  };


   // VoidSummary

 const VoidSummaryInitialState = {
    VoidSummaryLoading: false,
    VoidSummaryError: "",
    VoidSummary: {}
  };

  export const VoidSummaryReducer = (state = VoidSummaryInitialState, action) =>{
    switch (action.type) {
      case VOID_SUMMARY_REQUEST:
        return {
          ...state,
          VoidSummaryLoading: true,
          VoidSummaryError: '',
          VoidSummary: {}
        };
  
      case VOID_SUMMARY_SUCCESS:
        return {
          ...state,
          VoidSummaryLoading: false,
          VoidSummaryError: '',
          VoidSummary: action.payload
        };
  
      case VOID_SUMMARY_FAILURE:
        return {
          ...state,
          VoidSummaryLoading: false,
          VoidSummaryError: action.payload,
          VoidSummary: {}
        };
  
      default:
        return state;
    }
  };