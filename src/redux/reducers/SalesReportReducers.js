import {
  BARCODE_SALE_DETAIL_REQUEST,
  BARCODE_SALE_DETAIL_SUCCESS,
  BARCODE_SALE_DETAIL_FAILURE,
  PRODUCT_SALE_DETAIL_REQUEST,
  PRODUCT_SALE_DETAIL_SUCCESS,
  PRODUCT_SALE_DETAIL_FAILURE,
  DAILY_INVOICE_SALE_REQUEST,
  DAILY_INVOICE_SALE_SUCCESS,
  DAILY_INVOICE_SALE_FAILURE,
  CASHIER_WISE_SALE_DETAIL_REQUEST,
  CASHIER_WISE_SALE_DETAIL_SUCCESS,
  CASHIER_WISE_SALE_DETAIL_FAILURE,
  GENERIC_WISE_SALE_REQUEST,
  GENERIC_WISE_SALE_SUCCESS,
  GENERIC_WISE_SALE_FAILURE,
  ALL_CATEGORY_WISE_SALE_REQUEST,
  ALL_CATEGORY_WISE_SALE_SUCCESS,
  ALL_CATEGORY_WISE_SALE_FAILURE,
  ALL_SUBCATEGORY_WISE_SALE_REQUEST,
  ALL_SUBCATEGORY_WISE_SALE_SUCCESS,
  ALL_SUBCATEGORY_WISE_SALE_FAILURE,
  HOURLY_SALES_REPORT_REQUEST,
  HOURLY_SALES_REPORT_SUCCESS,
  HOURLY_SALES_REPORT_FAILURE,
  INVOICE_WISE_HOURLY_SALES_REQUEST,
  INVOICE_WISE_HOURLY_SALES_SUCCESS,
  INVOICE_WISE_HOURLY_SALES_FAILURE,
  SUPPLIER_WISE_PRODUCT_SALES_REQUEST,
  SUPPLIER_WISE_PRODUCT_SALES_SUCCESS,
  SUPPLIER_WISE_PRODUCT_SALES_FAILURE,
  CATEGORY_WISE_SUPPLIER_SALES_REQUEST,
  CATEGORY_WISE_SUPPLIER_SALES_SUCCESS,
  CATEGORY_WISE_SUPPLIER_SALES_FAILURE,
  ALL_SUPPLIER_WISE_SALES_REQUEST,
  ALL_SUPPLIER_WISE_SALES_SUCCESS,
  ALL_SUPPLIER_WISE_SALES_FAILURE,
  SUPPLIER_WISE_CATEGORY_SALES_REQUEST,
  SUPPLIER_WISE_CATEGORY_SALES_SUCCESS,
  SUPPLIER_WISE_CATEGORY_SALES_FAILURE,
  DAILY_SALE_SUMMARY_REQUEST,
  DAILY_SALE_SUMMARY_SUCCESS,
  DAILY_SALE_SUMMARY_FAILURE,
  DAILY_COUNTER_WISE_SALES_REQUEST,
  DAILY_COUNTER_WISE_SALES_SUCCESS,
  DAILY_COUNTER_WISE_SALES_FAILURE,
  DAILY_COUNTER_WISE_EXCHANGE_REQUEST,
  DAILY_COUNTER_WISE_EXCHANGE_SUCCESS,
  DAILY_COUNTER_WISE_EXCHANGE_FAILURE,
  CATEGORY_WISE_PRODUCT_SALES_REQUEST,
  CATEGORY_WISE_PRODUCT_SALES_SUCCESS,
  CATEGORY_WISE_PRODUCT_SALES_FAILURE,
  SUBCATEGORY_WISE_PRODUCT_SALES_REQUEST,
  SUBCATEGORY_WISE_PRODUCT_SALES_SUCCESS,
  SUBCATEGORY_WISE_PRODUCT_SALES_FAILURE,
  SECTION_WISE_SALE_REQUEST,
  SECTION_WISE_SALE_SUCCESS,
  SECTION_WISE_SALE_FAILURE,
  SUBCATEGORY_WISE_SUPPLIER_SALES_REQUEST,
  SUBCATEGORY_WISE_SUPPLIER_SALES_SUCCESS,
  SUBCATEGORY_WISE_SUPPLIER_SALES_FAILURE,
  SUPPLIER_WISE_SUBCATEGORY_SALES_REQUEST,
  SUPPLIER_WISE_SUBCATEGORY_SALES_SUCCESS,
  SUPPLIER_WISE_SUBCATEGORY_SALES_FAILURE,
} from "../type.js";

export const barcodeWiseSaleReducer = (
  state = { barcodeSales: {} },
  action
) => {
  switch (action.type) {
    case BARCODE_SALE_DETAIL_REQUEST:
      return { barcodeLoading: true, ...state };
    case BARCODE_SALE_DETAIL_SUCCESS:
      return {
        barcodeLoading: false,
        barcodeSales: action.payload,
      };
    case BARCODE_SALE_DETAIL_FAILURE:
      return {
        barcodeLoading: false,
        barcodeError: action.payload,
      };
    default:
      return state;
  }
};

export const productWiseSaleReducer = (
  state = { productSales: {} },
  action
) => {
  switch (action.type) {
    case PRODUCT_SALE_DETAIL_REQUEST:
      return { productLoading: true, ...state };
    case PRODUCT_SALE_DETAIL_SUCCESS:
      return {
        productLoading: false,
        productSales: action.payload,
      };
    case PRODUCT_SALE_DETAIL_FAILURE:
      return {
        productLoading: false,
        productError: action.payload,
      };
    default:
      return state;
  }
};

export const dailyInvoiceWiseSaleReducer = (
  state = { dailyInvoiceSales: {} },
  action
) => {
  switch (action.type) {
    case DAILY_INVOICE_SALE_REQUEST:
      return { dailyInvoiceLoading: true, ...state };
    case DAILY_INVOICE_SALE_SUCCESS:
      return {
        dailyInvoiceLoading: false,
        dailyInvoiceSales: action.payload,
      };
    case DAILY_INVOICE_SALE_FAILURE:
      return {
        dailyInvoiceLoading: false,
        dailyInvoiceError: action.payload,
      };
    default:
      return state;
  }
};

export const cashierWiseSaleReducer = (
  state = { cashierWiseSales: {} },
  action
) => {
  switch (action.type) {
    case CASHIER_WISE_SALE_DETAIL_REQUEST:
      return { cashierWiseLoading: true, ...state };
    case CASHIER_WISE_SALE_DETAIL_SUCCESS:
      return {
        cashierWiseLoading: false,
        cashierWiseSales: action.payload,
      };
    case CASHIER_WISE_SALE_DETAIL_FAILURE:
      return {
        cashierWiseLoading: false,
        cashierWiseError: action.payload,
      };
    default:
      return state;
  }
};

export const dailySaleSummaryReducer = (state = { dailySales: {} }, action) => {
  switch (action.type) {
    case DAILY_SALE_SUMMARY_REQUEST:
      return { dailyLoading: true, ...state };
    case DAILY_SALE_SUMMARY_SUCCESS:
      return {
        dailyLoading: false,
        dailySales: action.payload,
      };
    case DAILY_SALE_SUMMARY_FAILURE:
      return {
        dailyLoading: false,
        dailyError: action.payload,
      };
    default:
      return state;
  }
};

export const genericWiseSaleReducer = (
  state = { genericWiseSales: {} },
  action
) => {
  switch (action.type) {
    case GENERIC_WISE_SALE_REQUEST:
      return { genericWiseLoading: true, ...state };
    case GENERIC_WISE_SALE_SUCCESS:
      return {
        genericWiseLoading: false,
        genericWiseSales: action.payload,
      };
    case GENERIC_WISE_SALE_FAILURE:
      return {
        genericWiseLoading: false,
        genericWiseError: action.payload,
      };
    default:
      return state;
  }
};

export const allCategoryWiseSaleReducer = (
  state = { allCategoryWiseSales: {} },
  action
) => {
  switch (action.type) {
    case ALL_CATEGORY_WISE_SALE_REQUEST:
      return { allCategoryWiseLoading: true, ...state };
    case ALL_CATEGORY_WISE_SALE_SUCCESS:
      return {
        allCategoryWiseLoading: false,
        allCategoryWiseSales: action.payload,
      };
    case ALL_CATEGORY_WISE_SALE_FAILURE:
      return {
        allCategoryWiseLoading: false,
        allCategoryWiseError: action.payload,
      };
    default:
      return state;
  }
};

export const allSubCategoryWiseSaleReducer = (
  state = { allSubCategoryWiseSales: {} },
  action
) => {
  switch (action.type) {
    case ALL_SUBCATEGORY_WISE_SALE_REQUEST:
      return { allSubCategoryWiseLoading: true, ...state };
    case ALL_SUBCATEGORY_WISE_SALE_SUCCESS:
      return {
        allSubCategoryWiseLoading: false,
        allSubCategoryWiseSales: action.payload,
      };
    case ALL_SUBCATEGORY_WISE_SALE_FAILURE:
      return {
        allSubCategoryWiseLoading: false,
        allSubCategoryWiseError: action.payload,
      };
    default:
      return state;
  }
};

export const hourlySaleReportReducer = (
  state = { hourlySalesList: {} },
  action
) => {
  switch (action.type) {
    case HOURLY_SALES_REPORT_REQUEST:
      return { hourlySalesLoading: true, ...state };
    case HOURLY_SALES_REPORT_SUCCESS:
      return {
        hourlySalesLoading: false,
        hourlySalesList: action.payload,
      };
    case HOURLY_SALES_REPORT_FAILURE:
      return {
        hourlySalesLoading: false,
        hourlySalesError: action.payload,
      };
    default:
      return state;
  }
};

export const invoiceWiseSaleReportReducer = (
  state = { invoiceSalesList: {} },
  action
) => {
  switch (action.type) {
    case INVOICE_WISE_HOURLY_SALES_REQUEST:
      return { invoiceSalesLoading: true, ...state };
    case INVOICE_WISE_HOURLY_SALES_SUCCESS:
      return {
        invoiceSalesLoading: false,
        invoiceSalesList: action.payload,
      };
    case INVOICE_WISE_HOURLY_SALES_FAILURE:
      return {
        invoiceSalesLoading: false,
        invoiceSalesError: action.payload,
      };
    default:
      return state;
  }
};

export const supplierWiseSaleReportReducer = (
  state = { supplierSalesList: {} },
  action
) => {
  switch (action.type) {
    case SUPPLIER_WISE_PRODUCT_SALES_REQUEST:
      return { supplierSalesLoading: true, ...state };
    case SUPPLIER_WISE_PRODUCT_SALES_SUCCESS:
      return {
        suppliereSalesLoading: false,
        supplierSalesList: action.payload,
      };
    case SUPPLIER_WISE_PRODUCT_SALES_FAILURE:
      return {
        supplierSalesLoading: false,
        supplierSalesError: action.payload,
      };
    default:
      return state;
  }
};

export const categoryWiseSupplierReportReducer = (
  state = { categorySupplierSale: {} },
  action
) => {
  switch (action.type) {
    case CATEGORY_WISE_SUPPLIER_SALES_REQUEST:
      return { categorySupplierLoading: true, ...state };
    case CATEGORY_WISE_SUPPLIER_SALES_SUCCESS:
      return {
        categorySupplierLoading: false,
        categorySupplierSale: action.payload,
      };
    case CATEGORY_WISE_SUPPLIER_SALES_FAILURE:
      return {
        categorySupplierLoading: false,
        categorySupplierError: action.payload,
      };
    default:
      return state;
  }
};

export const allSupplierSummaryReportReducer = (
  state = { allSupplierSummary: {} },
  action
) => {
  switch (action.type) {
    case ALL_SUPPLIER_WISE_SALES_REQUEST:
      return { allSupplierSummaryLoading: true, ...state };
    case ALL_SUPPLIER_WISE_SALES_SUCCESS:
      return {
        allSupplierSummaryLoading: false,
        allSupplierSummary: action.payload,
      };
    case ALL_SUPPLIER_WISE_SALES_FAILURE:
      return {
        allSupplierSummaryLoading: false,
        allSupplierSummaryError: action.payload,
      };
    default:
      return state;
  }
};

export const supplierWiseCategoryReportReducer = (
  state = { supplierCategorySale: {} },
  action
) => {
  switch (action.type) {
    case SUPPLIER_WISE_CATEGORY_SALES_REQUEST:
      return { supplierCategorySaleLoading: true, ...state };
    case SUPPLIER_WISE_CATEGORY_SALES_SUCCESS:
      return {
        supplierCategorySaleLoading: false,
        supplierCategorySale: action.payload,
      };
    case SUPPLIER_WISE_CATEGORY_SALES_FAILURE:
      return {
        supplierCategorySaleLoading: false,
        supplierCategorySaleError: action.payload,
      };
    default:
      return state;
  }
};

export const dailyCounterSaleReducer = (
  state = { dailyCounterSales: {} },
  action
) => {
  switch (action.type) {
    case DAILY_COUNTER_WISE_SALES_REQUEST:
      return { dailyCounterLoading: true, ...state };
    case DAILY_COUNTER_WISE_SALES_SUCCESS:
      return {
        dailyCounterLoading: false,
        dailyCounterSales: action.payload,
      };
    case DAILY_COUNTER_WISE_SALES_FAILURE:
      return {
        dailyCounterLoading: false,
        dailyCounterError: action.payload,
      };
    default:
      return state;
  }
};

export const dailyCounterExchangeReducer = (
  state = { dailyCounterExchange: {} },
  action
) => {
  switch (action.type) {
    case DAILY_COUNTER_WISE_EXCHANGE_REQUEST:
      return { dailyExchangeLoading: true, ...state };
    case DAILY_COUNTER_WISE_EXCHANGE_SUCCESS:
      return {
        dailyExchangeLoading: false,
        dailyCounterExchange: action.payload,
      };
    case DAILY_COUNTER_WISE_EXCHANGE_FAILURE:
      return {
        dailyExchangeLoading: false,
        dailyExchangeError: action.payload,
      };
    default:
      return state;
  }
};

export const categoryWiseSaleReportReducer = (
  state = { categorySalesList: {} },
  action
) => {
  switch (action.type) {
    case CATEGORY_WISE_PRODUCT_SALES_REQUEST:
      return { categorySalesLoading: true, categorySalesList: {} };
    case CATEGORY_WISE_PRODUCT_SALES_SUCCESS:
      return {
        categorySalesLoading: false,
        categorySalesList: action.payload,
      };
    case CATEGORY_WISE_PRODUCT_SALES_FAILURE:
      return {
        categorySalesLoading: false,
        categorySalesError: action.payload,
      };
    default:
      return state;
  }
};

export const subcategoryWiseSaleReportReducer = (
  state = { subcategorySalesList: {} },
  action
) => {
  switch (action.type) {
    case SUBCATEGORY_WISE_PRODUCT_SALES_REQUEST:
      return { subcategorySalesLoading: true, subcategorySalesList: {} };
    case SUBCATEGORY_WISE_PRODUCT_SALES_SUCCESS:
      return {
        subcategorySalesLoading: false,
        subcategorySalesList: action.payload,
      };
    case SUBCATEGORY_WISE_PRODUCT_SALES_FAILURE:
      return {
        subcategorySalesLoading: false,
        subcategorySalesError: action.payload,
      };
    default:
      return state;
  }
};

export const sectionWiseSaleReportReducer = (
  state = { sectionSalesList: {} },
  action
) => {
  switch (action.type) {
    case SECTION_WISE_SALE_REQUEST:
      return { sectionSalesLoading: true, ...state };
    case SECTION_WISE_SALE_SUCCESS:
      return {
        sectionSalesLoading: false,
        sectionSalesList: action.payload,
      };
    case SECTION_WISE_SALE_FAILURE:
      return {
        sectionSalesLoading: false,
        sectionSalesError: action.payload,
      };
    default:
      return state;
  }
};

export const subcategoryWiseSupplierReportReducer = (
  state = { subcategorySupplierSale: {} },
  action
) => {
  switch (action.type) {
    case SUBCATEGORY_WISE_SUPPLIER_SALES_REQUEST:
      return { subcategorySupplierLoading: true, ...state };
    case SUBCATEGORY_WISE_SUPPLIER_SALES_SUCCESS:
      return {
        subcategorySupplierLoading: false,
        subcategorySupplierSale: action.payload,
      };
    case SUBCATEGORY_WISE_SUPPLIER_SALES_FAILURE:
      return {
        subcategorySupplierLoading: false,
        subcategorySupplierError: action.payload,
      };
    default:
      return state;
  }
};

export const supplierWiseSubcategoryReportReducer = (
  state = { supplierSubcategorySale: {} },
  action
) => {
  switch (action.type) {
    case SUPPLIER_WISE_SUBCATEGORY_SALES_REQUEST:
      return { supplierSubcategorySaleLoading: true, ...state };
    case SUPPLIER_WISE_SUBCATEGORY_SALES_SUCCESS:
      return {
        supplierSubcategorySaleLoading: false,
        supplierSubcategorySale: action.payload,
      };
    case SUPPLIER_WISE_SUBCATEGORY_SALES_FAILURE:
      return {
        supplierSubcategorySaleLoading: false,
        supplierSubcategorySaleError: action.payload,
      };
    default:
      return state;
  }
};
