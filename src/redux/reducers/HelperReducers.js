import {
  SUPPLIER_LIST_REQUEST,
  SUPPLIER_LIST_SUCCESS,
  SUPPLIER_LIST_FAILURE,
  CATEGORY_LIST_REQUEST,
  CATEGORY_LIST_SUCCESS,
  CATEGORY_LIST_FAILURE,
  SUBCATEGORY_LIST_REQUEST,
  SUBCATEGORY_LIST_SUCCESS,
  SUBCATEGORY_LIST_FAILURE,
  CASHIER_LIST_REQUEST,
  CASHIER_LIST_SUCCESS,
  CASHIER_LIST_FAILURE,
  COUNTER_LIST_REQUEST,
  COUNTER_LIST_SUCCESS,
  COUNTER_LIST_FAILURE,
} from "../type.js";

export const supplierListReducer = (state = { supplierList: {} }, action) => {
  switch (action.type) {
    case SUPPLIER_LIST_REQUEST:
      return { loading: true, ...state };
    case SUPPLIER_LIST_SUCCESS:
      return { loading: false, supplierList: action.payload };
    case SUPPLIER_LIST_FAILURE:
      return { loading: false, error: action.payload };
    default:
      return state;
  }
};

export const categoryListReducer = (state = { categoryList: {} }, action) => {
  switch (action.type) {
    case CATEGORY_LIST_REQUEST:
      return { categoryLoading: true, ...state };
    case CATEGORY_LIST_SUCCESS:
      return { categoryLoading: false, categoryList: action.payload };
    case CATEGORY_LIST_FAILURE:
      return { categoryLoading: false, categoryError: action.payload };
    default:
      return state;
  }
};

export const subcategoryListReducer = (
  state = { subcategoryList: {} },
  action
) => {
  switch (action.type) {
    case SUBCATEGORY_LIST_REQUEST:
      return { subcategoryLoading: true, ...state };
    case SUBCATEGORY_LIST_SUCCESS:
      return { subcategoryLoading: false, subcategoryList: action.payload };
    case SUBCATEGORY_LIST_FAILURE:
      return { subcategoryLoading: false, subcategoryError: action.payload };
    default:
      return state;
  }
};

export const cashierListReducer = (state = { cashierList: {} }, action) => {
  switch (action.type) {
    case CASHIER_LIST_REQUEST:
      return { cashierLoading: true, ...state };
    case CASHIER_LIST_SUCCESS:
      return { cashierLoading: false, cashierList: action.payload };
    case CASHIER_LIST_FAILURE:
      return { cashierLoading: false, cashierError: action.payload };
    default:
      return state;
  }
};

export const counterListReducer = (state = { counterList: {} }, action) => {
  switch (action.type) {
    case COUNTER_LIST_REQUEST:
      return { counterLoading: true, ...state };
    case COUNTER_LIST_SUCCESS:
      return { counterLoading: false, counterList: action.payload };
    case COUNTER_LIST_FAILURE:
      return { counterLoading: false, counterError: action.payload };
    default:
      return state;
  }
};
