import { TOGGLE_SIDE_BAR } from "../type";

export const SidebarReducer = (state = { toggleClick: false }, action) => {
  switch (action.type) {
    case TOGGLE_SIDE_BAR:
      return {
        ...state,
        toggleClick: action.payload,
      };
    default:
      return state;
  }
};
