import { composeWithDevTools } from "@redux-devtools/extension";
import { applyMiddleware, combineReducers, createStore } from "redux";
import thunk from "redux-thunk";
import {
  allCategoryWiseSaleReducer,
  allSubCategoryWiseSaleReducer,
  barcodeWiseSaleReducer,
  productWiseSaleReducer,
  dailyInvoiceWiseSaleReducer,
  cashierWiseSaleReducer,
  dailySaleSummaryReducer,
  genericWiseSaleReducer,
  hourlySaleReportReducer,
  invoiceWiseSaleReportReducer,
  supplierWiseSaleReportReducer,
  categoryWiseSupplierReportReducer,
  allSupplierSummaryReportReducer,
  supplierWiseCategoryReportReducer,
  dailyCounterSaleReducer,
  dailyCounterExchangeReducer,
  categoryWiseSaleReportReducer,
  subcategoryWiseSaleReportReducer,
  sectionWiseSaleReportReducer,
  subcategoryWiseSupplierReportReducer,
  supplierWiseSubcategoryReportReducer,
} from "./reducers/SalesReportReducers";
import {
  supplierListReducer,
  categoryListReducer,
  subcategoryListReducer,
  cashierListReducer,
  counterListReducer,
} from "./reducers/HelperReducers";
import { SidebarReducer } from "./reducers/SidebarReducers";
import { userDetailsReducer, userLoginReducer } from "./reducers/userReducers";

import {
  BestItemAllTimeReducer,
  DahboardOverlayReducer,
  OnlineServiceOverviewReducer,
  ProcurementOverviewReducer,
  ProductLocationOverviewReducer,
  SalesAnalysisReducer,
  SalesOverviewReducer,
} from "./reducers/DashboardReducers";


import {
  VoidReportDetailsReducer,
  VoidSummaryReducer 
} from "./reducers/VoidReportReducers.js";

const reducer = combineReducers({
  userLogin: userLoginReducer,
  userDetails: userDetailsReducer,
  sideBarState: SidebarReducer,
  salesOverview: SalesOverviewReducer,
  ProductLocationOverview: ProductLocationOverviewReducer,
  OnlineServiceOverview: OnlineServiceOverviewReducer,
  ProcurementOverview: ProcurementOverviewReducer,
  DahboardOverlay: DahboardOverlayReducer,
  SalesAnalysis: SalesAnalysisReducer,
  BestItemAllTime: BestItemAllTimeReducer,
  barcodeWiseSale: barcodeWiseSaleReducer,
  productWiseSale: productWiseSaleReducer,
  InvoiceWiseSale: dailyInvoiceWiseSaleReducer,
  cashierWiseSale: cashierWiseSaleReducer,
  dailySaleSummary: dailySaleSummaryReducer,
  genericWiseSale: genericWiseSaleReducer,
  allCategoryWiseSale: allCategoryWiseSaleReducer,
  allSubCategoryWiseSale: allSubCategoryWiseSaleReducer,
  hourlySaleReport: hourlySaleReportReducer,
  invoiceWiseSaleReport: invoiceWiseSaleReportReducer,
  supplierWiseSaleReport: supplierWiseSaleReportReducer,
  categoryWiseSupplierReport: categoryWiseSupplierReportReducer,
  allSupplierSummaryReport: allSupplierSummaryReportReducer,
  supplierWiseCategoryReport: supplierWiseCategoryReportReducer,
  dailyCounterSale: dailyCounterSaleReducer,
  dailyCounterExchange: dailyCounterExchangeReducer,
  categoryWiseSaleReport: categoryWiseSaleReportReducer,
  subcategoryWiseSaleReport: subcategoryWiseSaleReportReducer,
  sectionWiseSaleReport: sectionWiseSaleReportReducer,
  subcategoryWiseSupplierReport: subcategoryWiseSupplierReportReducer,
  supplierWiseSubcategoryReport: supplierWiseSubcategoryReportReducer,
  supplierList: supplierListReducer,
  categoryList: categoryListReducer,
  subcategoryList: subcategoryListReducer,
  cashierList: cashierListReducer,
  counterList: counterListReducer,
  VoidReportDetails: VoidReportDetailsReducer,
  VoidSummary : VoidSummaryReducer 
});

const menuToggleState =
  localStorage.getItem("menuState") === "true" ? true : false;

const userInfoFromStorage = localStorage.getItem("userInfo")
  ? JSON.parse(localStorage.getItem("userInfo"))
  : null;

//console.log(menuToggleState);
const initialState = {
  userLogin: {
    userInfo: userInfoFromStorage,
  },
  sideBarState: {
    toggleClick: menuToggleState,
  },
};

const middleware = [thunk];

const store = createStore(
  reducer,
  initialState,
  composeWithDevTools(applyMiddleware(...middleware))
);

export default store;
