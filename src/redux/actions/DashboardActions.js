import {
  ADD_DAHBOARD_OVERLAY,
  BEST_ITEM_ALL_TIME_FAILURE,
  BEST_ITEM_ALL_TIME_REQUEST,
  BEST_ITEM_ALL_TIME_SUCCESS,
  CHANGE_DAHBOARD_OVERLAY_STATUS,
  ONLINE_SERVICE_OVERVIEW_FAILURE,
  ONLINE_SERVICE_OVERVIEW_REQUEST,
  ONLINE_SERVICE_OVERVIEW_SUCCESS,
  PROCUREMENT_OVERVIEW_FAILURE,
  PROCUREMENT_OVERVIEW_REQUEST,
  PROCUREMENT_OVERVIEW_SUCCESS,
  PRODUCT_LOCATION_OVERVIEW_FAILURE,
  PRODUCT_LOCATION_OVERVIEW_REQUEST,
  PRODUCT_LOCATION_OVERVIEW_SUCCESS,
  REMOVE_DAHBOARD_OVERLAY,
  SALES_ANALYSIS_FAILURE,
  SALES_ANALYSIS_REQUEST,
  SALES_ANALYSIS_SUCCESS,
  SALES_OVERVIEW_FAILURE,
  SALES_OVERVIEW_REQUEST,
  SALES_OVERVIEW_SUCCESS,
} from "../type.js";

import axios from "axios";

export const getSalesOverview =
  ({ fromDate, toDate }) =>
  async (dispatch) => {
    // console.log("getSalesOverview");
    try {
      dispatch({
        type: SALES_OVERVIEW_REQUEST,
      });

      const config = {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("access_token")}`,
        },
      };

      let filterKeyword = "";
      if (fromDate) {
        filterKeyword = `?from_date=${fromDate}`;
      }
      if (toDate) {
        filterKeyword = `${filterKeyword}&to_date=${toDate}`;
      }

      // console.log("filterKeyword = ", filterKeyword);

      const data = await axios.get(
        `${
          import.meta.env.VITE_REACT_APP_BASE_URL
        }/api/sales_dash/v1/sales/${filterKeyword}`,
        config
      );

      dispatch({
        type: SALES_OVERVIEW_SUCCESS,
        payload: data,
      });
    } catch (error) {
      dispatch({
        type: SALES_OVERVIEW_FAILURE,
        payload:
          error.response && error.response.data.detail
            ? error.response.data.detail
            : error.message,
      });
    }
  };

//

export const getProductLocationOverview =
  ({ fromDate, toDate }) =>
  async (dispatch) => {
    // console.log("getProductLocationOverview");
    try {
      dispatch({
        type: PRODUCT_LOCATION_OVERVIEW_REQUEST,
      });

      const config = {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("access_token")}`,
        },
      };

      let filterKeyword = "";
      if (fromDate) {
        filterKeyword = `?from_date=${fromDate}`;
      }
      if (toDate) {
        filterKeyword = `${filterKeyword}&to_date=${toDate}`;
      }

      // console.log("filterKeyword = ", filterKeyword);

      const data = await axios.get(
        `${
          import.meta.env.VITE_REACT_APP_BASE_URL
        }/api/sales_dash/v1/location_overview/${filterKeyword}`,
        config
      );

      dispatch({
        type: PRODUCT_LOCATION_OVERVIEW_SUCCESS,
        payload: data,
      });
    } catch (error) {
      dispatch({
        type: PRODUCT_LOCATION_OVERVIEW_FAILURE,
        payload:
          error.response && error.response.data.detail
            ? error.response.data.detail
            : error.message,
      });
    }
  };

//

export const getOnlineServiceOverview =
  ({ fromDate, toDate }) =>
  async (dispatch) => {
    // console.log("getOnlineServiceOverview");
    try {
      dispatch({
        type: ONLINE_SERVICE_OVERVIEW_REQUEST,
      });

      const config = {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("access_token")}`,
        },
      };

      let filterKeyword = "";
      if (fromDate) {
        filterKeyword = `?from_date=${fromDate}`;
      }
      if (toDate) {
        filterKeyword = `${filterKeyword}&to_date=${toDate}`;
      }

      // console.log("filterKeyword = ", filterKeyword);

      const data = await axios.get(
        `${
          import.meta.env.VITE_REACT_APP_BASE_URL
        }/api/sales_dash/v1/service/${filterKeyword}`,
        config
      );

      dispatch({
        type: ONLINE_SERVICE_OVERVIEW_SUCCESS,
        payload: data,
      });
    } catch (error) {
      dispatch({
        type: ONLINE_SERVICE_OVERVIEW_FAILURE,
        payload:
          error.response && error.response.data.detail
            ? error.response.data.detail
            : error.message,
      });
    }
  };

//

export const getProcurementOverview = (filterKeyword) => async (dispatch) => {
  try {
    dispatch({
      type: PROCUREMENT_OVERVIEW_REQUEST,
    });

    const config = {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("access_token")}`,
      },
    };
    // console.log("filterKeyword = ", filterKeyword);

    const data = await axios.get(
      `${
        import.meta.env.VITE_REACT_APP_BASE_URL
      }/api/sales_dash/v1/procurement_overview${filterKeyword}`,
      config
    );

    dispatch({
      type: PROCUREMENT_OVERVIEW_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: PROCUREMENT_OVERVIEW_FAILURE,
      payload:
        error.response && error.response.data.detail
          ? error.response.data.detail
          : error.message,
    });
  }
};

// DahboardOverlay

export const addDahboardOverlay = () => ({
  type: ADD_DAHBOARD_OVERLAY,
});

export const removeDahboardOverlay = () => ({
  type: REMOVE_DAHBOARD_OVERLAY,
});

export const changeDahboardOverlayStatus = (value) => ({
  type: CHANGE_DAHBOARD_OVERLAY_STATUS,
  payload: value,
});

//

export const getSalesAnalysis = (value) => async (dispatch) => {
  // console.log("getSalesAnalysis");
  try {
    dispatch({
      type: SALES_ANALYSIS_REQUEST,
    });

    const config = {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("access_token")}`,
      },
    };

    let filterKeyword = "";
    if (value) {
      filterKeyword = `?data_type=${value}`;
    }

    // console.log("filterKeyword = ", filterKeyword);

    const data = await axios.get(
      `${
        import.meta.env.VITE_REACT_APP_BASE_URL
      }/api/sales_dash/v1/analysis/${filterKeyword}`,
      config
    );

    dispatch({
      type: SALES_ANALYSIS_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: SALES_ANALYSIS_FAILURE,
      payload:
        error.response && error.response.data.detail
          ? error.response.data.detail
          : error.message,
    });
  }
};

//

export const getBestItemAllTime = (value) => async (dispatch) => {
  // console.log("BestItemAllTime");
  try {
    dispatch({
      type: BEST_ITEM_ALL_TIME_REQUEST,
    });

    const config = {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("access_token")}`,
      },
    };

    let filterKeyword = "";
    if (value) {
      filterKeyword = `?query=${value}`;
    }

    // console.log("filterKeyword = ", filterKeyword);

    const data = await axios.get(
      `${
        import.meta.env.VITE_REACT_APP_BASE_URL
      }/api/sales_dash/v1/best_item_all_time/${filterKeyword}`,
      config
    );

    dispatch({
      type: BEST_ITEM_ALL_TIME_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: BEST_ITEM_ALL_TIME_FAILURE,
      payload:
        error.response && error.response.data.detail
          ? error.response.data.detail
          : error.message,
    });
  }
};
