import {
    VOID_REPORT_DETAILS_REQUEST,
    VOID_REPORT_DETAILS_SUCCESS,
    VOID_REPORT_DETAILS_FAILURE,
    VOID_SUMMARY_REQUEST,
    VOID_SUMMARY_SUCCESS,
    VOID_SUMMARY_FAILURE
  } from "../type";

  import axios from "axios";

  //

  export const getVoidReportDetails =
  ({ fromDate, toDate }) =>
  async (dispatch) => {
    // console.log("getSalesOverview");
    try {
      dispatch({
        type: VOID_REPORT_DETAILS_REQUEST,
      });

      const config = {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("access_token")}`,
        },
      };

      let filterKeyword = "";
      if (fromDate) {
        filterKeyword = `?from_date=${fromDate}`;
      }
      if (toDate) {
        filterKeyword = `${filterKeyword}&to_date=${toDate}`;
      }

      // console.log("filterKeyword = ", filterKeyword);

      const data = await axios.get(
        `${
          import.meta.env.VITE_REACT_APP_BASE_URL
        }/api/report/v1/void_report/order-void-details-report/${filterKeyword}`,
        config
      );

      dispatch({
        type: VOID_REPORT_DETAILS_SUCCESS,
        payload: data,
      });
    } catch (error) {
      dispatch({
        type: VOID_REPORT_DETAILS_FAILURE,
        payload:
          error.response && error.response.data.detail
            ? error.response.data.detail
            : error.message,
      });
    }
  };
  
  //

  export const getVoidSummary =
  ({ fromDate, toDate }) =>
  async (dispatch) => {
    // console.log("getSalesOverview");
    try {
      dispatch({
        type: VOID_SUMMARY_REQUEST,
      });

      const config = {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("access_token")}`,
        },
      };

      let filterKeyword = "";
      if (fromDate) {
        filterKeyword = `?from_date=${fromDate}`;
      }
      if (toDate) {
        filterKeyword = `${filterKeyword}&to_date=${toDate}`;
      }

      // console.log("filterKeyword = ", filterKeyword);

      const data = await axios.get(
        `${
          import.meta.env.VITE_REACT_APP_BASE_URL
        }/api/report/v1/void_report/order-void-summary-report/${filterKeyword}`,
        config
      );

      dispatch({
        type: VOID_SUMMARY_SUCCESS,
        payload: data,
      });
    } catch (error) {
      dispatch({
        type: VOID_SUMMARY_FAILURE,
        payload:
          error.response && error.response.data.detail
            ? error.response.data.detail
            : error.message,
      });
    }
  };