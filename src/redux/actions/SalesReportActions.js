import {
  BARCODE_SALE_DETAIL_REQUEST,
  BARCODE_SALE_DETAIL_SUCCESS,
  BARCODE_SALE_DETAIL_FAILURE,
  PRODUCT_SALE_DETAIL_REQUEST,
  PRODUCT_SALE_DETAIL_SUCCESS,
  PRODUCT_SALE_DETAIL_FAILURE,
  DAILY_INVOICE_SALE_REQUEST,
  DAILY_INVOICE_SALE_SUCCESS,
  DAILY_INVOICE_SALE_FAILURE,
  CASHIER_WISE_SALE_DETAIL_REQUEST,
  CASHIER_WISE_SALE_DETAIL_SUCCESS,
  CASHIER_WISE_SALE_DETAIL_FAILURE,
  GENERIC_WISE_SALE_REQUEST,
  GENERIC_WISE_SALE_SUCCESS,
  GENERIC_WISE_SALE_FAILURE,
  ALL_CATEGORY_WISE_SALE_REQUEST,
  ALL_CATEGORY_WISE_SALE_SUCCESS,
  ALL_CATEGORY_WISE_SALE_FAILURE,
  ALL_SUBCATEGORY_WISE_SALE_REQUEST,
  ALL_SUBCATEGORY_WISE_SALE_SUCCESS,
  ALL_SUBCATEGORY_WISE_SALE_FAILURE,
  HOURLY_SALES_REPORT_REQUEST,
  HOURLY_SALES_REPORT_SUCCESS,
  HOURLY_SALES_REPORT_FAILURE,
  INVOICE_WISE_HOURLY_SALES_REQUEST,
  INVOICE_WISE_HOURLY_SALES_SUCCESS,
  INVOICE_WISE_HOURLY_SALES_FAILURE,
  SUPPLIER_WISE_PRODUCT_SALES_REQUEST,
  SUPPLIER_WISE_PRODUCT_SALES_SUCCESS,
  SUPPLIER_WISE_PRODUCT_SALES_FAILURE,
  CATEGORY_WISE_SUPPLIER_SALES_REQUEST,
  CATEGORY_WISE_SUPPLIER_SALES_SUCCESS,
  CATEGORY_WISE_SUPPLIER_SALES_FAILURE,
  ALL_SUPPLIER_WISE_SALES_REQUEST,
  ALL_SUPPLIER_WISE_SALES_SUCCESS,
  ALL_SUPPLIER_WISE_SALES_FAILURE,
  SUPPLIER_WISE_CATEGORY_SALES_REQUEST,
  SUPPLIER_WISE_CATEGORY_SALES_SUCCESS,
  SUPPLIER_WISE_CATEGORY_SALES_FAILURE,
  DAILY_SALE_SUMMARY_REQUEST,
  DAILY_SALE_SUMMARY_SUCCESS,
  DAILY_SALE_SUMMARY_FAILURE,
  DAILY_COUNTER_WISE_SALES_REQUEST,
  DAILY_COUNTER_WISE_SALES_SUCCESS,
  DAILY_COUNTER_WISE_SALES_FAILURE,
  DAILY_COUNTER_WISE_EXCHANGE_REQUEST,
  DAILY_COUNTER_WISE_EXCHANGE_SUCCESS,
  DAILY_COUNTER_WISE_EXCHANGE_FAILURE,
  CATEGORY_WISE_PRODUCT_SALES_REQUEST,
  CATEGORY_WISE_PRODUCT_SALES_SUCCESS,
  CATEGORY_WISE_PRODUCT_SALES_FAILURE,
  SUBCATEGORY_WISE_PRODUCT_SALES_REQUEST,
  SUBCATEGORY_WISE_PRODUCT_SALES_SUCCESS,
  SUBCATEGORY_WISE_PRODUCT_SALES_FAILURE,
  SECTION_WISE_SALE_REQUEST,
  SECTION_WISE_SALE_SUCCESS,
  SECTION_WISE_SALE_FAILURE,
  SUBCATEGORY_WISE_SUPPLIER_SALES_REQUEST,
  SUBCATEGORY_WISE_SUPPLIER_SALES_SUCCESS,
  SUBCATEGORY_WISE_SUPPLIER_SALES_FAILURE,
  SUPPLIER_WISE_SUBCATEGORY_SALES_REQUEST,
  SUPPLIER_WISE_SUBCATEGORY_SALES_SUCCESS,
  SUPPLIER_WISE_SUBCATEGORY_SALES_FAILURE,
} from "../type.js";
import axios from "axios";

export const getBarcodeSaleReport = (keyword) => async (dispatch) => {
  try {
    dispatch({
      type: BARCODE_SALE_DETAIL_REQUEST,
    });

    const config = {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("access_token")}`,
      },
    };

    const data = await axios.get(
      `${
        import.meta.env.VITE_REACT_APP_BASE_URL
      }/api/report/v1/sales_report/barcode_sales_report${keyword}`,
      config
    );

    dispatch({
      type: BARCODE_SALE_DETAIL_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: BARCODE_SALE_DETAIL_FAILURE,
      payload:
        error.response && error.response.data.detail
          ? error.response.data.detail
          : error.message,
    });
  }
};

export const getProductSaleReport = (keyword) => async (dispatch) => {
  try {
    dispatch({
      type: PRODUCT_SALE_DETAIL_REQUEST,
    });

    const config = {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("access_token")}`,
      },
    };

    const data = await axios.get(
      `${
        import.meta.env.VITE_REACT_APP_BASE_URL
      }/api/report/v1/sales_report/product_sales_report${keyword}`,
      config
    );

    dispatch({
      type: PRODUCT_SALE_DETAIL_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: PRODUCT_SALE_DETAIL_FAILURE,
      payload:
        error.response && error.response.data.detail
          ? error.response.data.detail
          : error.message,
    });
  }
};

export const getInvoiceSaleReport = (keyword) => async (dispatch) => {
  try {
    dispatch({
      type: DAILY_INVOICE_SALE_REQUEST,
    });

    const config = {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("access_token")}`,
      },
    };

    const data = await axios.get(
      `${
        import.meta.env.VITE_REACT_APP_BASE_URL
      }/api/report/v1/sales_report/invoice_sales_report${keyword}`,
      config
    );

    dispatch({
      type: DAILY_INVOICE_SALE_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: DAILY_INVOICE_SALE_FAILURE,
      payload:
        error.response && error.response.data.detail
          ? error.response.data.detail
          : error.message,
    });
  }
};

export const getCashierSaleReport = (keyword) => async (dispatch) => {
  try {
    dispatch({
      type: CASHIER_WISE_SALE_DETAIL_REQUEST,
    });

    const config = {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("access_token")}`,
      },
    };

    const data = await axios.get(
      `${
        import.meta.env.VITE_REACT_APP_BASE_URL
      }/api/report/v1/sales_report/cashier_sales_report${keyword}`,
      config
    );

    dispatch({
      type: CASHIER_WISE_SALE_DETAIL_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: CASHIER_WISE_SALE_DETAIL_FAILURE,
      payload:
        error.response && error.response.data.detail
          ? error.response.data.detail
          : error.message,
    });
  }
};

export const getDailySaleSummary = (keyword) => async (dispatch) => {
  try {
    dispatch({
      type: DAILY_SALE_SUMMARY_REQUEST,
    });

    const config = {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("access_token")}`,
      },
    };

    const data = await axios.get(
      `${
        import.meta.env.VITE_REACT_APP_BASE_URL
      }/api/report/v1/sales_report/daily_sales_report${keyword}`,
      config
    );

    dispatch({
      type: DAILY_SALE_SUMMARY_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: DAILY_SALE_SUMMARY_FAILURE,
      payload:
        error.response && error.response.data.detail
          ? error.response.data.detail
          : error.message,
    });
  }
};

export const getGenericSaleReport = (keyword) => async (dispatch) => {
  try {
    dispatch({
      type: GENERIC_WISE_SALE_REQUEST,
    });

    const config = {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("access_token")}`,
      },
    };

    const data = await axios.get(
      `${
        import.meta.env.VITE_REACT_APP_BASE_URL
      }/api/report/v1/sales_report/generic_sales_report${keyword}`,
      config
    );

    dispatch({
      type: GENERIC_WISE_SALE_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: GENERIC_WISE_SALE_FAILURE,
      payload:
        error.response && error.response.data.detail
          ? error.response.data.detail
          : error.message,
    });
  }
};

export const getAllCategorySaleReport = (keyword) => async (dispatch) => {
  try {
    dispatch({
      type: ALL_CATEGORY_WISE_SALE_REQUEST,
    });

    const config = {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("access_token")}`,
      },
    };

    const data = await axios.get(
      `${
        import.meta.env.VITE_REACT_APP_BASE_URL
      }/api/report/v1/sales_report/all_category_sales_report${keyword}`,
      config
    );

    dispatch({
      type: ALL_CATEGORY_WISE_SALE_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: ALL_CATEGORY_WISE_SALE_FAILURE,
      payload:
        error.response && error.response.data.detail
          ? error.response.data.detail
          : error.message,
    });
  }
};

export const getAllSubCategorySaleReport = (keyword) => async (dispatch) => {
  try {
    dispatch({
      type: ALL_SUBCATEGORY_WISE_SALE_REQUEST,
    });

    const config = {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("access_token")}`,
      },
    };

    const data = await axios.get(
      `${
        import.meta.env.VITE_REACT_APP_BASE_URL
      }/api/report/v1/sales_report/all_sub_category_sales_report${keyword}`,
      config
    );

    dispatch({
      type: ALL_SUBCATEGORY_WISE_SALE_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: ALL_SUBCATEGORY_WISE_SALE_FAILURE,
      payload:
        error.response && error.response.data.detail
          ? error.response.data.detail
          : error.message,
    });
  }
};

export const getHourlySaleReport = (keyword) => async (dispatch) => {
  try {
    dispatch({
      type: HOURLY_SALES_REPORT_REQUEST,
    });

    const config = {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("access_token")}`,
      },
    };

    const data = await axios.get(
      `${
        import.meta.env.VITE_REACT_APP_BASE_URL
      }/api/report/v1/sales_report/hourly_total_sales_report${keyword}`,
      config
    );

    dispatch({
      type: HOURLY_SALES_REPORT_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: HOURLY_SALES_REPORT_FAILURE,
      payload:
        error.response && error.response.data.detail
          ? error.response.data.detail
          : error.message,
    });
  }
};

export const getInvoiceWiseHourlySaleReport = (keyword) => async (dispatch) => {
  try {
    dispatch({
      type: INVOICE_WISE_HOURLY_SALES_REQUEST,
    });

    const config = {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("access_token")}`,
      },
    };

    const data = await axios.get(
      `${
        import.meta.env.VITE_REACT_APP_BASE_URL
      }/api/report/v1/sales_report/hourly_total_sales_report/?invoice=true&${keyword}`,
      config
    );

    dispatch({
      type: INVOICE_WISE_HOURLY_SALES_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: INVOICE_WISE_HOURLY_SALES_FAILURE,
      payload:
        error.response && error.response.data.detail
          ? error.response.data.detail
          : error.message,
    });
  }
};

export const getSupplierProductSaleReport = (keyword) => async (dispatch) => {
  try {
    dispatch({
      type: SUPPLIER_WISE_PRODUCT_SALES_REQUEST,
    });

    const config = {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("access_token")}`,
      },
    };

    const data = await axios.get(
      `${
        import.meta.env.VITE_REACT_APP_BASE_URL
      }/api/report/v1/sales_report/supplier_sales_report${keyword}`,
      config
    );

    dispatch({
      type: SUPPLIER_WISE_PRODUCT_SALES_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: SUPPLIER_WISE_PRODUCT_SALES_FAILURE,
      payload:
        error.response && error.response.data.detail
          ? error.response.data.detail
          : error.message,
    });
  }
};

export const getCategoryWiseSupplierReport = (keyword) => async (dispatch) => {
  try {
    dispatch({
      type: CATEGORY_WISE_SUPPLIER_SALES_REQUEST,
    });

    const config = {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("access_token")}`,
      },
    };

    const data = await axios.get(
      `${
        import.meta.env.VITE_REACT_APP_BASE_URL
      }/api/report/v1/sales_report/category_supplier_sales_report${keyword}`,
      config
    );

    dispatch({
      type: CATEGORY_WISE_SUPPLIER_SALES_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: CATEGORY_WISE_SUPPLIER_SALES_FAILURE,
      payload:
        error.response && error.response.data.detail
          ? error.response.data.detail
          : error.message,
    });
  }
};

export const getSupplierSaleSummary = (keyword) => async (dispatch) => {
  try {
    dispatch({
      type: ALL_SUPPLIER_WISE_SALES_REQUEST,
    });

    const config = {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("access_token")}`,
      },
    };

    const data = await axios.get(
      `${
        import.meta.env.VITE_REACT_APP_BASE_URL
      }/api/report/v1/sales_report/all_supplier_sales_report${keyword}`,
      config
    );

    dispatch({
      type: ALL_SUPPLIER_WISE_SALES_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: ALL_SUPPLIER_WISE_SALES_FAILURE,
      payload:
        error.response && error.response.data.detail
          ? error.response.data.detail
          : error.message,
    });
  }
};

export const getSupplierWiseCategoryReport = (keyword) => async (dispatch) => {
  try {
    dispatch({
      type: SUPPLIER_WISE_CATEGORY_SALES_REQUEST,
    });

    const config = {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("access_token")}`,
      },
    };

    const data = await axios.get(
      `${
        import.meta.env.VITE_REACT_APP_BASE_URL
      }/api/report/v1/sales_report/supplier_category_sales_report${keyword}`,
      config
    );

    dispatch({
      type: SUPPLIER_WISE_CATEGORY_SALES_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: SUPPLIER_WISE_CATEGORY_SALES_FAILURE,
      payload:
        error.response && error.response.data.detail
          ? error.response.data.detail
          : error.message,
    });
  }
};

export const getDailyCounterSaleSummary = (keyword) => async (dispatch) => {
  try {
    dispatch({
      type: DAILY_COUNTER_WISE_SALES_REQUEST,
    });

    const config = {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("access_token")}`,
      },
    };

    const data = await axios.get(
      `${
        import.meta.env.VITE_REACT_APP_BASE_URL
      }/api/report/v1/sales_report/counter_sales_report${keyword}`,
      config
    );

    dispatch({
      type: DAILY_COUNTER_WISE_SALES_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: DAILY_COUNTER_WISE_SALES_FAILURE,
      payload:
        error.response && error.response.data.detail
          ? error.response.data.detail
          : error.message,
    });
  }
};

export const getDailyCounterExchangeSummary = (keyword) => async (dispatch) => {
  try {
    dispatch({
      type: DAILY_COUNTER_WISE_EXCHANGE_REQUEST,
    });

    const config = {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("access_token")}`,
      },
    };

    const data = await axios.get(
      `${
        import.meta.env.VITE_REACT_APP_BASE_URL
      }/api/report/v1/sales_report/counter_exchange_report${keyword}`,
      config
    );

    dispatch({
      type: DAILY_COUNTER_WISE_EXCHANGE_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: DAILY_COUNTER_WISE_EXCHANGE_FAILURE,
      payload:
        error.response && error.response.data.detail
          ? error.response.data.detail
          : error.message,
    });
  }
};

export const getCategoryProductSaleReport = (keyword) => async (dispatch) => {
  try {
    dispatch({
      type: CATEGORY_WISE_PRODUCT_SALES_REQUEST,
    });

    const config = {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("access_token")}`,
      },
    };

    const data = await axios.get(
      `${
        import.meta.env.VITE_REACT_APP_BASE_URL
      }/api/report/v1/sales_report/category_sales_report${keyword}`,
      config
    );

    dispatch({
      type: CATEGORY_WISE_PRODUCT_SALES_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: CATEGORY_WISE_PRODUCT_SALES_FAILURE,
      payload:
        error.response && error.response.data.detail
          ? error.response.data.detail
          : error.message,
    });
  }
};

export const getSubCategoryProductSaleReport =
  (keyword) => async (dispatch) => {
    try {
      dispatch({
        type: SUBCATEGORY_WISE_PRODUCT_SALES_REQUEST,
      });

      const config = {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("access_token")}`,
        },
      };

      const data = await axios.get(
        `${
          import.meta.env.VITE_REACT_APP_BASE_URL
        }/api/report/v1/sales_report/sub_category_sales_report${keyword}`,
        config
      );

      dispatch({
        type: SUBCATEGORY_WISE_PRODUCT_SALES_SUCCESS,
        payload: data,
      });
    } catch (error) {
      dispatch({
        type: SUBCATEGORY_WISE_PRODUCT_SALES_FAILURE,
        payload:
          error.response && error.response.data.detail
            ? error.response.data.detail
            : error.message,
      });
    }
  };

export const getSectionWiseSaleReport = (keyword) => async (dispatch) => {
  try {
    dispatch({
      type: SECTION_WISE_SALE_REQUEST,
    });

    const config = {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("access_token")}`,
      },
    };

    const data = await axios.get(
      `${
        import.meta.env.VITE_REACT_APP_BASE_URL
      }/api/report/v1/sales_report/section_sales_report${keyword}`,
      config
    );

    dispatch({
      type: SECTION_WISE_SALE_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: SECTION_WISE_SALE_FAILURE,
      payload:
        error.response && error.response.data.detail
          ? error.response.data.detail
          : error.message,
    });
  }
};

export const getSubcategoryWiseSupplierReport =
  (keyword) => async (dispatch) => {
    try {
      dispatch({
        type: SUBCATEGORY_WISE_SUPPLIER_SALES_REQUEST,
      });

      const config = {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("access_token")}`,
        },
      };

      const data = await axios.get(
        `${
          import.meta.env.VITE_REACT_APP_BASE_URL
        }/api/report/v1/sales_report/sub_category_supplier_sales_report${keyword}`,
        config
      );

      dispatch({
        type: SUBCATEGORY_WISE_SUPPLIER_SALES_SUCCESS,
        payload: data,
      });
    } catch (error) {
      dispatch({
        type: SUBCATEGORY_WISE_SUPPLIER_SALES_FAILURE,
        payload:
          error.response && error.response.data.detail
            ? error.response.data.detail
            : error.message,
      });
    }
  };

export const getSupplierWiseSubcategoryReport =
  (keyword) => async (dispatch) => {
    try {
      dispatch({
        type: SUPPLIER_WISE_SUBCATEGORY_SALES_REQUEST,
      });

      const config = {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("access_token")}`,
        },
      };

      const data = await axios.get(
        `${
          import.meta.env.VITE_REACT_APP_BASE_URL
        }/api/report/v1/sales_report/supplier_sub_category_sales_report${keyword}`,
        config
      );

      dispatch({
        type: SUPPLIER_WISE_SUBCATEGORY_SALES_SUCCESS,
        payload: data,
      });
    } catch (error) {
      dispatch({
        type: SUPPLIER_WISE_SUBCATEGORY_SALES_FAILURE,
        payload:
          error.response && error.response.data.detail
            ? error.response.data.detail
            : error.message,
      });
    }
  };
