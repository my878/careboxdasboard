import { TOGGLE_SIDE_BAR } from "../type";

export const SidebarAction = (changeState) => async (dispatch) => {
  dispatch({
    type: TOGGLE_SIDE_BAR,
    payload: changeState,
  });
  localStorage.setItem("menuState", changeState);
};
