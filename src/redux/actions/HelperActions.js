import {
  SUPPLIER_LIST_REQUEST,
  SUPPLIER_LIST_SUCCESS,
  SUPPLIER_LIST_FAILURE,
  CATEGORY_LIST_REQUEST,
  CATEGORY_LIST_SUCCESS,
  CATEGORY_LIST_FAILURE,
  SUBCATEGORY_LIST_REQUEST,
  SUBCATEGORY_LIST_SUCCESS,
  SUBCATEGORY_LIST_FAILURE,
  CASHIER_LIST_REQUEST,
  CASHIER_LIST_SUCCESS,
  CASHIER_LIST_FAILURE,
  COUNTER_LIST_REQUEST,
  COUNTER_LIST_SUCCESS,
  COUNTER_LIST_FAILURE,
} from "../type.js";
import axios from "axios";

export const getSupplierList = (limit, offset) => async (dispatch) => {
  try {
    dispatch({
      type: SUPPLIER_LIST_REQUEST,
    });
    const config = {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("access_token")}`,
      },
    };
    const data = await axios.get(
      `${
        import.meta.env.VITE_REACT_APP_BASE_URL
      }/api/inventory/v1/SupplierListView/?limit=${limit}&offset=${offset}`,
      config
    );

    dispatch({
      type: SUPPLIER_LIST_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: SUPPLIER_LIST_FAILURE,
      payload:
        error.response && error.response.data.detail
          ? error.response.data.detail
          : error.message,
    });
  }
};

export const getCategoryList = (limit, offset) => async (dispatch) => {
  try {
    dispatch({
      type: CATEGORY_LIST_REQUEST,
    });

    const data = await axios.get(
      `${
        import.meta.env.VITE_REACT_APP_BASE_URL
      }/api/inventory/v1/product/catagoryinventory/?ordering=-id&limit=${limit}&offset=${offset}`
    );

    dispatch({
      type: CATEGORY_LIST_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: CATEGORY_LIST_FAILURE,
      payload:
        error.response && error.response.data.detail
          ? error.response.data.detail
          : error.message,
    });
  }
};

export const getSubcategoryList = (limit, offset) => async (dispatch) => {
  try {
    dispatch({
      type: SUBCATEGORY_LIST_REQUEST,
    });

    const data = await axios.get(
      `${
        import.meta.env.VITE_REACT_APP_BASE_URL
      }/api/inventory/v1/product/ListofSubCatagoryinventoryView/?ordering=-id&limit=${limit}&offset=${offset}`
    );

    dispatch({
      type: SUBCATEGORY_LIST_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: SUBCATEGORY_LIST_FAILURE,
      payload:
        error.response && error.response.data.detail
          ? error.response.data.detail
          : error.message,
    });
  }
};

export const getCashierList = () => async (dispatch) => {
  try {
    dispatch({
      type: CASHIER_LIST_REQUEST,
    });
    const config = {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("access_token")}`,
      },
    };
    const data = await axios.get(
      `${
        import.meta.env.VITE_REACT_APP_BASE_URL
      }/api/v1/pos/pos_cashire_list/?limit=500&offset=0`,
      config
    );

    dispatch({
      type: CASHIER_LIST_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: CASHIER_LIST_FAILURE,
      payload:
        error.response && error.response.data.detail
          ? error.response.data.detail
          : error.message,
    });
  }
};

export const getCounterList = () => async (dispatch) => {
  try {
    dispatch({
      type: COUNTER_LIST_REQUEST,
    });
    const config = {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("access_token")}`,
      },
    };
    const data = await axios.get(
      `${
        import.meta.env.VITE_REACT_APP_BASE_URL
      }/api/v1/product/polls/counter_list/`,
      config
    );

    dispatch({
      type: COUNTER_LIST_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: COUNTER_LIST_FAILURE,
      payload:
        error.response && error.response.data.detail
          ? error.response.data.detail
          : error.message,
    });
  }
};
