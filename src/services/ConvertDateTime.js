export const convertTime = (dateTime) => {
  const timestamp = new Date(dateTime);
  const formattedTime = timestamp.toLocaleTimeString([], {
    hour: "numeric",
    minute: "numeric",
    hour12: true,
  });
  return formattedTime;
};
