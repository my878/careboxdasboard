import jsPDF from "jspdf";
import "jspdf-autotable";

const addFooters = (doc, date, time) => {
  const pageCount = doc.internal.getNumberOfPages();
  doc.setFontSize(8);
  for (var i = 1; i <= pageCount; i++) {
    doc.setPage(i);
    doc.text("Date:" + date, 16, doc.internal.pageSize.height - 10);
    doc.text(
      "Page " + String(i) + " of " + String(pageCount),
      doc.internal.pageSize.width / 2,
      doc.internal.pageSize.height - 10,
      {
        align: "center",
      }
    );
    doc.text(
      "Time:" + time,
      doc.internal.pageSize.width - 35,
      doc.internal.pageSize.height - 10
    );
  }
};

const InvoiceReportPdf = (
  tableData,
  reportTitle,
  pdfTitle,
  fromDate,
  toDate
) => {
  const doc = new jsPDF({
    orientation: "landscape",
  });

  const date = new Date();
  var title = "Care-Box";
  var reportName = reportTitle;
  var titleWidth = doc.getTextDimensions(title).w;
  var pageWidth =
    doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
  var x = (pageWidth - titleWidth) / 2;
  doc.text(title, x, 14);
  doc.text(
    reportName,
    (pageWidth - doc.getTextDimensions(reportName).w) / 2,
    20
  );

  tableData.forEach((data, index) => {
    doc.autoTable({
      startY: index === 0 ? 30 : doc.autoTable.previous.finalY + 10, // Adjust starting Y position
      head: [
        [
          {
            content: `From Date : ${fromDate !== "" ? fromDate : "N/A"}`,
            colSpan: 8,
          },
          {
            content: `To Date : ${toDate !== "" ? toDate : "N/A"}`,
            colSpan: 7,
          },
        ],
        [
          "Invoice No.",
          "MRP",
          "Disc. Amount",
          "Vat",
          "Exchange",
          "Net Amount",
          "Cash",
          "Online",
          "Card",
          "Due",
          "Total Cost",
          "Return Cost",
          "Pft Amount",
          "GP%",
          "I.Time",
        ],
      ],
      body: data.invoices
        .map((product) => [
          product.id,
          product.mrp,
          product.discount_amount,
          product.vat_amount,
          product.exchange_amount,
          product.net_amount,
          product.cash,
          product.online,
          product.card,
          product.due,
          product.total_cost,
          product.return_cost,
          product.profit_amount,
          product.gross_profit_percent,
          product.invoice_time.slice(0, 8),
        ])
        .concat([
          [
            `(${data.hour})  Grand Total`,
            data.grand.grand_mrp,
            data.grand.grand_discount_amount,
            data.grand.grand_vat_amount,
            data.grand.grand_exchange_amount,
            data.grand.grand_net_amount,
            data.grand.grand_cash,
            data.grand.grand_online,
            data.grand.grand_card,
            data.grand.grand_due,
            data.grand.grand_total_cost,
            data.grand.grand_return_cost,
            data.grand.grand_profit_amount,
            data.grand.grand_gross_profit_percent,
          ],
        ]),
      theme: "plain",
      styles: {
        halign: "center",
        lineColor: "DCE0E4",
        lineWidth: 0.2,
      },
      headStyles: {
        textColor: "black",
        fillColor: "#fafbfe",
      },
    });
  });

  addFooters(doc, date.toLocaleDateString(), date.toLocaleTimeString());
  doc.save(`${pdfTitle}_${date.toLocaleTimeString()}`);
};

export default InvoiceReportPdf;
