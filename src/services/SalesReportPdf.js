import jsPDF from "jspdf";
import "jspdf-autotable";

const addFooters = (doc, date, time) => {
  const pageCount = doc.internal.getNumberOfPages();
  doc.setFontSize(8);
  for (var i = 1; i <= pageCount; i++) {
    doc.setPage(i);
    doc.text("Date:" + date, 16, doc.internal.pageSize.height - 10);
    doc.text(
      "Page " + String(i) + " of " + String(pageCount),
      doc.internal.pageSize.width / 2,
      doc.internal.pageSize.height - 10,
      {
        align: "center",
      }
    );
    doc.text(
      "Time:" + time,
      doc.internal.pageSize.width - 35,
      doc.internal.pageSize.height - 10
    );
  }
};

const SalesReportPdf = (
  headerRows,
  itemDetails,
  reportTitle,
  pdfTitle,
  filterData
) => {
  const doc = new jsPDF({
    orientation: "landscape",
  });
  var tableRows = [];
  const date = new Date();
  if (reportTitle === "Barcode Wise Sales") {
    itemDetails.products_data.results.forEach((item, index) => {
      const rowData = [
        index + 1,
        item.product_name + " " + item.generic_name + " " + item.product_unit,
        item.sales_date?.split("T")[0],
        item.sold_quantity,
        item.total_mrp,
        item.discount_amount,
        item.vat_amount,
        item.return_amount,
        item.total_cost,
        item.net_amount,
        item.profit_amouunt,
        item.gross_profit,
      ];

      tableRows.push(rowData);
    });
    const grandTotalRow = [
      "Grand Total",
      "",
      "",
      itemDetails.grand_total.total_sold_quantity,
      itemDetails.grand_total.grand_total_mrp,
      itemDetails.grand_total.total_discount_amount,
      itemDetails.grand_total.total_vat_amount,
      itemDetails.grand_total.total_return_amount,
      itemDetails.grand_total.grand_total_cost,
      itemDetails.grand_total.total_net_amount,
      itemDetails.grand_total.total_profit_amount,
      itemDetails.grand_total.total_gross_profit,
    ];
    tableRows.push(grandTotalRow);
  } else if (reportTitle === "Product Wise Sales") {
    itemDetails.products_data.results.forEach((item, index) => {
      const rowData = [
        index + 1,
        item.barcode ? item.barcode : "N/A",
        item.product_name + " " + item.generic_name + " " + item.product_unit,
        item.sales_date?.split("T")[0],
        item.sold_quantity,
        item.total_mrp,
        item.discount_amount,
        item.vat_amount,
        item.return_amount,
        item.total_cost,
        item.net_amount,
        item.profit_amouunt,
        item.gross_profit,
      ];

      tableRows.push(rowData);
    });
    const grandTotalRow = [
      "Grand Total",
      "",
      "",
      "",
      itemDetails.grand_total.total_sold_quantity,
      itemDetails.grand_total.grand_total_mrp,
      itemDetails.grand_total.total_discount_amount,
      itemDetails.grand_total.total_vat_amount,
      itemDetails.grand_total.total_return_amount,
      itemDetails.grand_total.grand_total_cost,
      itemDetails.grand_total.total_net_amount,
      itemDetails.grand_total.total_profit_amount,
      itemDetails.grand_total.total_gross_profit,
    ];
    tableRows.push(grandTotalRow);
  } else if (reportTitle === "Daily Invoice Wise Sales Summary") {
    itemDetails.products_data.results.forEach((item, index) => {
      const rowData = [
        index + 1,
        item.invoice_number ? item.invoice_number : "N/A",
        item.total_mrp,
        item.discount_amount,
        item.vat_amount,
        item.exchange_amount,
        item.net_amount,
        item.redeem_point ? item.redeem_point : 0,
        item.order_cost,
        item.exchange_cost,
        item.cash_paid,
        item.online_paid,
        item.card_paid,
        item.due,
        item.profit_amount,
        item.gp_amount,
        item.created_time.slice(0, 8),
      ];

      tableRows.push(rowData);
    });
    const grandTotalRow = [
      "Grand Total",
      "",
      itemDetails.sub_total.sub_total_mrp,
      itemDetails.sub_total.total_discount,
      itemDetails.sub_total.total_vat,
      itemDetails.sub_total.total_exchange,
      itemDetails.sub_total.total_net_amount,
      itemDetails.sub_total.total_point,
      itemDetails.sub_total.sub_total_cost,
      itemDetails.sub_total.total_exchange_cost,
      itemDetails.sub_total.total_cash,
      itemDetails.sub_total.total_online,
      itemDetails.sub_total.total_card,
      itemDetails.sub_total.total_due,
      itemDetails.sub_total.total_profit,
      itemDetails.sub_total.total_gp,
      "",
    ];
    tableRows.push(grandTotalRow);
  } else if (reportTitle === "Cashier Wise Daily Sales") {
    itemDetails.products_data.results.forEach((item, index) => {
      const rowData = [
        index + 1,
        item.invoice_number ? item.invoice_number : item.id,
        item.total_amount,
        item.discount_amount,
        item.net_amount,
        item.cash_paid,
        item.online_paid,
        item.card_paid,
        item.due,
        item.payment_method,
      ];

      tableRows.push(rowData);
    });
    const grandTotalRow = [
      "Grand Total",
      "",
      itemDetails.grand_total.grand_total,
      itemDetails.grand_total.grand_discount,
      itemDetails.grand_total.grand_net,
      itemDetails.grand_total.grand_cash,
      itemDetails.grand_total.grand_online,
      itemDetails.grand_total.grand_card,
      itemDetails.grand_total.grand_due,
      "",
    ];
    tableRows.push(grandTotalRow);
  } else if (reportTitle === "Daily Sales Summary") {
    filterData.forEach((item) => {
      const rowData = [
        item.sales_date,
        item.sales_amount,
        item.less_return_vat,
        item.return_vat,
        item.cumulativeSale ? item.cumulativeSale.toFixed(2) : 0,
      ];

      tableRows.push(rowData);
    });
    const grandTotalRow = [
      "Grand Total",
      itemDetails.grand_total.total_sales_amount,
      itemDetails.grand_total.total_less_return_vat,
      itemDetails.grand_total.total_return_vat,
      itemDetails.grand_total.total_sales_amount,
    ];
    tableRows.push(grandTotalRow);
  } else if (reportTitle === "Generic Name Wise Sales") {
    itemDetails.generic_data.results.forEach((item, index) => {
      const rowData = [
        index + 1,
        item.barcode ? item.barcode : "N/A",
        item.sale_date,
        item.product_name + item.generic_name + item.product_unit,
        item.sold_quantity,
        item.mrp_amount,
        item.discount_amount,
        item.vat_amount,
        item.exchange_amount,
        item.cost_amount,
        item.net_amount,
        item.profit_amount,
        item.gp_amount,
      ];

      tableRows.push(rowData);
    });
    const grandTotalRow = [
      "Grand Total",
      "",
      "",
      "",
      itemDetails.grand_total.total_sold,
      itemDetails.grand_total.total_mrp,
      itemDetails.grand_total.total_discount,
      itemDetails.grand_total.total_vat,
      itemDetails.grand_total.total_exchange,
      itemDetails.grand_total.total_cost,
      itemDetails.grand_total.total_amount,
      itemDetails.grand_total.total_profit,
      itemDetails.grand_total.total_gp,
    ];
    tableRows.push(grandTotalRow);
  } else if (reportTitle === "Section Wise Product Sales Summary") {
    itemDetails.category_data.forEach((item, index) => {
      const rowData = [
        index + 1,
        item.barcode ? item.barcode : "N/A",
        item.sales_date,
        item.product_name + item.unit,
        item.sold_qty,
        item.total_mrp,
        item.discount_amount,
        item.vat_amount,
        item.exchange_amount,
        item.total_cost,
        item.net_amount,
        item.pft_amount,
        item.gross_profit,
      ];

      tableRows.push(rowData);
    });
    const grandTotalRow = [
      "Grand Total",
      "",
      "",
      "",
      itemDetails.sub_total[0].sold_qty,
      itemDetails.sub_total[0].total_mrp,
      itemDetails.sub_total[0].discount_amount,
      itemDetails.sub_total[0].vat_amount,
      itemDetails.sub_total[0].exchange_amount,
      itemDetails.sub_total[0].total_cost,
      itemDetails.sub_total[0].net_amount,
      itemDetails.sub_total[0].pft_amount,
      itemDetails.sub_total[0].gross_profit,
    ];
    tableRows.push(grandTotalRow);
  } else if (reportTitle === "SubCategory Wise Product Sales Summary") {
    itemDetails.sub_category_data.forEach((item, index) => {
      const rowData = [
        index + 1,
        item.barcode ? item.barcode : "N/A",
        item.sales_date,
        item.product_name + item.unit,
        item.sold_qty,
        item.total_mrp,
        item.discount_amount,
        item.vat_amount,
        item.exchange_amount,
        item.total_cost,
        item.net_amount,
        item.pft_amount,
        item.gross_profit,
      ];

      tableRows.push(rowData);
    });
    const grandTotalRow = [
      "Grand Total",
      "",
      "",
      "",
      itemDetails.sub_total[0].sold_qty,
      itemDetails.sub_total[0].total_mrp,
      itemDetails.sub_total[0].discount_amount,
      itemDetails.sub_total[0].vat_amount,
      itemDetails.sub_total[0].exchange_amount,
      itemDetails.sub_total[0].total_cost,
      itemDetails.sub_total[0].net_amount,
      itemDetails.sub_total[0].pft_amount,
      itemDetails.sub_total[0].gross_profit,
    ];
    tableRows.push(grandTotalRow);
  } else if (reportTitle === "All Category/Sub Category Wise Sales Summary") {
    itemDetails.forEach((item, index) => {
      const rowData = [
        index + 1,
        item.product_category ? item.product_category : item.sub_category,
        item.sold_qty,
        item.exchange_qty,
        item.total_mrp,
        item.discount_amount,
        item.vat_amount,
        item.exchange_amount,
        item.total_cost,
        item.net_amount,
        item.pft_amount,
        item.gross_profit,
      ];

      tableRows.push(rowData);
    });
    const grandTotalRow = [
      "Grand Total",
      "",
      filterData.total_sold_qty,
      filterData.total_exchange_qty,
      filterData.sub_total_mrp,
      filterData.sub_total_discount,
      filterData.sub_total_vat,
      filterData.sub_total_exchange,
      filterData.sub_total_cost,
      filterData.total_net_amount,
      filterData.total_pft_amount,
      filterData.total_gross_profit,
    ];
    tableRows.push(grandTotalRow);
  } else if (reportTitle === "Comparative Hourly Sales Summary") {
    itemDetails.sales.forEach((item) => {
      const rowData = [
        item.date,
        item.hourly_sales["00"],
        item.hourly_sales["01"],
        item.hourly_sales["02"],
        item.hourly_sales["03"],
        item.hourly_sales["04"],
        item.hourly_sales["05"],
        item.hourly_sales["06"],
        item.hourly_sales["07"],
        item.hourly_sales["08"],
        item.hourly_sales["09"],
        item.hourly_sales["10"],
        item.hourly_sales["11"],
        item.hourly_sales["12"],
        item.hourly_sales["13"],
        item.hourly_sales["14"],
        item.hourly_sales["15"],
        item.hourly_sales["16"],
        item.hourly_sales["17"],
        item.hourly_sales["18"],
        item.hourly_sales["19"],
        item.hourly_sales["20"],
        item.hourly_sales["21"],
        item.hourly_sales["22"],
        item.hourly_sales["23"],
      ];

      tableRows.push(rowData);
    });
    const grandTotalRow = [
      "Grand Total",
      itemDetails.grand_total.filter((data) => data.hour === "00")[0].value,
      itemDetails.grand_total.filter((data) => data.hour === "01")[0].value,
      itemDetails.grand_total.filter((data) => data.hour === "02")[0].value,
      itemDetails.grand_total.filter((data) => data.hour === "03")[0].value,
      itemDetails.grand_total.filter((data) => data.hour === "04")[0].value,
      itemDetails.grand_total.filter((data) => data.hour === "05")[0].value,
      itemDetails.grand_total.filter((data) => data.hour === "06")[0].value,
      itemDetails.grand_total.filter((data) => data.hour === "07")[0].value,
      itemDetails.grand_total.filter((data) => data.hour === "08")[0].value,
      itemDetails.grand_total.filter((data) => data.hour === "09")[0].value,
      itemDetails.grand_total.filter((data) => data.hour === "10")[0].value,
      itemDetails.grand_total.filter((data) => data.hour === "11")[0].value,
      itemDetails.grand_total.filter((data) => data.hour === "12")[0].value,
      itemDetails.grand_total.filter((data) => data.hour === "13")[0].value,
      itemDetails.grand_total.filter((data) => data.hour === "14")[0].value,
      itemDetails.grand_total.filter((data) => data.hour === "15")[0].value,
      itemDetails.grand_total.filter((data) => data.hour === "16")[0].value,
      itemDetails.grand_total.filter((data) => data.hour === "17")[0].value,
      itemDetails.grand_total.filter((data) => data.hour === "18")[0].value,
      itemDetails.grand_total.filter((data) => data.hour === "19")[0].value,
      itemDetails.grand_total.filter((data) => data.hour === "20")[0].value,
      itemDetails.grand_total.filter((data) => data.hour === "21")[0].value,
      itemDetails.grand_total.filter((data) => data.hour === "22")[0].value,
      itemDetails.grand_total.filter((data) => data.hour === "23")[0].value,
    ];
    tableRows.push(grandTotalRow);
  } else if (reportTitle === "Supplier Wise Product Sales") {
    itemDetails.supplier_data.forEach((item, index) => {
      const rowData = [
        index + 1,
        item.product_barcode ? item.product_barcode : "N/A",
        item.product_unit
          ? item.product_name + item.product_unit
          : item.product_name,
        item.sales_date,
        item.sold_qty,
        item.total_mrp,
        item.discount_amount,
        item.vat_amount,
        item.exchange_amount,
        item.total_cost,
        item.net_amount,
        item.pft_amount,
        item.gross_profit,
      ];

      tableRows.push(rowData);
    });
    const grandTotalRow = [
      "Grand Total",
      "",
      "",
      "",
      itemDetails.sub_total[0].sold_qty,
      itemDetails.sub_total[0].total_mrp,
      itemDetails.sub_total[0].discount_amount,
      itemDetails.sub_total[0].vat_amount,
      itemDetails.sub_total[0].exchange_amount,
      itemDetails.sub_total[0].total_cost,
      itemDetails.sub_total[0].net_amount,
      itemDetails.sub_total[0].pft_amount,
      itemDetails.sub_total[0].gross_profit,
    ];
    tableRows.push(grandTotalRow);
  } else if (reportTitle === "All Supplier Wise Sales Summary") {
    itemDetails.supplier_data.forEach((item, index) => {
      const rowData = [
        index + 1,
        item.supplier_name ? item.supplier_name : "N/A",
        item.sold_qty,
        item.exchange_qty,
        item.total_mrp,
        item.discount_amount,
        item.vat_amount,
        item.exchange_amount,
        item.net_amount,
        item.pft_amount,
        item.gross_profit,
      ];

      tableRows.push(rowData);
    });
    const grandTotalRow = [
      "Grand Total",
      "",
      itemDetails.sub_total.total_sold_qty,
      itemDetails.sub_total.total_exchange_qty,
      itemDetails.sub_total.sub_total_mrp,
      itemDetails.sub_total.sub_total_discount,
      itemDetails.sub_total.sub_total_vat,
      itemDetails.sub_total.sub_total_exchange,
      itemDetails.sub_total.total_net_amount,
      itemDetails.sub_total.total_pft_amount,
      itemDetails.sub_total.total_gross_profit,
    ];
    tableRows.push(grandTotalRow);
  } else if (reportTitle === "Daily Counter Wise Sales Summary") {
    itemDetails.counter_data.forEach((item) => {
      const rowData = [
        item.invoice_id ? item.invoice_id : "N/A",
        item.total_mrp,
        item.discount_amount,
        item.vat_amount,
        item.exchange_amount,
        item.net_amount,
        item.total_cost,
        item.exchange_amount,
        item.cash_paid,
        item.online_paid,
        item.card_paid,
        item.due,
        item.less_amount,
        item.pft_amount,
        item.gross_profit,
      ];

      tableRows.push(rowData);
    });
    const grandTotalRow = [
      "Grand Total",
      itemDetails.counter_total[0].total_mrp,
      itemDetails.counter_total[0].discount_amount,
      itemDetails.counter_total[0].vat_amount,
      itemDetails.counter_total[0].exchange_amount,
      itemDetails.counter_total[0].net_amount,
      itemDetails.counter_total[0].total_cost,
      itemDetails.counter_total[0].exchange_amount,
      itemDetails.counter_total[0].cash_paid,
      itemDetails.counter_total[0].online_paid,
      itemDetails.counter_total[0].card_paid,
      itemDetails.counter_total[0].due,
      itemDetails.counter_total[0].less_amount,
      itemDetails.counter_total[0].pft_amount,
      itemDetails.counter_total[0].gross_profit,
    ];
    tableRows.push(grandTotalRow);
  }
  var title = "Care-Box";
  var reportName = reportTitle;
  var titleWidth = doc.getTextDimensions(title).w;
  var pageWidth =
    doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
  var x = (pageWidth - titleWidth) / 2;
  doc.text(title, x, 14);
  doc.text(
    reportName,
    (pageWidth - doc.getTextDimensions(reportName).w) / 2,
    20
  );

  doc.autoTable({
    //styles: { fillColor: [0, 0, 0] },
    startY: 30,
    head: headerRows,
    body: tableRows,
    theme: "plain",
    styles: {
      halign: "center",
      lineColor: "DCE0E4",
      lineWidth: 0.2,
    },
    headStyles: {
      textColor: "black",
      fillColor: "#fafbfe",
    },
  });
  addFooters(doc, date.toLocaleDateString(), date.toLocaleTimeString());
  doc.save(`${pdfTitle}_${date.toLocaleTimeString()}`);
};

export default SalesReportPdf;
