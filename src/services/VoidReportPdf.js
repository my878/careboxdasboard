import React from "react";
import { jsPDF } from "jspdf";
import autoTable from "jspdf-autotable";

const addFooters = (doc, date, time) => {
  const pageCount = doc.internal.getNumberOfPages();
  doc.setFontSize(8);
  for (var i = 1; i <= pageCount; i++) {
    doc.setPage(i);
    doc.text("Date:" + date, 16, doc.internal.pageSize.height - 10);
    doc.text(
      "Page " + String(i) + " of " + String(pageCount),
      doc.internal.pageSize.width / 2,
      doc.internal.pageSize.height - 10,
      {
        align: "center",
      }
    );
    doc.text(
      "Time:" + time,
      doc.internal.pageSize.width - 35,
      doc.internal.pageSize.height - 10
    );
  }
};

const VoidReportPdf = (pdfTitle, headerRows, itemDetails) => {
  // console.log("pdfTitle = ", pdfTitle);
  // console.log("headerRows = ", headerRows);
  // console.log("itemDetails = ", itemDetails);
  const date = new Date();
  const doc = new jsPDF({
    orientation: "landscape",
  });

  let tableRows = [];

  if (pdfTitle === "Void Report Details") {
    // console.log("inside");
    if (itemDetails.void_data.length) {
      itemDetails.void_data.map((product, index) => {
        const rowData = [
          product.barcode || "N/A",
          `${product.product_name} - ${product.style_size} - ${product.sub_category}`,
          product.sale_date,
          product.void_date,
          product.sold_quantity,
          product.quantity,
          product.mrp,
          product.discount_amount,
          product.vat_amount,
          product.net_amount,
          product.void_type,
          product.voided_by,
        ];
        tableRows.push(rowData);
      });

      const grandTotalRow = [
        {
          content: `Grand Total`,
          colSpan: 4,
          styles: {
            halign: "left",
            fontStyle: "bold",
          },
        },
        {
          content: `${itemDetails.sub_total.total_sold_quantity}`,
          styles: {
            fontStyle: "bold",
          },
        },
        {
          content: `${itemDetails.sub_total.total_quantity}`,
          styles: {
            fontStyle: "bold",
          },
        },
        {
          content: `${itemDetails.sub_total.total_mrp}`,
          styles: {
            fontStyle: "bold",
          },
        },
        {
          content: `${itemDetails.sub_total.total_discount_amount}`,
          styles: {
            fontStyle: "bold",
          },
        },
        {
          content: `${itemDetails.sub_total.total_vat_amount}`,
          styles: {
            fontStyle: "bold",
          },
        },
        {
          content: `${itemDetails.sub_total.total_net_amount}`,
          styles: {
            fontStyle: "bold",
          },
        },
        "",
        "",
      ];
      tableRows.push(grandTotalRow);
    } else {
      let emptyDataRow = [
        {
          content: `No Data Found`,
          colSpan: 12,
        },
      ];
      tableRows.push(emptyDataRow);
    }
  } else if (pdfTitle === "Void Summary") {
    // console.log("Void Summary");
    if (itemDetails.void_data.length) {
      itemDetails.void_data.map((product, index) => {
        const rowData = [
          product.invoice_no,
          product.mrp,
          product.discount_amount,
          product.vat_amount,
          product.net_amount,
          product.void_cost,
          product.pft_amount,
          product.gross_profit,
          product.void_date,
          product.void_time.slice(0, 8),
          product.ref_invoice,
          product.voided_by,
        ];
        tableRows.push(rowData);
      });

      const grandTotalRow = [
        {
          content: `Grand Total`,
          styles: {
            halign: "left",
            fontStyle: "bold",
          },
        },
        {
          content: `${itemDetails.sub_total.total_mrp}`,
          styles: {
            fontStyle: "bold",
          },
        },
        {
          content: `${itemDetails.sub_total.total_discount_amount}`,
          styles: {
            fontStyle: "bold",
          },
        },
        {
          content: `${itemDetails.sub_total.total_vat_amount}`,
          styles: {
            fontStyle: "bold",
          },
        },
        {
          content: `${itemDetails.sub_total.total_net_amount}`,
          styles: {
            fontStyle: "bold",
          },
        },
        {
          content: `${itemDetails.sub_total.total_void_cost}`,
          styles: {
            fontStyle: "bold",
          },
        },
        {
          content: `${itemDetails.sub_total.total_pft_amount}`,
          styles: {
            fontStyle: "bold",
          },
        },
        {
          content: `${itemDetails.sub_total.total_gp}`,
          styles: {
            fontStyle: "bold",
          },
        },
        "",
        "",
        "",
        "",
      ];
      tableRows.push(grandTotalRow);
    } else {
      let emptyDataRow = [
        {
          content: `No Data Found`,
          colSpan: 12,
        },
      ];
      tableRows.push(emptyDataRow);
    }
  }

  var title = "Care-Box";
  var reportName = pdfTitle;
  var titleWidth = doc.getTextDimensions(title).w;
  var pageWidth =
    doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
  var x = (pageWidth - titleWidth) / 2;
  doc.text(title, x, 14);
  doc.text(
    reportName,
    (pageWidth - doc.getTextDimensions(reportName).w) / 2,
    20
  );

  autoTable(doc, {
    startY: 30,
    head: headerRows,
    body: tableRows,
    theme: "plain",
    styles: {
      halign: "center",
      lineColor: "DCE0E4",
      lineWidth: 0.2,
    },
    headStyles: {
      textColor: "black",
      fillColor: "#fafbfe",
    },
  });

  addFooters(doc, date.toLocaleDateString(), date.toLocaleTimeString());
  doc.save(`${pdfTitle}_${date.toLocaleTimeString()}`);
};

export default VoidReportPdf;
