import jsPDF from "jspdf";
import "jspdf-autotable";

const addFooters = (doc, date, time) => {
  const pageCount = doc.internal.getNumberOfPages();
  doc.setFontSize(8);
  for (var i = 1; i <= pageCount; i++) {
    doc.setPage(i);
    doc.text("Date:" + date, 16, doc.internal.pageSize.height - 10);
    doc.text(
      "Page " + String(i) + " of " + String(pageCount),
      doc.internal.pageSize.width / 2,
      doc.internal.pageSize.height - 10,
      {
        align: "center",
      }
    );
    doc.text(
      "Time:" + time,
      doc.internal.pageSize.width - 35,
      doc.internal.pageSize.height - 10
    );
  }
};

const SupplierSaleReportPdf = (
  tableData,
  reportTitle,
  pdfTitle,
  fromDate,
  toDate,
  headerTitle
) => {
  const doc = new jsPDF({
    orientation: "landscape",
  });

  const date = new Date();
  var title = "Care-Box";
  var reportName = reportTitle;
  var titleWidth = doc.getTextDimensions(title).w;
  var pageWidth =
    doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
  var x = (pageWidth - titleWidth) / 2;
  doc.text(title, x, 14);
  doc.text(
    reportName,
    (pageWidth - doc.getTextDimensions(reportName).w) / 2,
    20
  );

  tableData.forEach((data, index) => {
    doc.autoTable({
      startY: index === 0 ? 30 : doc.autoTable.previous.finalY + 10, // Adjust starting Y position
      head: [
        [
          {
            content: `${headerTitle} ${
              reportTitle === "Category Wise Supplier Sales Summary"
                ? data.category.product_category
                : data.category.product_sub_category
            }`,
            colSpan: 4,
          },
          {
            content: `From Date : ${fromDate !== "" ? fromDate : "N/A"}`,
            colSpan: 3,
          },
          {
            content: `To Date : ${toDate !== "" ? toDate : "N/A"}`,
            colSpan: 3,
          },
        ],
        [
          "Supplier Name",
          "Sold(Qty)",
          "Total MRP",
          "Discount Amount",
          "Vat Amount",
          "Ex/Ret Amount",
          "Total Cost",
          "Net Amount",
          "Pft Amount",
          "GP%",
        ],
      ],
      body: data.products_data
        .map((product) => [
          product.supplier_name ? product.supplier_name : "N/A",
          product.sold_qty,
          product.total_mrp,
          product.discount_amount,
          product.vat_amount,
          product.exchange_amount,
          product.total_cost,
          product.net_amount,
          product.pft_amount,
          product.gross_profit,
        ])
        .concat([
          [
            `Grand Total`,
            data.category.sold_qty,
            data.category.total_mrp,
            data.category.discount_amount,
            data.category.vat_amount,
            data.category.exchange_amount,
            data.category.total_cost,
            data.category.net_amount,
            data.category.pft_amount,
            data.category.gross_profit,
          ],
        ]),
      theme: "plain",
      styles: {
        halign: "center",
        lineColor: "DCE0E4",
        lineWidth: 0.2,
      },
      headStyles: {
        textColor: "black",
        fillColor: "#fafbfe",
      },
    });
  });

  addFooters(doc, date.toLocaleDateString(), date.toLocaleTimeString());
  doc.save(`${pdfTitle}_${date.toLocaleTimeString()}`);
};

export default SupplierSaleReportPdf;
